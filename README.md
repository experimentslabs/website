# README

Website used for [ExperimentsLabs](https://experimentslabs.com).

Feel free to use it too.

[TOC]: #

# Table of Contents
- [Requirements](#requirements)
- [Development](#development)
  - [Translations](#translations)
  - [Running tests](#running-tests)
- [Deployment](#deployment)
  - [Cron jobs](#cron-jobs)
- [Shortcodes guide](#shortcodes-guide)
  - [Inline shortcodes](#inline-shortcodes)
  - [Cards](#cards)

## Requirements

- Ruby >= 2.5.1
- Node >= 10
- Postgresql
- ImageMagick and ffmpeg to generate file previews

## Development

- Clone the repo
- Install Node dependencies: `yarn install`. It may fail as there is an
  issue with node engine version and some dependencies. If that's the
  case, run `yarn install --ignore-engines`
- Setup the app: `bin/setup`. It will:
  - Install Ruby dependencies (`bundle install`)
  - Set up configuration files (database, application, ...)
  - Prepare the database (`bundle exec rails db:prepare`)
  - Clear the logs
  - Restart the application server (`bundle exec rails restart`)
- Run the seeds: `bundle exec rails db:seed`. Seeds are per-environment;
  in development, you will end up with an `admin@example.com:password`
  user and some generated content.

Start the server:

```sh
bundle exec rails server
```

And in another terminal, to have an assets server

```sh
bin/webpack-dev-server
```

### Translations

Translations are in `config/locales`.

We use `i18n-tasks` to keep them clean, with a few custom rake tasks:

- `rake i18n:add-model-attributes` extracts attributes names from
  database tables and saves them in `app/i18n/model_attributes.i18n`
- `rake i18n:add-missing` is a wrapper around `i18n-tasks add-missing`
  that prefixes new strings with `TRANSLATE_ME`

### Running tests

This project uses various tools to enforce code consistency and to run
tests:

- [Rubocop](http://batsov.com/rubocop/) to check the code style. Run
  `bundle exec rubocop` or `bundle exec rubocop -a` to fix some errors
  automatically.
- [Brakeman](https://brakemanscanner.org/), to check the code against
  known vulnerabilities and possible security issues. You can launch it
  locally with `bundle exec brakeman`.
- [RSpec](http://rspec.org/) for unit test. You can launch it locally
  with `bundle exec rspec`.
- [RSpec-Rails-API](https://gitlab.com/experimentslabs/rspec-rails-api)
  to test JSON views (it also can generate Swagger documentation, but
  this is not enabled here)
- [Cucumber](https://cucumber.io/) for integration tests. Run `bundle
  exec cucumber` locally to test the code.
- [Eslint](https://eslint.org/) for JS code style. Launch `yarn run
  lint:js` or `yarn run lint:js-fix` to check locally. It's not perfect,
  but it helps.
- [StyleLint](http://stylelint.io/) for SCSS code style. Run `yarn run
  lint:sass` or `yarn run lint:sass-fix` locally.
- [Haml-lint](https://github.com/brigade/haml-lint) for views
  consistency. Run `bundle exec haml-lint`.

We use a few helper gems for our tests:

- FactoryBot (factories are in `/spec/factories`)
- Faker

#### Cucumber

Cucumber features (in `/features`).

You can specify the browser with `CAPYBARA_BROWSER` environment
variable:

```sh
CAPYBARA_BROWSER=chrome bundle exec cucumber
```

Check `/features/support/configuration.rb` to see supported ones.

##### Feature terms

There are the different terms used in the features for clarity and
reference:

- `user`: someone visiting the website, registered or not
- `visitor`: unregistered user
- `member`: registered user
- `maintainer`: website owner
- `administrator`: a member with admin rights

#### RSpec

RSpec tests are in `/spec`.

When writing tests, don't bother for views and routes tests; views are
tested with Cucumber. Routes aren't tested at all.

## Deployment

TODO

### Cron jobs

Sitemap should be generated regularly. Use whenever to set up the cron
jobs:

```sh
bundle exec whenever --update-crontab
```

## Shortcodes guide

Reference file: `app/helpers/shortcodes_helper.rb`

General behavior:

- If a card shortcode is made to some site content, the card content
  won't be visible in indexes to limit the requests
- If a card shortcode points to the content in which it is created, a
  warning is displayed instead, to avoid infinite loops
- If a card shortcode contains shortcodes too, these shortcodes won't be
  displayed to limit the requests and to keep the page clean

### Inline shortcodes

| Shortcode                       | Example                                                         | Description                                                  |
|:--------------------------------|:----------------------------------------------------------------|:-------------------------------------------------------------|
| `[album:<album-slug>]`          | `[album:my-first-album]`                                        | Link with album icon and name                                |
| `[article:<article-slug>]`      | `[article:my-first-article]`                                    | Link with article icon and title                             |
| `[note:<note-slug>]`            | `[note:2018-11-23-18-57-02-3]`                                  | Link with note icon and id                                   |
| `[project:<project-slug>]`      | `[project:my-first-project]`                                    | Link with project icon and name                              |
| `[upload:<upload-slug>]`        | `[upload:my-first-upload]`                                      | Link with upload icon and title                              |
| `[user:<username>]`             | `[user:jimmy]`                                                  | Link with avatar if any                                      |
| `[github-repo:<username/repo>]` | `[github-repo:el-cms/elabs]`                                    | Github icon, link to user, link to repo with a `/` separator |
| `[gitlab-repo:<link>]`          | `[gitlab-repo:https://gitlab.com/experimentslabs/engine_elabs]` | Gitlab icon, link to user, link to repo with a `/` separator |

Behavior:
- for site-content, if content is unpublished/inexistant, a warning is
  displayed instead
- for site-content, if content is NSFW, a warning is displayed,
  depending on current NSFW preference
- for Github/Gitlab links, if the targeted element does not exist, the
  links are created anyway.

### Cards

| Shortcode                           | Example                                                              | Description                                |
|:------------------------------------|:---------------------------------------------------------------------|:-------------------------------------------|
| `[album-card:<album-slug>]`         | `[album-card:my-first-album]`                                        | Simple card as the ones visible in indexes |
| `[article-card:<article-slug>]`     | `[article-card:my-first-article]`                                    | Simple card as the ones visible in indexes |
| `[note-card:<note-slug>]`           | `[note-card:2018-11-23-18-57-02-3]`                                  | Simple card as the ones visible in indexes |
| `[project-card:<project-slug>]`     | `[project-card:my-first-project]`                                    | Simple card as the ones visible in indexes |
| `[upload-card:<upload-slug>]`       | `[upload-card:my-first-upload]`                                      | Simple card as the ones visible in indexes |
| `[user-card:<username>]`            | `[user-card:jimmy]`                                                  | Simple card as the ones visible in indexes |
| `[github-repo-card:<username/repo>` | `[github-repo-card:el-cms/elabs]`                                    | Card with base information and description |
| `[github-user-card:<username>`      | `[github-user-card:mtancoigne]`                                      | Card with base information                 |
| `[gitlab-repo-card:<link>`          | `[gitlab-repo-card:https://gitlab.com/experimentslabs/engine_elabs]` | Card with base information and description |
| `[gitlab-user-card:<link>`          | `[gitlab-user-card:https://gitlab.com/mtancoigne]`                   | Card with base information                 |
| `[gitlab-group-card:<link>`         | `[gitlab-group-card:https://gitlab.com/experimentslabs]`             | Card with base information                 |

Behavior:
- for site-content, if content is unpublished/inexistant, a warning is
  displayed instead
- for site-content, if content is NSFW, a warning is displayed,
  depending on current NSFW preference
- for Github/Gitlab links, if the targeted element does not exist, an
  error is displayed.
- for Github cards, the visitor may reach a limit of cards displayed, as
  we don't send any developer key to github.

