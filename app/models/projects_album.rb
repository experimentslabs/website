class ProjectsAlbum < ApplicationRecord
  self.table_name = 'projects_albums'

  include CountablePivot

  COUNTABLE_DEFINITION = [:albums, :project].freeze

  belongs_to :project
  belongs_to :album
end
