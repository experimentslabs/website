class Preference < ApplicationRecord
  self.table_name = 'preferences'

  validates :locale, inclusion: { in: %w[en fr] }, allow_blank: true

  belongs_to :user
  belongs_to :writing_language, class_name: 'Language', optional: true
  belongs_to :writing_license, class_name: 'License', optional: true
end
