class Article < ApplicationContentRecord
  self.table_name = 'articles'

  SLUGGABLE_FIELD = :title
  COUNTER_CACHES = [
    %w[projects articles],
    %w[tags articles],
    'license',
    'language',
    'user',
  ].freeze

  validates :title,   presence: true
  validates :excerpt, presence: true
  validates :content, presence: true
  validates :slug,    presence: true, uniqueness: true
  validates_with AssociatedAuthorValidator, relations: %w[project]

  belongs_to :user
  belongs_to :license
  belongs_to :language
  has_many :articles_tags # rubocop:disable Rails/HasManyOrHasOneDependent
  has_many :projects_articles # rubocop:disable Rails/HasManyOrHasOneDependent
  has_many :tags,     through: :articles_tags,     dependent: :destroy
  has_many :projects, through: :projects_articles, dependent: :destroy
  has_many :comments, as: 'content', dependent: :destroy
  # Public filters
  has_many :public_projects, -> { publicly_visible }, through: :projects_articles, source: :project

  def title_to_display
    title
  end

  def short_text_to_display
    excerpt
  end

  def long_text_to_display
    content
  end
end
