class ArticlesTag < ApplicationRecord
  self.table_name = 'articles_tags'

  include CountablePivot

  COUNTABLE_DEFINITION = [:articles, :tag].freeze

  belongs_to :article
  belongs_to :tag
end
