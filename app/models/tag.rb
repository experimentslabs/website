class Tag < ApplicationRecord
  self.table_name = 'tags'
  include Sluggable

  SLUGGABLE_FIELD = :name
  SLUG_FIELD      = :slug

  validates :name, presence: true
  validates :slug, presence: true, uniqueness: true

  has_many :albums_tags # rubocop:disable Rails/HasManyOrHasOneDependent
  has_many :articles_tags # rubocop:disable Rails/HasManyOrHasOneDependent
  has_many :links_tags # rubocop:disable Rails/HasManyOrHasOneDependent
  has_many :notes_tags # rubocop:disable Rails/HasManyOrHasOneDependent
  has_many :projects_tags # rubocop:disable Rails/HasManyOrHasOneDependent
  has_many :uploads_tags # rubocop:disable Rails/HasManyOrHasOneDependent
  has_many :albums,   through: :albums_tags,     dependent: :destroy
  has_many :articles, through: :articles_tags,   dependent: :destroy
  has_many :links,    through: :links_tags,      dependent: :destroy
  has_many :notes,    through: :notes_tags,      dependent: :destroy
  has_many :projects, through: :projects_tags,   dependent: :destroy
  has_many :uploads,  through: :uploads_tags,    dependent: :destroy
  # Public filters
  has_many :public_albums,   -> { publicly_visible }, through: :albums_tags,   source: :album
  has_many :public_articles, -> { publicly_visible }, through: :articles_tags, source: :article
  has_many :public_links,    -> { publicly_visible }, through: :links_tags,    source: :link
  has_many :public_notes,    -> { publicly_visible }, through: :notes_tags,    source: :note
  has_many :public_projects, -> { publicly_visible }, through: :projects_tags, source: :project
  has_many :public_uploads,  -> { publicly_visible }, through: :uploads_tags,  source: :upload
end
