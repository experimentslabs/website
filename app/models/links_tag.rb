class LinksTag < ApplicationRecord
  self.table_name = 'links_tags'

  include CountablePivot

  COUNTABLE_DEFINITION = [:links, :tag].freeze

  belongs_to :link
  belongs_to :tag
end
