class Upload < ApplicationContentRecord
  self.table_name = 'uploads'

  SLUGGABLE_FIELD = :title
  COUNTER_CACHES = [
    %w[albums uploads],
    %w[projects uploads],
    %w[tags uploads],
    'license',
    'language',
    'user',
  ].freeze

  validates :title,           presence: true
  validates :description,     presence: true
  validates :slug,            presence: true, uniqueness: true
  validates_with AssociatedAuthorValidator, relations: %w[album project]
  validates_with FileUploadPresenceValidator, field: :file

  belongs_to :user
  belongs_to :license
  belongs_to :language
  has_many :uploads_tags # rubocop:disable Rails/HasManyOrHasOneDependent
  has_many :projects_uploads # rubocop:disable Rails/HasManyOrHasOneDependent
  has_many :albums_uploads # rubocop:disable Rails/HasManyOrHasOneDependent
  has_many :tags,     through: :uploads_tags,     dependent: :destroy
  has_many :projects, through: :projects_uploads, dependent: :destroy
  has_many :albums,   through: :albums_uploads,   dependent: :destroy
  has_many :comments, as: 'content', dependent: :destroy
  # Public filters
  has_many :public_albums,   -> { publicly_visible }, through: :albums_uploads,   source: :album
  has_many :public_projects, -> { publicly_visible }, through: :projects_uploads, source: :project

  has_one_attached :file

  scope :for_list, -> { order(:title).pluck(:title, :id) }

  def title_to_display
    title
  end

  def short_text_to_display
    nil
  end

  def long_text_to_display
    description
  end
end
