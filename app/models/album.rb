class Album < ApplicationContentRecord
  self.table_name = 'albums'

  SLUGGABLE_FIELD = :name
  COUNTER_CACHES = [
    %w[projects albums],
    %w[tags albums],
    'license',
    'language',
    'user',
  ].freeze

  validates :name,        presence: true
  validates :description, presence: true
  validates :slug,        presence: true, uniqueness: true
  validates_with AssociatedAuthorValidator, relations: %w[project upload]

  belongs_to :user
  belongs_to :license
  belongs_to :language
  has_many :albums_tags # rubocop:disable Rails/HasManyOrHasOneDependent
  has_many :projects_albums # rubocop:disable Rails/HasManyOrHasOneDependent
  has_many :albums_uploads # rubocop:disable Rails/HasManyOrHasOneDependent
  has_many :tags,     through: :albums_tags,     dependent: :destroy
  has_many :projects, through: :projects_albums, dependent: :destroy
  has_many :uploads,  through: :albums_uploads,  dependent: :destroy
  has_many :comments, as: 'content', dependent: :destroy
  # Public filters
  has_many :public_projects, -> { publicly_visible }, through: :projects_albums, source: :project
  has_many :public_uploads,  -> { publicly_visible }, through: :albums_uploads,  source: :upload

  scope :for_list, -> { order(:name).pluck(:name, :id) }

  def title_to_display
    name
  end

  def short_text_to_display
    nil
  end

  def long_text_to_display
    description
  end
end
