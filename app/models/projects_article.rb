class ProjectsArticle < ApplicationRecord
  self.table_name = 'projects_articles'

  include CountablePivot

  COUNTABLE_DEFINITION = [:articles, :project].freeze

  belongs_to :project
  belongs_to :article
end
