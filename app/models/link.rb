class Link < ApplicationRecord
  self.table_name = 'links'

  include ContentEntity
  include ActableEntity
  include CountableEntity
  include Taggable
  include NotifiableEntity

  ACT_UPDATES    = false
  COUNTER_CACHES = [
    %w[tags links],
    'language',
    'user',
  ].freeze

  validates :title, presence: true
  validates_with LinkValidator

  belongs_to :user
  belongs_to :language
  has_many :links_tags # rubocop:disable Rails/HasManyOrHasOneDependent
  has_many :tags, through: :links_tags, dependent: :destroy

  def title_to_display
    title
  end

  def short_text_to_display
    title
  end

  def long_text_to_display
    description
  end
end
