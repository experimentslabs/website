class AlbumsTag < ApplicationRecord
  self.table_name = 'albums_tags'

  include CountablePivot
  COUNTABLE_DEFINITION = [:albums, :tag].freeze

  belongs_to :album
  belongs_to :tag
end
