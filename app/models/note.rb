class Note < ApplicationContentRecord
  self.table_name = 'notes'

  SLUGGABLE_FIELD = :created_at
  COUNTER_CACHES = [
    %w[projects notes],
    %w[tags notes],
    'license',
    'language',
    'user',
  ].freeze

  validates :content, presence: true
  validates :slug,    presence: true, uniqueness: true
  validates_with AssociatedAuthorValidator, relations: %w[project]

  belongs_to :user
  belongs_to :license
  belongs_to :language
  has_many :notes_tags # rubocop:disable Rails/HasManyOrHasOneDependent
  has_many :projects_notes # rubocop:disable Rails/HasManyOrHasOneDependent
  has_many :tags,     through: :notes_tags,     dependent: :destroy
  has_many :projects, through: :projects_notes, dependent: :destroy
  has_many :comments, as: 'content', dependent: :destroy
  # Public filters
  has_many :public_projects, -> { publicly_visible }, through: :projects_notes, source: :project

  def title_to_display
    I18n.t('activerecord.attributes.note.title', date: I18n.l(created_at, format: :medium))
  end

  def short_text_to_display
    content
  end

  def long_text_to_display
    nil
  end
end
