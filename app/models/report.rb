class Report < ApplicationRecord
  self.table_name = 'reports'

  validates :reason, presence: true
  validates :user, presence: true, unless: :name
  validates :name, presence: true, unless: :user
  validates_with ReportValidator

  belongs_to :user, optional: true
  has_many :notifications, as: 'content', dependent: :destroy

  after_save :notify_admins

  def username
    user_id ? user.display_name : name
  end

  def notify_admins
    User.admins.each do |admin|
      Notification.create! content: self,
                           event:   :report,
                           user:    admin
    end
  end
end
