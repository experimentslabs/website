class Notification < ApplicationRecord
  self.table_name = 'notifications'
  belongs_to :content, polymorphic: true
  belongs_to :user
  belongs_to :source_user, class_name: 'User', optional: true

  scope :for_user, ->(id) { where(user_id: id) }

  after_save :send_notification_email

  private

  def send_notification_email
    case event
    when 'comment'
      send_comment_notification
    when 'report'
      send_report_notification
    when 'lock', 'unlock'
      send_lock_notification
    end
  end

  def send_comment_notification
    NotificationMailer.with(notification: self).comment_notification_email.deliver_later if user.preference.receive_comment_notifications
  end

  def send_report_notification
    NotificationMailer.with(notification: self).report_notification_email.deliver_later if user.preference.receive_admin_notifications
  end

  def send_lock_notification
    NotificationMailer.with(notification: self).lock_notification_email.deliver_later if user.preference.receive_content_notifications
  end
end
