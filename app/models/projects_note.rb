class ProjectsNote < ApplicationRecord
  self.table_name = 'projects_notes'

  include CountablePivot

  COUNTABLE_DEFINITION = [:notes, :project].freeze

  belongs_to :project
  belongs_to :note
end
