class Project < ApplicationContentRecord
  self.table_name = 'projects'

  SLUGGABLE_FIELD = :name
  COUNTER_CACHES = [
    %w[tags projects],
    'license',
    'language',
    'user',
  ].freeze

  validates :name,              presence: true
  validates :short_description, presence: true
  validates :description,       presence: true
  validates :slug,              presence: true, uniqueness: true
  validates_with AssociatedAuthorValidator, relations: %w[album article note upload]
  validate :logo_format

  belongs_to :user
  belongs_to :license
  belongs_to :language
  has_many :projects_tags # rubocop:disable Rails/HasManyOrHasOneDependent
  has_many :projects_albums # rubocop:disable Rails/HasManyOrHasOneDependent
  has_many :projects_articles # rubocop:disable Rails/HasManyOrHasOneDependent
  has_many :projects_notes # rubocop:disable Rails/HasManyOrHasOneDependent
  has_many :projects_uploads # rubocop:disable Rails/HasManyOrHasOneDependent
  has_many :tags,     through: :projects_tags,     dependent: :destroy
  has_many :albums,   through: :projects_albums,   dependent: :destroy
  has_many :articles, through: :projects_articles, dependent: :destroy
  has_many :notes,    through: :projects_notes,    dependent: :destroy
  has_many :uploads,  through: :projects_uploads,  dependent: :destroy
  has_many :comments, as: 'content', dependent: :destroy
  # Public filters
  has_many :public_albums,   -> { publicly_visible }, through: :projects_albums,   source: :album
  has_many :public_articles, -> { publicly_visible }, through: :projects_articles, source: :article
  has_many :public_notes,    -> { publicly_visible }, through: :projects_notes,    source: :note
  has_many :public_uploads,  -> { publicly_visible }, through: :projects_uploads,  source: :upload

  has_one_attached :logo

  scope :for_list, -> { order(:name).pluck(:name, :id) }

  def title_to_display
    name
  end

  def short_text_to_display
    short_description
  end

  def long_text_to_display
    description
  end

  private

  def logo_format
    return unless logo.attached?

    errors.add(:logo, I18n.t('model.project.needs_to_be_an_image')) unless %w[image/png image/jpeg image/gif image/webp].include? logo.blob.content_type
  end
end
