class License < ApplicationRecord
  self.table_name = 'licenses'
  include Sluggable

  SLUGGABLE_FIELD = :name
  SLUG_FIELD      = :slug

  validates :name, presence: true
  validates :url,  presence: true
  validates :icon, presence: true
  validates :slug, presence: true, uniqueness: true

  has_many :albums,   dependent: :restrict_with_error
  has_many :articles, dependent: :restrict_with_error
  has_many :notes,    dependent: :restrict_with_error
  has_many :projects, dependent: :restrict_with_error
  has_many :uploads,  dependent: :restrict_with_error
  # Public filters
  has_many :public_albums,   -> { publicly_visible }, class_name: 'Album',   inverse_of: :license
  has_many :public_articles, -> { publicly_visible }, class_name: 'Article', inverse_of: :license
  has_many :public_notes,    -> { publicly_visible }, class_name: 'Note',    inverse_of: :license
  has_many :public_projects, -> { publicly_visible }, class_name: 'Project', inverse_of: :license
  has_many :public_uploads,  -> { publicly_visible }, class_name: 'Upload',  inverse_of: :license

  scope :for_list, -> { order(:name).pluck(:name, :id) }
end
