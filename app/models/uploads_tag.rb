class UploadsTag < ApplicationRecord
  self.table_name = 'uploads_tags'

  include CountablePivot

  COUNTABLE_DEFINITION = [:uploads, :tag].freeze

  belongs_to :upload
  belongs_to :tag
end
