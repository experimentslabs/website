class Comment < ApplicationRecord
  self.table_name = 'comments'

  validates :comment, presence: true
  validates :user,    presence: true, unless: :name
  validates :name,    presence: true, unless: :user

  belongs_to :content,       polymorphic: true
  belongs_to :user,          optional: true
  has_many   :notifications, as: :content, dependent: :destroy

  after_create :notify_author, if: :notify_author?

  def username
    user_id ? user.display_name : name
  end

  def notify_author?
    user_id ? content.user_id != user_id : true
  end

  def notify_author
    notification = { content: self,
                     event:   :comment,
                     user_id: content.user_id }
    notification[:source_user] = user if user_id
    Notification.create! notification
  end

  def archive!
    update(archived: true)
    notifications.destroy_all
  end
end
