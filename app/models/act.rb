class Act < ApplicationRecord
  self.table_name = 'acts'

  belongs_to :content, polymorphic: true
end
