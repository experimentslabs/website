class AlbumsUpload < ApplicationRecord
  self.table_name = 'albums_uploads'

  include CountablePivot

  COUNTABLE_DEFINITION = [:uploads, :album].freeze

  belongs_to :album
  belongs_to :upload
end
