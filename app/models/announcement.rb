class Announcement < ApplicationRecord
  self.table_name = 'announcements'

  validates :level, inclusion: { in: %w[info warning danger] }, presence: true
  validates :target, inclusion: { in: %w[global public member admin] }, presence: true
  validates :content, presence: true

  before_save :clean_dates

  belongs_to :user

  scope :visible, lambda {
    now = Time.current
    where('start_at <= ? OR start_at IS NULL', now).where('end_at >= ? OR end_at IS NULL', now)
  }

  def self.for_user(user)
    return visible.where('target = ? OR target = ?', :global, :member) if user&.role == 'user'

    return visible.where('target = ? OR target = ? OR target = ?', :global, :member, :admin) if user&.role == 'admin'

    visible.where(target: :global)
  end

  private

  def clean_dates
    self.start_at = nil if start_at == ''
    self.end_at   = nil if end_at == ''
  end
end
