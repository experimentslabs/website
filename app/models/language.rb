class Language < ApplicationRecord
  self.table_name = 'languages'
  include Sluggable

  SLUGGABLE_FIELD = nil
  SLUG_FIELD = :iso639_1

  validates :iso639_1, presence: true
  validates :name,     presence: true

  has_many :albums,   dependent: :restrict_with_error
  has_many :articles, dependent: :restrict_with_error
  has_many :links,    dependent: :restrict_with_error
  has_many :notes,    dependent: :restrict_with_error
  has_many :projects, dependent: :restrict_with_error
  has_many :uploads,  dependent: :restrict_with_error
  # Public filters
  has_many :public_albums,   -> { publicly_visible }, class_name: 'Album',   inverse_of: :language
  has_many :public_articles, -> { publicly_visible }, class_name: 'Article', inverse_of: :language
  has_many :public_links,    -> { publicly_visible }, class_name: 'Link',    inverse_of: :language
  has_many :public_notes,    -> { publicly_visible }, class_name: 'Note',    inverse_of: :language
  has_many :public_projects, -> { publicly_visible }, class_name: 'Project', inverse_of: :language
  has_many :public_uploads,  -> { publicly_visible }, class_name: 'Upload',  inverse_of: :language

  scope :with_content_only, -> { with_albums.or(with_articles).or(with_links).or(with_notes).or(with_projects).or(with_uploads) }
  scope :available_site_translations, -> { select(:id, :name, :iso639_1).where(iso639_1: I18n.available_locales) }
  scope :for_list, -> { order(:name).pluck(:name, :id) }
  scope :with_albums, -> { where.not(albums_count: 0) }
  scope :with_articles, -> { where.not(articles_count: 0) }
  scope :with_links, -> { where.not(links_count: 0) }
  scope :with_notes, -> { where.not(notes_count: 0) }
  scope :with_projects, -> { where.not(projects_count: 0) }
  scope :with_uploads, -> { where.not(uploads_count: 0) }

  def to_param
    iso639_1
  end
end
