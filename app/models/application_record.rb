class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  attr_accessor :changed_by

  def self.find_by_slug_or_id(params)
    if sluggable?
      slug = params[:slug] || params["#{table_name.classify.downcase}_slug"]
      return find_by!(slug: slug)
    end

    find(params[:id] || params["#{table_name.classify.downcase}_id"])
  end

  def self.sluggable?
    const_defined?('SLUG_FIELD') && !self::SLUG_FIELD.nil?
  end
end
