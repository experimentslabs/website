class ProjectsTag < ApplicationRecord
  self.table_name = 'projects_tags'

  include CountablePivot

  COUNTABLE_DEFINITION = [:projects, :tag].freeze

  belongs_to :project
  belongs_to :tag
end
