class NotesTag < ApplicationRecord
  self.table_name = 'notes_tags'

  include CountablePivot

  COUNTABLE_DEFINITION = [:notes, :tag].freeze

  belongs_to :note
  belongs_to :tag
end
