class ProjectsUpload < ApplicationRecord
  self.table_name = 'projects_uploads'

  include CountablePivot

  COUNTABLE_DEFINITION = [:uploads, :project].freeze

  belongs_to :project
  belongs_to :upload
end
