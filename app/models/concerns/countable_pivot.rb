module CountablePivot
  extend ActiveSupport::Concern

  # List of tables to query for update
  COUNTABLE_DEFINITION = [].freeze

  included do
    before_destroy :update_associated_counter_cache
  end

  # rubocop:disable Rails/SkipsModelValidations
  # We don't want any hook to be called
  def update_associated_counter_cache
    entity = send(self.class::COUNTABLE_DEFINITION[1])
    entity.decrement! "#{self.class::COUNTABLE_DEFINITION[0]}_count"
  end
  # rubocop:enable Rails/SkipsModelValidations
end
