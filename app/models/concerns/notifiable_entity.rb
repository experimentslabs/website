module NotifiableEntity
  extend ActiveSupport::Concern

  included do
    before_save :create_notification?, if: :changed_by_someone_else
    after_save :create_notification, if: :changed_by_someone_else

    after_destroy :create_destroy_notifications, if: :changed_by_someone_else
  end

  private

  def create_notification?
    @action = if changed.include?('locked')
                current_lock_action
              else
                :nothing
              end
  end

  def create_notification
    return if @action == :nothing

    Notification.create(
      content:     self,
      event:       @action,
      user:        user,
      source_user: changed_by
    )
  end

  # TODO: check if used ? (dependent: destroy may do the trick.)
  def create_destroy_notifications
    Notification.where(content: self).destroy_all
    Notification.create(
      content:     self,
      event:       :delete,
      user:        user,
      source_user: changed_by
    )
  end
end
