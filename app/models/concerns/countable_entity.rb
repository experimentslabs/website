module CountableEntity
  extend ActiveSupport::Concern

  # List of lists like ['albums', 'uploads'] if 'uploads_counter' is in 'albums' table
  COUNTER_CACHES = [].freeze

  included do
    after_save :update_counter_caches
    before_destroy :decrement_counter_caches
  end

  private

  # rubocop:disable Rails/SkipsModelValidations, Metrics/AbcSize
  # We don't want any hook to be called
  def update_counter_caches
    self.class::COUNTER_CACHES.each do |relation|
      if relation.is_a? Array
        send(relation[0]).each do |entity|
          entity.update_column "#{relation[1]}_count", entity.send("public_#{relation[1]}").count
        end
      else
        table_name     = self.class.table_name
        entity         = send(relation)
        relation_field = "#{table_name}_count"
        entity.update_column relation_field, entity.send("public_#{table_name}").count
      end
    end
  end
  # rubocop:enable Rails/SkipsModelValidations, Metrics/AbcSize

  # rubocop:disable Rails/SkipsModelValidations
  # We don't want any hook to be called
  def decrement_counter_caches
    self.class::COUNTER_CACHES.each do |relation|
      next if relation.is_a? Array

      send(relation).decrement! "#{self.class.table_name}_count"
    end
  end
  # rubocop:enable Rails/SkipsModelValidations
end
