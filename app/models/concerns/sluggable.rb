# For this concern to work, a SLUGGABLE_FIELD constant should be declared in the model
module Sluggable
  extend ActiveSupport::Concern

  included do
    before_validation :fill_slug, on: :create
  end

  def fill_slug
    return unless self.class::SLUGGABLE_FIELD

    slug       = make_slug_from_field
    slug_field = self.class::SLUG_FIELD || :slug

    same_slugs = self.class
                     .unscoped
                     .where("#{slug_field} LIKE ?", "#{slug}%")
                     .pluck(slug_field)
                     .count { |r| r[/^#{slug}(-\d+)?$/] }

    slug += "-#{same_slugs}" if same_slugs.positive?

    self.slug = slug
  end

  def to_param
    slug
  end

  private

  def make_slug_from_field
    return Time.current.strftime('%Y-%m-%d-%H-%M-%S') if self.class::SLUGGABLE_FIELD == :created_at

    send(self.class::SLUGGABLE_FIELD).parameterize
  end
end
