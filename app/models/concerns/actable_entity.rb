module ActableEntity
  extend ActiveSupport::Concern

  included do
    attr_accessor :update_description
    attr_reader :minor_update

    has_many :acts, as: :content, dependent: :destroy

    before_save :update_acts?
    after_save :update_acts
  end

  def minor_update=(value)
    @minor_update = ['t', '1', 1, true].include? value
  end

  def minor_update?
    @minor_update || false
  end

  private

  def update_acts? # rubocop:disable Metrics/CyclomaticComplexity, Metrics/PerceivedComplexity
    @action = if minor_update?
                :nothing
              elsif just_published?
                current_publish_action
              elsif made_locked? && act_updates?
                current_lock_action
              elsif notably_changed? && act_updates?
                current_update_action
              else
                :nothing
              end
  end

  def update_acts
    acts.destroy_all if [:lock, :unpublish].include? @action
    reason = update_description.nil? ? nil : update_description
    Act.create content: self, event: @action, reason: reason unless @action == :nothing || hidden_in_history
  end

  # Whether or not to save the update acts for the current model
  def act_updates?
    self.class::ACT_UPDATES
  end

  def made_locked?
    changed.include?('locked') && published?
  end

  def notably_changed?
    (changed & %w[title name description short_description content excerpt]).any?
  end

  def just_published?
    changed.include?('published')
  end
end
