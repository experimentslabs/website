module Taggable
  extend ActiveSupport::Concern

  included do
    attr_writer :tags_list

    after_save :save_tags_list
  end

  def save_tags_list
    if @tags_list
      tags = []
      @tags_list.split(/, */).each do |t|
        tags.push Tag.find_or_create_by(name: t.chomp)
      end

      @tags_list = nil

      update(tags: tags) if tags.count.positive?
    end

    true
  end

  # Returns a list of comma-separated tags
  def tags_list
    @tags_list || tags.map(&:name).join(', ')
  end
end
