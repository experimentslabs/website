class ApplicationContentRecord < ApplicationRecord
  include ContentEntity
  include ActableEntity
  include CountableEntity
  include NotifiableEntity
  include Taggable
  include Sluggable

  SLUG_FIELD  = :slug
  ACT_UPDATES = true

  self.abstract_class = true
end
