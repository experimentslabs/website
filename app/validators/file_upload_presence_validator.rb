class FileUploadPresenceValidator < ActiveModel::Validator
  def validate(record)
    record.errors[options[:field]] << I18n.t('errors.messages.blank') unless record.send(options[:field]).attached?
  end
end
