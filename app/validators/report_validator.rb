class ReportValidator < ActiveModel::Validator
  def validate(record)
    Rails.application.routes.recognize_path record.url
  rescue ActionController::RoutingError
    record.errors[:url] << I18n.t('validator.report_validator.invalid_url')
  end
end
