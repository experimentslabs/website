class AssociatedAuthorValidator < ActiveModel::Validator
  def validate(record)
    options[:relations].each do |association|
      record.send(association.pluralize).each do |element|
        record.errors[:base] << I18n.t('validator.associated_author_validator.not_owned_model', model: association) if element.user_id != record.user_id
      end
    end
  end
end
