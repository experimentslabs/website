class LinkValidator < ActiveModel::Validator
  def validate(record)
    regex = %r{^(https?|s?ftp)://.+}
    record.errors[:url] << I18n.t('validator.link_validator.invalid_url') unless regex.match record.url
  end
end
