export default {
  removeClass (element, className) {
    if (element.classList) {
      element.classList.remove(className)
    } else {
      element.className = element.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ')
    }
  },
  addClass (element, className) {
    if (element.classList) {
      element.classList.add(className)
    } else {
      element.className += ` ${className}`
    }
  },

  hasClass (element, className) {
    if (element.classList) {
      return element.classList.contains(className)
    }
    return (` ${element.className} `).indexOf(` ${className} `) > -1
  },

  toggleClass (element, className) {
    if (this.hasClass(element, className)) {
      this.removeClass(element, className)
    } else {
      this.addClass(element, className)
    }
  },

  documentReady (fn) {
    if (document.attachEvent ? document.readyState === 'complete' : document.readyState !== 'loading') {
      fn()
    } else {
      document.addEventListener('DOMContentLoaded', fn)
    }
  },
  ajax (method, url, data, success, failure, error) {
    const request = new window.XMLHttpRequest()
    request.open(method, url, true)
    request.setRequestHeader('Accept', 'application/json')

    request.onload = function () {
      if (request.status >= 200 && request.status < 400) {
        if (typeof success === 'function') success(request)
      } else {
        if (typeof failure === 'function') failure(request)
      }
    }
    request.onerror = function () {
      if (typeof error === 'function') error(request)
    }
    request.send(data)
  },
}
