import CodeMirror from 'codemirror/lib/codemirror'
import 'codemirror/mode/gfm/gfm'

window.initializeCodeMirrorIn = function (element) {
  return CodeMirror.fromTextArea(element, {
    lineNumbers: true,
    mode: {
      name: 'gfm',
      tokenTypeOverrides: {
        emoji: 'emoji',
      },
    },
    theme: 'base16-light',
  })
}

window.limitCMToLength = function (cm, change) {
  const maxLength = cm.getOption('maxLength')
  if (maxLength && change.update) {
    let str = change.text.join('\n')
    let delta = str.length - (cm.indexFromPos(change.to) - cm.indexFromPos(change.from))
    if (delta <= 0) { return true }
    delta = cm.getValue().length + delta - maxLength
    if (delta > 0) {
      str = str.substr(0, str.length - delta)
      change.update(change.from, change.to, str.split('\n'))
    }
  }
  return true
}
