import h from './helpers'

window.hideAnnounce = function (id) {
  let announces = window.localStorage.getItem('hiddenAnnounces')

  if (announces) {
    announces = JSON.parse(announces)
  } else {
    announces = []
  }

  announces.push(id)

  window.localStorage.setItem('hiddenAnnounces', JSON.stringify(announces))
  h.addClass(document.getElementById(`announce-${id}`), 'hidden')
}

function hideAnnounces () {
  let announces = window.localStorage.getItem('hiddenAnnounces')

  if (!announces) return

  announces = JSON.parse(announces)

  for (var a in announces) {
    const element = document.getElementById(`announce-${announces[a]}`)
    if (element) h.addClass(element, 'hidden')
  }
}

// eslint-disable-next-line no-undef
h.documentReady(function () {
  hideAnnounces()
})
