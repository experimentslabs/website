import h from './helpers'

window.openModal = function (e, id) {
  if (!id) {
    id = e
  } else {
    e.preventDefault()
  }
  h.addClass(document.getElementById(id), 'modal--visible')
}

// eslint-disable-next-line no-unused-vars
window.closeModal = function (e, id) {
  if (!id) {
    id = e
  } else {
    e.preventDefault()
  }
  h.removeClass(document.getElementById(id), 'modal--visible')
}
