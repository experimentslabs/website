import h from './helpers'

window.selectTab = function (event, tab) {
  if (tab) { event.preventDefault() } else { tab = event }

  const tabs = document.getElementsByClassName('tabs__tabs-list__tab active')
  const tabsContent = document.getElementsByClassName('tabs__tab-content active')

  for (let i = 0; i < tabs.length; i++) {
    h.removeClass(tabs[i], 'active')
  }

  for (let i = 0; i < tabsContent.length; i++) {
    h.removeClass(tabsContent[i], 'active')
  }
  h.addClass(document.getElementById('tab-content-' + tab), 'active')
  h.addClass(document.getElementById('tab-' + tab), 'active')
}

window.initializeTabs = function () {
  const tabs = document.getElementsByClassName('tabs__tab-content')

  for (let i = 0; i < tabs.length; i++) {
    h.addClass(tabs[i], 'tab--initialized')
  }
}
