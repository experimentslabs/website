window.toggleUpdateMessageField = function (event, element) {
  const checked = event.target.checked
  if (checked) {
    document.getElementById(element).setAttribute('disabled', 'disabled')
  } else {
    document.getElementById(element).removeAttribute('disabled')
  }
}
