import './modals'
import h from './helpers'

// eslint-disable-next-line no-unused-vars
window.createMDPreviewer = function (cmInstance) {
  return function (event) {
    event.preventDefault()
    const targetElement = document.getElementById('content-description-preview')
    const data = new window.FormData()
    data.append('text', cmInstance.getValue())

    // eslint-disable-next-line no-undef
    h.ajax('POST', '/member/markdown_previewer', data, function (request) {
      targetElement.innerHTML = JSON.parse(request.responseText).html_content
    }, function () {
      targetElement.innerHTML = 'Something bad happened on our side. Sorry for that…'
    }, function () {
      targetElement.innerHTML = 'Something bad happened on our side. Sorry for that…'
    })

    window.openModal('content-preview-modal')
  }
}
