import h from 'tools/helpers'

import hljs from 'highlight.js/lib/highlight'
import apache from 'highlight.js/lib/languages/apache'
import bash from 'highlight.js/lib/languages/bash'
import cSharp from 'highlight.js/lib/languages/cs'
import cPP from 'highlight.js/lib/languages/cpp'
import css from 'highlight.js/lib/languages/css'
import coffeeScript from 'highlight.js/lib/languages/coffeescript'
import diff from 'highlight.js/lib/languages/diff'
import xml from 'highlight.js/lib/languages/xml'
import http from 'highlight.js/lib/languages/http'
import ini from 'highlight.js/lib/languages/ini'
import json from 'highlight.js/lib/languages/json'
import javascript from 'highlight.js/lib/languages/javascript'
import makefile from 'highlight.js/lib/languages/makefile'
import markdown from 'highlight.js/lib/languages/markdown'
import nginx from 'highlight.js/lib/languages/nginx'
import php from 'highlight.js/lib/languages/php'
import python from 'highlight.js/lib/languages/python'
import ruby from 'highlight.js/lib/languages/ruby'
import sql from 'highlight.js/lib/languages/sql'
import shellSession from 'highlight.js/lib/languages/shell'
import dockerFile from 'highlight.js/lib/languages/dockerfile'
import erb from 'highlight.js/lib/languages/erb'
import haml from 'highlight.js/lib/languages/haml'
import puppet from 'highlight.js/lib/languages/puppet'
import scss from 'highlight.js/lib/languages/scss'
import typeScript from 'highlight.js/lib/languages/typescript'
import yaml from 'highlight.js/lib/languages/yaml'

hljs.registerLanguage('apache', apache)
hljs.registerLanguage('bash', bash)
hljs.registerLanguage('cSharp', cSharp)
hljs.registerLanguage('cPP', cPP)
hljs.registerLanguage('css', css)
hljs.registerLanguage('coffeescript', coffeeScript)
hljs.registerLanguage('diff', diff)
hljs.registerLanguage('xml', xml)
hljs.registerLanguage('http', http)
hljs.registerLanguage('ini', ini)
hljs.registerLanguage('json', json)
hljs.registerLanguage('javascript', javascript)
hljs.registerLanguage('makefile', makefile)
hljs.registerLanguage('markdown', markdown)
hljs.registerLanguage('nginx', nginx)
hljs.registerLanguage('php', php)
hljs.registerLanguage('python', python)
hljs.registerLanguage('ruby', ruby)
hljs.registerLanguage('sql', sql)
hljs.registerLanguage('shell session', shellSession)
hljs.registerLanguage('dockerfile', dockerFile)
hljs.registerLanguage('erb', erb)
hljs.registerLanguage('haml', haml)
hljs.registerLanguage('puppet', puppet)
hljs.registerLanguage('scss', scss)
hljs.registerLanguage('typescript', typeScript)
hljs.registerLanguage('yaml', yaml)

h.documentReady(function () {
  hljs.initHighlighting()
})
