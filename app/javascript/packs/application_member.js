// Updates "update reason" area on content forms
import '../tools/forms'

// Codemirror editor
import '../tools/codemirror-editor'
import 'codemirror/lib/codemirror.css'
import 'codemirror/theme/base16-light.css'

// Preview markdown from text area
import '../tools/markdown-previewer'
