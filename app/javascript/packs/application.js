import h from '../tools/helpers'
// Hide announcements
import '../tools/announcements'
import '../tools/hljs'
import '../tools/menu'
import '../tools/tabs'
import '../tools/modals'
import vue from 'vue/dist/vue.min'

// Styles
import 'highlight.js/styles/a11y-light.css'
// Application style. Should be loaded last to override other libs
import '../../assets/stylesheets/application.scss'

require('@rails/ujs').start()
require('@rails/activestorage').start()
require('channels')

window.documentReady = h.documentReady
window.Vue = vue

// Uncomment to copy all static images under ../images to the output folder and reference
// them with the image_pack_tag helper in views (e.g <%= image_pack_tag 'rails.png' %>)
// or the `imagePath` JavaScript helper below.
//
// const images = require.context('../images', true)
// const imagePath = (name) => images(name, true)
