module LinksHelper
  def show_online_link(entity)
    options = {
      title: t('links_helper.show_online_link.view_online'),
      class: 'btn btn--small btn--show',
    }
    if entity.respond_to?(:publicly_visible?) && !entity.publicly_visible?
      options[:disabled] = true
      return content_tag 'a', icon('eye', ['fw']), options
    end
    link_to entity, options do
      icon('eye', ['fw'])
    end
  end

  def namespace_show_link(namespace, singular_model, entity)
    link_to send("#{namespace}_#{singular_model}_path", entity),
            title: t('links_helper.namespace_show_link.view'),
            class: 'btn btn--small btn--warning' do
      icon('eye', ['fw'])
    end
  end

  def namespace_edit_link(namespace, singular_model, entity)
    link_to send("edit_#{namespace}_#{singular_model}_path", entity),
            title: t('links_helper.namespace_edit_link.edit'),
            class: 'btn btn--small btn--edit' do
      icon('pencil-alt', ['fw'])
    end
  end

  def namespace_destroy_link(namespace, singular_model, entity, confirm = true)
    link_params = {
      title:  t('links_helper.namespace_destroy_link.destroy'),
      method: :delete,
      class:  'btn btn--small btn--destroy',
    }
    link_params[:data] = { confirm: t('links_helper.namespace_destroy_link.are_you_sure') } if confirm
    link_to send("#{namespace}_#{singular_model}_path".to_sym, entity), link_params do
      icon('trash', ['fw'])
    end
  end

  def entity_nested_resource_url(entity, nested_type)
    entity_type = singular_type_name entity
    send "#{entity_type}_#{nested_type}_url", entity
  end

  def related_content_show_all_link(amount, link)
    if amount.positive?
      link_to(t('links_helper.related_content_show_all_link.show_all'), link, class: 'btn btn--small btn--link btn--primary')
    else
      content_tag('a', t('links_helper.related_content_show_all_link.show_all'), disabled: true, class: 'btn btn--small btn--link')
    end
  end
end
