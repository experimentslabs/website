module ThumbnailsHelper
  THUMBNAILS_DIMENSIONS = {
    small:  '50x50',
    medium: '300x300',
  }.freeze

  AVATAR_DIMENSIONS = {
    large:  '512',
    medium: '256',
    small:  '64',
    tiny:   '32',
  }.freeze

  def thumbnail_center_crop(document, size = :medium)
    return resize_and_center_crop(document, AVATAR_DIMENSIONS[size]) if document.representable?

    thumbnail_preview_placeholder_file
  end

  def thumbnail_preview_placeholder_file
    '/assets/preview_placeholder.png'
  end

  def image_resize_properties(size = :page)
    dimensions = case size
                 when :large
                   '>1920'
                 else
                   '>960' # page size
                 end

    { resize: dimensions }
  end

  def avatar_image(avatar, size = :medium)
    resize_and_center_crop avatar, AVATAR_DIMENSIONS[size]
  end

  def resize_and_center_crop(entity, size)
    options = { resize_to_fill: [size, size, { gravity: 'Center' }] }
    url_for(entity.representation(options).processed)
  end
end
