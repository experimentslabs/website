module ContentHelper
  def show_item?(entity)
    (entity.sfw? || show_nsfw?) && !entity.locked?
  end

  def user_is_author_of(entity)
    current_user&.id && entity.user_id == current_user.id
  end

  def content_association_order
    [:projects, :articles, :notes, :albums, :uploads, :links]
  end
end
