module FormHelper
  def album_selector(form, field = :album_ids, multiple: true)
    relation_selector(form, field, Album.by_member(current_user).for_list, multiple)
  end

  def language_selector(form, field = :language_id, multiple: false)
    relation_selector(form, field, Language.for_list, multiple)
  end

  def license_selector(form, field = :license_id, multiple: false)
    relation_selector(form, field, License.for_list, multiple)
  end

  def projects_selector(form, field = :project_ids, multiple: true)
    relation_selector(form, field, Project.by_member(current_user).for_list, multiple)
  end

  def uploads_selector(form, field = :upload_ids, multiple: true)
    relation_selector(form, field, Upload.by_member(current_user).for_list, multiple)
  end

  def relation_selector(form, field, values, multiple = false)
    input_class = multiple ? 'is-multiple' : ''
    form.select field, values, {}, multiple: multiple, id: selector_id(form, field), class: input_class
  end

  private

  def selector_id(form, field)
    field_name = field.to_s.sub(/(.*)_id(s?)/, '\1\2')
    "#{form.object_name}_#{field_name}"
  end
end
