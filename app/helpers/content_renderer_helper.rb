module ContentRendererHelper
  def markdown(text, reference_entity = nil, show_short_codes = true)
    return '' if text.nil?

    # rubocop:disable Rails/OutputSafety
    # Issue may come from the CommonMarker configuration.
    # https://github.com/gjtorikian/commonmarker#options
    process_short_codes CommonMarker.render_html(text, [:DEFAULT], [:table]).html_safe,
                        reference_entity,
                        show_short_codes
    # rubocop:enable Rails/OutputSafety
  end

  def render_entity_content(entity, field, show_short_codes = true)
    markdown entity.send(field), entity, show_short_codes
  end

  def render_content_card(entity)
    lookup_directory = entity.class.name.underscore.pluralize
    partial_name = 'content_card'

    if lookup_context.exists?('content_card', lookup_directory, true)
      render "#{lookup_directory}/#{partial_name}", entity: entity
    else
      render "layouts/#{partial_name}", entity: entity
    end
  end
end
