module ContentFiltersHelper
  ALLOWED_FILTERS = %w[
    acts
    albums
    articles
    languages
    licenses
    links
    notes
    projects
    tags
    uploads
    users
  ].freeze

  def order_link(title, field)
    param_name            = "order_by_#{field}"
    direction             = params[param_name] && params[param_name] == 'asc' ? :desc : :asc
    options               = { param_name => direction }
    options['sfw_status'] = params['sfw_status'] if params['sfw_status']

    link_icon = determine_sort_icon param_name, direction

    link_to options, class: 'filter-link' do
      icon_text link_icon, content_tag('span', title, class: 'filter-link__link'), %w[fw filter-link__icon], true
    end
  end

  def sfw_status_links
    out = []

    out.push all_sfw_status_link
    out.push sfw_only_link
    out.push nsfw_only_link

    # rubocop:disable Rails/OutputSafety
    # These are all links created with "link_to", with titles from i18n
    out.join('').html_safe
    # rubocop:enable Rails/OutputSafety
  end

  def sfw_only_link
    link_icon = params['sfw_status'] && params['sfw_status'] == 'sfw_only' ? 'dot-circle' : 'circle'
    link_to request.params.merge(sfw_status: 'sfw_only'), class: 'filter-link' do
      icon_text link_icon, content_tag('span', t('content_filters_helper.sfw_only_link.only_sfw'), class: 'filter-link__link'), %w[fw filter-link__icon]
    end
  end

  def nsfw_only_link
    link_icon = params['sfw_status'] && params['sfw_status'] == 'nsfw_only' ? 'dot-circle' : 'circle'
    link_to request.params.merge(sfw_status: 'nsfw_only'), class: 'filter-link' do
      icon_text link_icon, content_tag('span', t('content_filters_helper.nsfw_only_link.only_nsfw'), class: 'filter-link__link'), %w[fw filter-link__icon]
    end
  end

  def all_sfw_status_link
    link_icon = params['sfw_status'] && %w[sfw_only nsfw_only].include?(params['sfw_status']) ? 'circle' : 'dot-circle'
    link_to request.params.merge(sfw_status: 'all'), class: 'filter-link' do
      icon_text link_icon, content_tag('span', t('content_filters_helper.all_sfw_status_link.show_all'), class: 'filter-link__link'), %w[fw filter-link__icon]
    end
  end

  def all_filter_link
    icon = params['with_content_only'] && params['with_content_only'] == 'false' ? 'dot-circle' : 'circle'
    link_to request.params.merge(with_content_only: 'false'), class: 'filter-link' do
      icon_text(icon, t('content_filters_helper.all_filter_link.show_all'), ['fw'], true)
    end
  end

  def admin_order_by_link(title, field)
    current_direction = params['order_by'] == field && params['direction'] ? params['direction'] : 'desc'
    direction         = current_direction == 'asc' ? :desc : :asc
    options           = { order_by: field, direction: direction }

    icon = direction == :asc ? 'sort-up' : 'sort-down'

    link_to options, class: 'filter-link' do
      icon_text icon, title, ['fw'], true
    end
  end

  alias member_order_by_link admin_order_by_link

  def render_filters?
    ALLOWED_FILTERS.include?(controller_name) && action_name == 'index'
  end

  private

  def determine_sort_icon(field, direction)
    return 'sort-up' if params[field] && direction == :asc
    return 'sort-down' if params[field] && direction == :desc

    'minus'
  end
end
