module ShortcodesHelper
  SHORT_CODES = {
    /\[album:([\w-]+)\]/                                       => [:shortcode_content, { type: :album, size: :inline }],
    /\[article:([\w-]+)\]/                                     => [:shortcode_content, { type: :article, size: :inline }],
    /\[note:([\w-]+)\]/                                        => [:shortcode_content, { type: :note, size: :inline }],
    /\[project:([\w-]+)\]/                                     => [:shortcode_content, { type: :project, size: :inline }],
    /\[upload:([\w-]+)\]/                                      => [:shortcode_content, { type: :upload, size: :inline }],
    /\[user:([a-zA-Z0-9]+([_-]?[a-zA-Z0-9]+)?)\]/              => [:shortcode_content, { type: :user, size: :inline }],
    /\[album-card:([\w-]+)\]/                                  => [:shortcode_content, { type: :album, size: :large }],
    /\[article-card:([\w-]+)\]/                                => [:shortcode_content, { type: :article, size: :large }],
    /\[note-card:([\w-]+)\]/                                   => [:shortcode_content, { type: :note, size: :large }],
    /\[project-card:([\w-]+)\]/                                => [:shortcode_content, { type: :project, size: :large }],
    /\[upload-card:([\w-]+)\]/                                 => [:shortcode_content, { type: :upload, size: :large }],
    /\[user-card:([a-zA-Z0-9]+([_-]?[a-zA-Z0-9]+)?)\]/         => [:shortcode_content, { type: :user, size: :large }],
    %r{\[github-repo:([a-zA-Z0-9\-_]+/[a-zA-Z0-9\-_]+)\]}      => [:shortcode_github_repo_inline, { size: :inline }],
    %r{\[github-repo-card:([a-zA-Z0-9\-_]+/[a-zA-Z0-9\-_]+)\]} => [:shortcode_github_repo, {}],
    /\[github-user-card:([a-zA-Z0-9\-_]+)\]/                   => [:shortcode_github_user, {}],
    %r{\[gitlab-repo:(https://.*)\]}                           => [:shortcode_gitlab_repo_inline, { size: :inline }],
    %r{\[gitlab-repo-card:(https://.*)\]}                      => [:shortcode_gitlab_repo, {}],
    %r{\[gitlab-user-card:(https://.*)\]}                      => [:shortcode_gitlab_user, {}],
    %r{\[gitlab-group-card:(https://.*)\]}                     => [:shortcode_gitlab_group, {}],
  }.freeze

  def process_short_codes(text, original_entity = nil, display = true)
    SHORT_CODES.each do |key, method|
      text.gsub! key do |match|
        size = method[1].key?(:size) ? method[1][:size] : :inline
        if display || size == :inline
          send(method[0], original_entity, match.sub(key, '\1'), method[1])
        else
          render(partial: 'layouts/shortcodes/open_to_see') unless display
        end
      end
    end

    # rubocop:disable Rails/OutputSafety
    # Rendered elements are safe; the only possibility is to have a broken URL
    # making broken API calls and having a dead widget.
    text.html_safe
    # rubocop:enable Rails/OutputSafety
  end

  private

  def shortcode_content(original_entity, slug, options = { size: :large })
    type        = options[:type].to_s
    model       = model_from_entity type
    widget_size = options[:size] == :inline ? '_inline' : ''

    return render "layouts/shortcodes/infinite_loop#{widget_size}", format: :html if shortcode_may_cause_loop?(model, slug, original_entity)

    entity = find_entity_for_shortcode(model, slug)
    return render "layouts/shortcodes/missing_content#{widget_size}", format: :html unless entity

    render_shortcode_widget(model, type, entity, widget_size)
  end

  def shortcode_github_repo(_original_entity, repo_url, _options = {})
    render partial: 'layouts/shortcodes/widgets/github_repo_card', locals: { repo_url: repo_url }
  end

  def shortcode_github_repo_inline(_original_entity, repo_url, _options = {})
    url_matches = %r{([a-zA-Z0-9\-_]+)/([a-zA-Z0-9\-_]+)}.match repo_url
    render partial: 'layouts/shortcodes/widgets/github_repo_inline', locals: { url_matches: url_matches }
  end

  def shortcode_github_user(_original_entity, user_name, _options = {})
    render partial: 'layouts/shortcodes/widgets/github_user_card', locals: { user_name: user_name }
  end

  def shortcode_gitlab_repo(_original_entity, repo_url, _options = {})
    url_matches = %r{(?:https://)?([a-z0-9\-_\.]+)/(.+)}.match repo_url
    render partial: 'layouts/shortcodes/widgets/gitlab_repo_card', locals: { url_matches: url_matches }
  end

  def shortcode_gitlab_repo_inline(_original_entity, repo_url, _options = {})
    url_matches = %r{(.*)/([a-zA-Z0-9\-_]+)/([a-zA-Z0-9\-_]+)}.match repo_url
    render partial: 'layouts/shortcodes/widgets/gitlab_repo_inline', locals: { url_matches: url_matches }
  end

  def shortcode_gitlab_user(_original_entity, user_url, _options = {})
    url_matches = %r{(?:https://)?([a-z0-9\-_\.]*)/([a-zA-Z0-9\-_]+)}.match user_url
    render partial: 'layouts/shortcodes/widgets/gitlab_user_card', locals: { url_matches: url_matches }
  end

  def shortcode_gitlab_group(_original_entity, group_url, _options = {})
    url_matches = %r{(?:https://)?([a-z0-9\-_\.]*)/([a-zA-Z0-9\-_]+)}.match group_url
    render partial: 'layouts/shortcodes/widgets/gitlab_group_card', locals: { url_matches: url_matches }
  end

  def shortcode_may_cause_loop?(model, slug, original_entity)
    original_entity && original_entity[model::SLUG_FIELD] == slug && original_entity.class == model
  end

  def find_entity_for_shortcode(model, slug)
    return model.where("#{model::SLUG_FIELD}": slug).publicly_visible.first if shortcode_for_content? model

    model.find_by("#{model::SLUG_FIELD}": slug)
  end

  def shortcode_for_content?(model)
    %w[Article Album Note Project Upload].include? model.to_s
  end

  def render_shortcode_widget(model, type, entity, widget_size)
    return render partial: "#{type.pluralize}/#{type}_inline", locals: { type.to_sym => entity }, format: :html if widget_size == '_inline'
    return render partial: 'layouts/content_card', locals: { entity: entity }, format: :html if shortcode_for_content? model

    render partial: "#{type.pluralize}/#{type}#{widget_size}", locals: { type.to_sym => entity }, format: :html
  end
end
