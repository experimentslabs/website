module ActsHelper
  def act_action(action)
    actions = {
      create:    t('acts_helper.act_action.created'),
      update:    t('acts_helper.act_action.updated'),
      lock:      t('acts_helper.act_action.locked'),
      unlock:    t('acts_helper.act_action.unlocked'),
      publish:   t('acts_helper.act_action.published'),
      unpublish: t('acts_helper.act_action.removed_from_publication'),
    }.freeze
    actions[action.to_sym] || "[missing: #{action}]"
  end

  def act_notice_string(act, include_link = true)
    type   = act.content_type.demodulize.underscore
    title  = if %w[unpublish destroy lock].include?(act.event) || !include_link
               html_escape act.content.title_to_display
             else
               link_to(act.content.title_to_display, act.content)
             end

    # rubocop:disable Rails/OutputSafety
    # "title" is the only threat here and it is escaped.
    t("acts_helper.act_notice_string.#{type}_#{act.event}_event", name: title).html_safe
    # rubocop:enable Rails/OutputSafety
  end
end
