module LinksAdminHelper
  def admin_action_links(singular_model, entity, except = [])
    out = []
    out.push(show_online_link(entity)) unless except.include? :show
    out.push(admin_lock_link(singular_model, entity)) unless except.include? :lock
    out.push(admin_destroy_link(singular_model, entity)) unless except.include? :destroy

    # rubocop:disable Rails/OutputSafety
    # These are all links created with "link_to", with titles from i18n
    out.join('').html_safe
    # rubocop:enable Rails/OutputSafety
  end

  def admin_show_link(singular_model, entity)
    namespace_show_link('admin', singular_model, entity)
  end

  def admin_lock_link(singular_model, entity)
    link_to send("admin_#{singular_model}_toggle_lock_path", entity),
            title:  (entity.locked? ? t('links_admin_helper.admin_lock_link.unlock') : t('links_admin_helper.admin_lock_link.lock')),
            class:  'btn btn--small btn--lock',
            method: :put do
      icon(entity.locked? ? 'unlock' : 'lock', ['fw'])
    end
  end

  def admin_destroy_link(singular_model, entity)
    namespace_destroy_link('admin', singular_model, entity)
  end

  def admin_edit_link(singular_model, entity)
    namespace_edit_link('admin', singular_model, entity)
  end
end
