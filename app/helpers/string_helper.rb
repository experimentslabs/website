module StringHelper
  # Create a text ellipsis at the given length.
  # max is the final length, with the ellipsis symbol included.
  def ellipsis(text, max, symbol = '…')
    ellipse_size = symbol.size

    return text if max <= ellipse_size || text.length <= max

    "#{text[0...(max - ellipse_size)]}#{symbol}"
  end

  def human_model_name(model, count = 1)
    I18n.t("activerecord.models.#{model.to_s.singularize}", count: count)
  end
end
