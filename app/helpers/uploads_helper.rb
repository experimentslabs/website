module UploadsHelper
  def playable?(uploaded_file)
    video?(uploaded_file) || audio?(uploaded_file)
  end

  def audio?(uploaded_file)
    Rails.configuration.elabs.av_formats_audio.include? uploaded_file.content_type
  end

  def video?(uploaded_file)
    Rails.configuration.elabs.av_formats_video.include? uploaded_file.content_type
  end

  def available_formats
    Rails.configuration.elabs.av_formats_audio + Rails.configuration.elabs.av_formats_video
  end
end
