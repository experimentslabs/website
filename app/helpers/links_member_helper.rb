module LinksMemberHelper
  def member_action_links(singular_model, entity, except = [])
    out = []
    out.push show_online_link(entity) unless except.include? :show
    out.push member_edit_link(singular_model, entity) unless except.include? :edit
    out.push member_publish_link(singular_model, entity) unless except.include? :publish
    out.push member_destroy_link(singular_model, entity) unless except.include? :destroy

    # rubocop:disable Rails/OutputSafety
    # These are all links created with "link_to", with titles from i18n
    out.join('').html_safe
    # rubocop:enable Rails/OutputSafety
  end

  def member_edit_link(singular_model, entity)
    namespace_edit_link('member', singular_model, entity)
  end

  def member_destroy_link(singular_model, entity, confirm = true)
    namespace_destroy_link('member', singular_model, entity, confirm)
  end

  def member_publish_link(singular_model, entity)
    link_to send("member_#{singular_model}_toggle_publication_path", entity),
            title:  entity.published? ? t('links_member_helper.member_publish_link.unpublish') : t('links_member_helper.member_publish_link.publish'),
            method: :put,
            class:  'btn btn--small' do
      icon(entity.published? ? 'calendar-times' : 'calendar-check', ['fw'])
    end
  end
end
