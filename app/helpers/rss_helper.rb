module RssHelper
  def rss_title(entity, field)
    sfw      = entity.sfw ? '[SFW]' : '[NSFW]'
    language = "[#{entity.language.iso639_1}]"
    "#{language} #{sfw} - #{entity[field]}"
  end
end
