module NotificationHelper
  def notification_message(notification, links = true)
    user   = notification_user notification, links
    action = notification_action notification.event
    link   = notification_link notification, links
    string = notification_string notification, user: ERB::Util.html_escape(user), action: action, link: link

    # rubocop:disable Rails/OutputSafety
    # Username is escaped
    # Link title is escaped
    string.html_safe
    # rubocop:enable Rails/OutputSafety
  end

  def notification_icon(event)
    actions = {
      comment: 'comment',
      report:  'flag',
      lock:    'lock',
      unlock:  'unlock',
      delete:  'trash',
    }.freeze
    icon (actions[event.to_sym] || 'question'), ['fw']
  end

  def notification_count
    Notification.for_user(current_user).count
  end

  private

  def notification_action(event)
    actions = {
      comment: t('notification_helper.notification_action.has_commented'),
      report:  t('notification_helper.notification_action.has_reported'),
      lock:    t('notification_helper.notification_action.has_locked'),
      unlock:  t('notification_helper.notification_action.has_unlocked'),
      delete:  t('notification_helper.notification_action.has_deleted'),
    }.freeze
    actions[event.to_sym] || "[missing: #{event}]"
  end

  def notification_user(notification, link)
    if %w[Comment Report].include? notification.content_type
      user = nil
      username = notification.content.username
    else
      user = notification.source_user
      username = user.display_name
    end

    return link_to username, user if user && link
    return username if username

    t('notification_helper.notification_user.someone')
  end

  def notification_link(notification, link)
    return "##{notification.content_id}" if notification.event == 'delete'

    title = notification_link_title notification

    return title unless link

    case notification.content_type
    when 'Report'
      ERB::Util.html_escape admin_reports_url
    when 'Comment'
      # rubocop:disable Rails/OutputSafety
      # Internal URL; title is safe
      link_to(title, url_for(notification.content.content)).html_safe
      # rubocop:enable Rails/OutputSafety
    else
      # rubocop:disable Rails/OutputSafety
      # Internal URL; title is safe
      link_to(title, notification.content).html_safe
      # rubocop:enable Rails/OutputSafety
    end
  end

  def notification_link_title(notification)
    return notification.content.title_to_display if notification.content.respond_to?(:title_to_display)

    case notification.content_type
    when 'Comment'
      ERB::Util.html_escape notification.content.content.title_to_display
    when 'Report'
      t('notification_helper.notification_link_title.a_page')
    else
      "##{notification.content_id}"
    end
  end

  def notification_string(notification, interpolations)
    strings = {
      'Album'   => t('notification_helper.notification_string.user_did_something_on_album', interpolations),
      'Article' => t('notification_helper.notification_string.user_did_something_on_article', interpolations),
      'Note'    => t('notification_helper.notification_string.user_did_something_on_note', interpolations),
      'Project' => t('notification_helper.notification_string.user_did_something_on_project', interpolations),
      'Upload'  => t('notification_helper.notification_string.user_did_something_on_file', interpolations),
      'Report'  => t('notification_helper.notification_string.user_did_action', interpolations),
      'Comment' => t('notification_helper.notification_string.user_did_action', interpolations),
    }.freeze
    strings[notification.content_type] || t('notification_helper.notification_string.user_did_something_on_somewhat')
  end
end
