class ApplicationMailer < ActionMailer::Base
  default from: Rails.configuration.elabs.mailer_default_sender
  layout 'mailer'
end
