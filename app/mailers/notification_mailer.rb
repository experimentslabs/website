class NotificationMailer < ApplicationMailer
  helper NotificationHelper
  include NotificationHelper

  def comment_notification_email
    @notification = params[:notification]
    subject       = t('.subject', site_name: Rails.configuration.elabs.site_name, title: notification_message(@notification, false))

    mail(to: @notification.user.email, subject: subject)
  end

  def lock_notification_email
    @notification = params[:notification]
    subject       = t('.subject', site_name: Rails.configuration.elabs.site_name, title: notification_message(@notification, false))

    mail(to: @notification.user.email, subject: subject)
  end

  def report_notification_email
    @notification = params[:notification]
    subject       = t('.subject', site_name: Rails.configuration.elabs.site_name, title: notification_message(@notification, false))

    mail(to: @notification.user.email, subject: subject)
  end
end
