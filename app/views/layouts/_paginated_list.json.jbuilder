json.content do
  json.array! content, partial: partial, as: as
end
json.per_page content.limit_value
json.pages content.total_pages
json.total content.total_count
json.page content.current_page
