builder.item do
  builder.title rss_title(article, 'title')
  builder.description "#{article.excerpt}\n#{article.content}"
  builder.pubDate article.published_at.to_s(:rfc822)
  builder.link article_url(article)
  builder.guid article_url(article)
end
