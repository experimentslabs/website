xml.instruct! :xml, version: '1.0'
xml.rss version: '2.0' do
  xml.channel do
    xml.title "#{Rails.configuration.elabs.site_name} - articles"
    xml.description "Last #{params['show_nsfw'] == 'false' ? 'SFW' : ''} articles from #{Rails.configuration.elabs.site_name}"
    xml.link articles_url

    @articles.each do |article|
      next if params['show_nsfw'] == 'false' && !article.sfw

      render 'article', builder: xml, article: article
    end
  end
end
