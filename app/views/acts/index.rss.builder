xml.instruct! :xml, version: '1.0'
xml.rss version: '2.0' do
  xml.channel do
    xml.title "#{Rails.configuration.elabs.site_name} - activity"
    xml.description "Last #{params['show_nsfw'] == 'false' ? 'SFW' : ''} activity from #{Rails.configuration.elabs.site_name}"
    xml.link activities_url

    @acts.each do |act|
      next if params['show_nsfw'] == 'false' && !act.content.sfw
      next if params['only_creations'] == 'true' && !act.event != 'publish'

      render 'act', act: act, builder: xml
    end
  end
end
