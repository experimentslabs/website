if act.event == 'publish'
  case act.content_type
  when 'Album'
    render 'albums/album', album: act.content, builder: builder
  when 'Article'
    render 'articles/article', article: act.content, builder: builder
  when 'Note'
    render 'notes/note', note: act.content, builder: builder
  when 'Project'
    render 'projects/project', project: act.content, builder: builder
  when 'Upload'
    render 'uploads/upload', upload: act.content, builder: builder
  when 'Link'
    render 'links/link', link: act.content, builder: builder
  end
else
  builder.item do
    builder.title act_notice_string(act, false)
    builder.description act_notice_string(act)
    builder.pubDate act.created_at.to_s(:rfc822)
    builder.link url_for act.content
    builder.guid url_for act.content
  end
end
