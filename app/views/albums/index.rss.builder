xml.instruct! :xml, version: '1.0'
xml.rss version: '2.0' do
  xml.channel do
    xml.title "#{Rails.configuration.elabs.site_name} - albums"
    xml.description "Last #{params['show_nsfw'] == 'false' ? 'SFW' : ''} albums from #{Rails.configuration.elabs.site_name}"
    xml.link albums_url

    @albums.each do |album|
      next if params['show_nsfw'] == 'false' && !album.sfw

      render 'album', builder: xml, album: album
    end
  end
end
