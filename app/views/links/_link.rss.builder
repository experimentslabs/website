builder.item do
  builder.title rss_title(link, 'title')
  builder.description "#{link.url}\n\n#{link.description}"
  builder.pubDate link.published_at.to_s(:rfc822)
  builder.guid link.url
end
