xml.instruct! :xml, version: '1.0'
xml.rss version: '2.0' do
  xml.channel do
    xml.title "#{Rails.configuration.elabs.site_name} - notes"
    xml.description "Last #{params['show_nsfw'] == 'false' ? 'SFW' : ''} notes from #{Rails.configuration.elabs.site_name}"
    xml.link notes_url

    @notes.each do |note|
      next if params['show_nsfw'] == 'false' && !note.sfw

      render 'note', builder: xml, note: note
    end
  end
end
