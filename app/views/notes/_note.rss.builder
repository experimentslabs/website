builder.item do
  builder.title rss_title(note, 'published_at')
  builder.description note.content
  builder.pubDate note.published_at.to_s(:rfc822)
  builder.link note_url(note)
  builder.guid note_url(note)
end
