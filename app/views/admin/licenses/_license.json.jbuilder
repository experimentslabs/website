json.extract! license, :id,
              :name,
              :url,
              :tldr_url,
              :icon,
              :slug,
              :albums_count,
              :articles_count,
              :notes_count,
              :projects_count,
              :uploads_count
