xml.instruct! :xml, version: '1.0'
xml.rss version: '2.0' do
  xml.channel do
    xml.title "#{Rails.configuration.elabs.site_name} - projects"
    xml.description "Last #{params['show_nsfw'] == 'false' ? 'SFW' : ''} projects from #{Rails.configuration.elabs.site_name}"
    xml.link projects_url

    @projects.each do |project|
      next if params['show_nsfw'] == 'false' && !project.sfw

      render 'project', builder: xml, project: project
    end
  end
end
