xml.instruct! :xml, version: '1.0'
xml.rss version: '2.0' do
  xml.channel do
    xml.title "#{Rails.configuration.elabs.site_name} - uploads"
    xml.description "Last #{params['show_nsfw'] == 'false' ? 'SFW' : ''} uploads from #{Rails.configuration.elabs.site_name}"
    xml.link uploads_url

    @uploads.each do |upload|
      next if params['show_nsfw'] == 'false' && !upload.sfw

      render 'upload', builder: xml, upload: upload
    end
  end
end
