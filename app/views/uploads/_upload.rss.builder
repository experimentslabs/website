builder.item do
  builder.title rss_title(upload, 'title')
  builder.description upload.description
  builder.pubDate upload.published_at.to_s(:rfc822)
  builder.link upload_url(upload)
  builder.guid upload_url(upload)
end
