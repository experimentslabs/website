json.extract! notification,
              :id,
              :content,
              :event,
              :user_id,
              :source_user_id,
              :created_at,
              :updated_at
