json.extract! preference,
              :show_nsfw,
              :locale,
              :writing_language_id,
              :writing_license_id,
              :receive_comment_notifications,
              :receive_content_notifications,
              :receive_admin_notifications
