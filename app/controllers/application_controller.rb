class ApplicationController < ActionController::Base
  include Localizable

  before_action :set_nsfw_option
  protect_from_forgery with: :exception

  rescue_from ActiveRecord::RecordNotFound, with: :error_not_found

  private

  def set_nsfw_option
    session[:show_nsfw] ||= false
    session[:show_nsfw] = params['show_nsfw'] == 'true' if params['show_nsfw']
  end

  def create_instance_variable(variable, entity = @entity)
    instance_variable_set "@#{variable}", entity
  end

  def error_not_found(exception = nil)
    message = exception.message || 'Requested content was not found'
    respond_to do |format|
      format.json { render json: { error: message }, status: :not_found }
      format.html { raise exception }
    end
  end
end
