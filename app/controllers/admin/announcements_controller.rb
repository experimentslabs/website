module Admin
  class AnnouncementsController < AdminApplicationController
    before_action :set_announcement, only: [:edit, :update, :destroy]

    # GET /announcements
    def index
      @announcements = Announcement.all
    end

    # GET /announcements/new
    def new
      @announcement = Announcement.new
    end

    # GET /announcements/1/edit
    def edit; end

    # POST /announcements
    def create
      @announcement      = Announcement.new(announcement_params)
      @announcement.user = current_user

      respond_to do |format|
        if @announcement.save
          format.html { redirect_to admin_announcements_url, notice: t('.create_success') }
          format.json { render '_announcement', status: :created, locals: { announcement: @announcement } }
        else
          format.html { render :new }
          format.json { render json: @announcement.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /announcements/1
    def update
      respond_to do |format|
        if @announcement.update(announcement_params)
          format.html { redirect_to admin_announcements_url, notice: t('.update_success') }
          format.json { render '_announcement', status: :ok, locals: { announcement: @announcement } }

        else
          format.html { render :edit }
          format.json { render json: @announcement.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /announcements/1
    def destroy
      @announcement.destroy
      respond_to do |format|
        format.html { redirect_to admin_announcements_url, notice: t('.destroy_success') }
        format.json { head :no_content }
      end
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_announcement
      @announcement = Announcement.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def announcement_params
      params.require(:announcement).permit(:start_at, :end_at, :content, :level, :target)
    end
  end
end
