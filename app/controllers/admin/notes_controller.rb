module Admin
  class NotesController < AdminContentApplicationController
    DEFAULT_ORDER_FIELD = 'content'.freeze
    MODEL               = Note
    PLURAL_NAME         = 'notes'.freeze
    SINGULAR_NAME       = 'note'.freeze
  end
end
