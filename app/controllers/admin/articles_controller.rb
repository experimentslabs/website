module Admin
  class ArticlesController < AdminContentApplicationController
    DEFAULT_ORDER_FIELD = 'title'.freeze
    MODEL               = Article
    PLURAL_NAME         = 'articles'.freeze
    SINGULAR_NAME       = 'article'.freeze
  end
end
