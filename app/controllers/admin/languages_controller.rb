module Admin
  class LanguagesController < Admin::AdminApplicationController
    DEFAULT_ORDER_FIELD = 'name'.freeze

    before_action :set_language, only: [:edit, :update, :destroy]

    # GET /admin/languages
    # GET /admin/languages.json
    def index
      order      = params['order_by'] || self.class::DEFAULT_ORDER_FIELD
      direction  = params['direction'] || 'desc'
      @languages = Language.order(order => direction).page(params[:page]).per(self.class::MAX_ITEMS_PER_PAGE)
    end

    # GET /admin/languages/new
    def new
      @language = Language.new
    end

    # GET /admin/languages/1/edit
    def edit; end

    # POST /admin/languages
    # POST /admin/languages.json
    def create
      @language = Language.new(language_params)

      respond_to do |format|
        if @language.save
          format.html { redirect_to admin_languages_url, notice: t('.create_success') }
          format.json { render '_language', status: :created, location: @language, locals: { language: @language } }
        else
          format.html { render :new }
          format.json { render json: @language.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /admin/languages/1
    # PATCH/PUT /admin/languages/1.json
    def update
      respond_to do |format|
        if @language.update(language_params)
          format.html { redirect_to admin_languages_url, notice: t('.update_success') }
          format.json { render '_language', status: :ok, location: @language, locals: { language: @language } }
        else
          format.html { render :edit }
          format.json { render json: @language.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /admin/languages/1
    # DELETE /admin/languages/1.json
    def destroy
      @language.destroy
      respond_to do |format|
        format.html { redirect_to admin_languages_url, notice: t('.destroy_success') }
        format.json { head :no_content }
      end
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_language
      @language = Language.find_by!(iso639_1: params[:iso639_1])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def language_params
      params.require(:language).permit(:iso639_1, :name)
    end
  end
end
