module Admin
  class TagsController < AdminApplicationController
    DEFAULT_ORDER_FIELD = 'name'.freeze

    before_action :set_tag, only: [:edit, :update, :destroy]

    # GET /tags
    # GET /tags.json
    def index
      order     = params['order_by'] || self.class::DEFAULT_ORDER_FIELD
      direction = params['direction'] || 'desc'
      @tags     = Tag.order(order => direction).all
    end

    # GET /tags/1/edit
    def edit; end

    # PATCH/PUT /tags/1
    # PATCH/PUT /tags/1.json
    def update
      respond_to do |format|
        if @tag.update(tag_params)
          format.html { redirect_to admin_tags_url, notice: t('.update_success') }
          format.json { render :_tag, status: :ok, location: admin_tags_url, locals: { tag: @tag } }
        else
          format.html { render :edit, tag: @tag }
          format.json { render json: @tag.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /tags/1
    # DELETE /tags/1.json
    def destroy
      @tag.destroy
      respond_to do |format|
        format.html { redirect_to admin_tags_url, notice: t('.destroy_success') }
        format.json { head :no_content }
      end
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_tag
      @tag = Tag.find_by!(slug: params[:slug])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tag_params
      params.require(:tag).permit(:name)
    end
  end
end
