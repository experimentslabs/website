module Admin
  class ReportsController < AdminApplicationController
    DEFAULT_ORDER_FIELD = 'created_at'.freeze

    before_action :set_report, only: [:destroy]

    # GET /reports
    # GET /reports.json
    def index
      order     = params['order_by'] || self.class::DEFAULT_ORDER_FIELD
      direction = params['direction'] || 'desc'
      @reports = Report.order(order => direction).all
    end

    # DELETE /reports/1
    # DELETE /reports/1.json
    def destroy
      @report.destroy
      respond_to do |format|
        format.html { redirect_to admin_reports_url, notice: t('.destroy_success') }
        format.json { head :no_content }
      end
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_report
      @report = Report.find(params[:id])
    end
  end
end
