module Admin
  class AdminContentApplicationController < AdminApplicationController
    MODEL         = nil
    SINGULAR_NAME = nil
    PLURAL_NAME   = nil

    before_action :set_entity, only: [:destroy, :toggle_lock]

    # GET /<entity>
    # GET /<entity>.json
    def index
      order     = params['order_by'] || self.class::DEFAULT_ORDER_FIELD
      direction = params['direction'] || 'desc'
      create_instance_variable self.class::PLURAL_NAME, self.class::MODEL.order(order => direction).page(params[:page]).per(self.class::MAX_ITEMS_PER_PAGE)
    end

    # DELETE /<entity>/1
    # DELETE /<entity>/1.json
    def destroy
      @entity.changed_by = current_user
      @entity.destroy
      respond_to do |format|
        format.html { redirect_to index_url, notice: t('admin.admin_content_application.destroy.success', model_name: self.class::SINGULAR_NAME.capitalize) }
        format.json { head :no_content }
      end
    end

    # rubocop:disable Metrics/AbcSize
    # PUT /<entity>/1/toggle_lock
    def toggle_lock
      @entity.changed_by = current_user
      respond_to do |format|
        if @entity.toggle(:locked).save
          format.html { redirect_to index_url, notice: lock_message(@entity) }
          format.json { render partial_name, status: :ok, locals: { "#{self.class::SINGULAR_NAME}": @entity } }
        else
          format.html { redirect_to index_url, error: t('admin.admin_content_application.toggle_lock.error') }
          format.json { render json: @entity.errors, status: :unprocessable_entity }
        end
      end
    end

    # rubocop:enable Metrics/AbcSize

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_entity
      @entity = self.class::MODEL.find_by_slug_or_id params # rubocop:disable Rails/DynamicFindBy

      create_instance_variable self.class::SINGULAR_NAME
    end

    def index_url
      "admin_#{self.class::PLURAL_NAME}".to_sym
    end

    def partial_name
      "_#{self.class::SINGULAR_NAME}".to_sym
    end

    def lock_message(entity)
      model_name = self.class::SINGULAR_NAME.capitalize
      if entity.locked?
        t('admin.admin_content_application.lock_message.lock_success', model_name: model_name)
      else
        t('admin.admin_content_application.lock_message.unlock_success', model_name: model_name)
      end
    end
  end
end
