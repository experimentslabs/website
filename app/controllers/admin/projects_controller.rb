module Admin
  class ProjectsController < AdminContentApplicationController
    DEFAULT_ORDER_FIELD = 'name'.freeze
    MODEL               = Project
    PLURAL_NAME         = 'projects'.freeze
    SINGULAR_NAME       = 'project'.freeze
  end
end
