module Admin
  class AlbumsController < AdminContentApplicationController
    DEFAULT_ORDER_FIELD = 'name'.freeze
    MODEL               = Album
    PLURAL_NAME         = 'albums'.freeze
    SINGULAR_NAME       = 'album'.freeze
  end
end
