module Admin
  class UsersController < AdminApplicationController
    DEFAULT_ORDER_FIELD = 'username'.freeze

    before_action :set_admin_user, only: [:show, :destroy]

    # GET /admin/users
    # GET /admin/users.json
    def index
      order     = params['order_by'] || self.class::DEFAULT_ORDER_FIELD
      direction = params['direction'] || 'desc'
      @users = User.order(order => direction).page(params[:page]).per(self.class::MAX_ITEMS_PER_PAGE)
    end

    # GET /admin/users/1
    # GET /admin/users/1.json
    def show; end

    # DELETE /admin/users/1
    # DELETE /admin/users/1.json
    def destroy
      @user.destroy
      respond_to do |format|
        format.html { redirect_to admin_users_url, notice: t('.destroy_success') }
        format.json { head :no_content }
      end
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_admin_user
      @user = User.find_by!(username: params[:username])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_user_params
      params.fetch(:user, {})
    end
  end
end
