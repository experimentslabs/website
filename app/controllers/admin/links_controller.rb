module Admin
  class LinksController < AdminContentApplicationController
    DEFAULT_ORDER_FIELD = 'title'.freeze
    MODEL               = Link
    PLURAL_NAME         = 'links'.freeze
    SINGULAR_NAME       = 'link'.freeze
  end
end
