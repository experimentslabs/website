module Admin
  class AdminApplicationController < ApplicationController
    DEFAULT_ORDER_FIELD = 'id'.freeze
    MAX_ITEMS_PER_PAGE  = Rails.configuration.elabs.max_admin_items_per_page

    before_action :authenticate_admin!

    layout 'layouts/application_admin'

    private

    def authenticate_admin!
      authenticate_user!
      redirect_to user_url(current_user), status: :unauthorized unless current_user.admin?
    end
  end
end
