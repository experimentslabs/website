module Admin
  class UploadsController < AdminContentApplicationController
    DEFAULT_ORDER_FIELD = 'title'.freeze
    MODEL               = Upload
    PLURAL_NAME         = 'uploads'.freeze
    SINGULAR_NAME       = 'upload'.freeze
  end
end
