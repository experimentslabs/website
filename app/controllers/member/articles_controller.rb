module Member
  class ArticlesController < MemberContentApplicationController
    ALLOWED_FIELDS    = [:title, :excerpt, :content, :sfw, :published, :published_at, :hidden_in_history, :license_id, :language_id, :tags_list, :minor_update, :update_description].freeze
    ALLOWED_RELATIONS = { project_ids: [] }.freeze
    MODEL             = Article
    PLURAL_NAME       = 'articles'.freeze
    SINGULAR_NAME     = 'article'.freeze
  end
end
