module Member
  class UploadsController < MemberContentApplicationController
    ALLOWED_FIELDS    = [:title, :description, :sfw, :published, :hidden_in_history, :language_id, :license_id, :file, :tags_list, :minor_update, :update_description].freeze
    ALLOWED_RELATIONS = { album_ids: [], project_ids: [] }.freeze
    MODEL             = Upload
    PLURAL_NAME       = 'uploads'.freeze
    SINGULAR_NAME     = 'upload'.freeze
  end
end
