module Member
  class MarkdownPreviewerController < ApplicationController
    self.allow_forgery_protection = false

    def preview
      render locals: { text: params['text'] }
    end
  end
end
