module Member
  class MemberApplicationController < ApplicationController
    DEFAULT_ORDER_FIELD = 'id'.freeze
    MAX_ITEMS_PER_PAGE  = Rails.configuration.elabs.max_members_items_per_page

    before_action :authenticate_user!

    layout 'layouts/application_member'
  end
end
