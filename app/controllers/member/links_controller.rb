module Member
  class LinksController < MemberContentApplicationController
    ALLOWED_FIELDS    = [:title, :url, :description, :sfw, :hidden_in_history, :published, :language_id, :tags_list, :minor_update, :update_description].freeze
    ALLOWED_RELATIONS = {}.freeze
    MODEL             = Link
    PLURAL_NAME       = 'links'.freeze
    SINGULAR_NAME     = 'link'.freeze
  end
end
