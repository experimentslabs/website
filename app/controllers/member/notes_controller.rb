module Member
  class NotesController < MemberContentApplicationController
    ALLOWED_FIELDS    = [:content, :sfw, :hidden_in_history, :published, :license_id, :language_id, :tags_list, :minor_update, :update_description].freeze
    ALLOWED_RELATIONS = { project_ids: [] }.freeze
    MODEL             = Note
    PLURAL_NAME       = 'notes'.freeze
    SINGULAR_NAME     = 'note'.freeze
  end
end
