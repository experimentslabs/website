module Member
  class MemberContentApplicationController < MemberApplicationController
    ALLOWED_FIELDS      = [].freeze
    ALLOWED_RELATIONS   = {}.freeze
    DEFAULT_ORDER_FIELD = 'updated_at'.freeze
    MODEL               = nil
    PLURAL_NAME         = nil
    SINGULAR_NAME       = nil

    before_action :set_entity, only: [:edit, :update, :destroy, :toggle_publication]

    # GET /articles
    # GET /articles.json
    # rubocop: disable Metrics/AbcSize
    def index
      order     = params['order_by'] || self.class::DEFAULT_ORDER_FIELD
      direction = params['direction'] || 'desc'
      create_instance_variable self.class::PLURAL_NAME, self.class::MODEL.by_member(current_user.id).order(order => direction).page(params[:page]).per(self.class::MAX_ITEMS_PER_PAGE)
    end

    # rubocop: enable Metrics/AbcSize

    # GET /articles/new
    def new
      entity = complete_entity_from_preferences(self.class::MODEL.new)
      create_instance_variable self.class::SINGULAR_NAME, entity
    end

    # GET /articles/1/edit
    def edit; end

    # POST /articles
    # POST /articles.json
    # rubocop: disable Metrics/AbcSize
    def create
      build_entity

      respond_to do |format|
        if @entity.save
          format.html { redirect_to_index_with_notice :notice, t('member.member_content_application.create.create_success', model_name: self.class::SINGULAR_NAME.capitalize) }
          format.json { render partial_name, status: :created, locals: { "#{self.class::SINGULAR_NAME}": @entity } }
        else
          format.html { render :new }
          format.json { render json: @entity.errors, status: :unprocessable_entity }
        end
      end
    end

    # rubocop: enable Metrics/AbcSize

    # PATCH/PUT /articles/1
    # PATCH/PUT /articles/1.json
    # rubocop: disable Metrics/AbcSize
    def update
      @entity.changed_by = current_user
      respond_to do |format|
        if @entity.update entity_params
          format.html { redirect_to_index_with_notice :notice, t('member.member_content_application.update.update_success', model_name: self.class::SINGULAR_NAME.capitalize) }
          format.json { render partial_name, status: :ok, locals: { "#{self.class::SINGULAR_NAME}": @entity } }
        else
          format.html { render :edit }
          format.json { render json: @entity.errors, status: :unprocessable_entity }
        end
      end
    end

    # rubocop: enable Metrics/AbcSize

    # DELETE /articles/1
    # DELETE /articles/1.json
    def destroy
      @entity.changed_by = current_user
      @entity.destroy
      respond_to do |format|
        format.html { redirect_to index_url, notice: t('member.member_content_application.destroy.destroy_success', model_name: self.class::SINGULAR_NAME.capitalize) }
        format.json { head :no_content }
      end
    end

    # PUT /<entity>/1/toggle_publication
    def toggle_publication
      respond_to do |format|
        if @entity.toggle(:published).save
          format.html { redirect_to_index_with_notice :notice, publication_message(@entity) }
          format.json { render partial_name, status: :ok, locals: { "#{self.class::SINGULAR_NAME}": @entity } }
        else
          format.html { redirect_to_index_with_notice :error, t('member.member_content_application.toggle_publication.error') }
          format.json { render json: @entity.errors, status: :unprocessable_entity }
        end
      end
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_entity
      @entity = self.class::MODEL.find_by_slug_or_id params # rubocop:disable Rails/DynamicFindBy

      create_instance_variable self.class::SINGULAR_NAME
    end

    def complete_entity_from_preferences(entity)
      entity.language_id = current_user.preference.writing_language_id if entity.has_attribute?('language_id') && current_user.preference.writing_language_id
      entity.license_id = current_user.preference.writing_license_id if entity.has_attribute?('license_id') && current_user.preference.writing_license_id

      entity
    end

    def build_entity
      @entity      = self.class::MODEL.new(entity_params)
      @entity.user = current_user
      create_instance_variable self.class::SINGULAR_NAME
    end

    def index_url
      "member_#{self.class::PLURAL_NAME}".to_sym
    end

    def partial_name
      "_#{self.class::SINGULAR_NAME}".to_sym
    end

    def redirect_to_index_with_notice(type, notice)
      redirect_to "member_#{self.class::PLURAL_NAME}".to_sym, "#{type}": notice
    end

    def publication_message(entity)
      model_name = self.class::SINGULAR_NAME.capitalize
      if entity.published?
        t('member.member_content_application.publication_message.publish_success', model_name: t("activerecord.models.#{model_name.underscore}"))
      else
        t('member.member_content_application.publication_message.unpublish_success', model_name: t("activerecord.models.#{model_name.underscore}"))
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def entity_params
      params.require(self.class::SINGULAR_NAME.to_sym).permit(self.class::ALLOWED_FIELDS, self.class::ALLOWED_RELATIONS)
    end
  end
end
