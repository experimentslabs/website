module Member
  class PreferencesController < MemberApplicationController
    before_action :set_preference, only: [:edit, :update]

    # GET /preferences/edit
    def edit; end

    # PATCH/PUT /preferences
    # PATCH/PUT /preferences
    def update
      respond_to do |format|
        if @preference.update(preference_params)
          format.html { redirect_to member_edit_preferences_url, notice: t('.update_success') }
          format.json { render :edit, status: :ok }
        else
          format.html { render :edit }
          format.json { render json: @preference.errors, status: :unprocessable_entity }
        end
      end
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_preference
      @preference = current_user.preference
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def preference_params
      params.require(:preference).permit(:show_nsfw, :locale, :writing_language_id, :writing_license_id, :receive_comment_notifications, :receive_content_notifications, :receive_admin_notifications)
    end
  end
end
