module Member
  class ProjectsController < MemberContentApplicationController
    ALLOWED_FIELDS    = [:name, :logo, :short_description, :description, :main_url, :sources_url, :docs_url, :sfw, :published, :hidden_in_history, :license_id, :language_id, :tags_list, :minor_update, :update_description].freeze
    ALLOWED_RELATIONS = {}.freeze
    MODEL             = Project
    PLURAL_NAME       = 'projects'.freeze
    SINGULAR_NAME     = 'project'.freeze
  end
end
