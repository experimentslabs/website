module Member
  class NotificationsController < MemberApplicationController
    MAX_ITEMS_PER_PAGE = Rails.configuration.elabs.max_members_items_per_page
    DEFAULT_ORDER_FIELD = 'created_at'.freeze

    before_action :set_notification, only: [:destroy]

    # GET /notifications
    def index
      order     = params['order_by'] || self.class::DEFAULT_ORDER_FIELD
      direction = params['direction'] || 'desc'
      @notifications = Notification.for_user(current_user).order(order => direction).page(params[:page]).per(self.class::MAX_ITEMS_PER_PAGE)
    end

    # DELETE /notifications/1
    def destroy
      @notification.destroy
      respond_to do |format|
        format.html { redirect_to member_notifications_url, notice: t('.destroy_success') }
        format.json { head :no_content }
      end
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_notification
      @notification = Notification.find(params[:id])
    end
  end
end
