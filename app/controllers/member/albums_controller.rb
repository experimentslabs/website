module Member
  class AlbumsController < MemberContentApplicationController
    ALLOWED_FIELDS    = [:name, :description, :sfw, :published, :hidden_in_history, :language_id, :license_id, :tags_list, :minor_update, :update_description].freeze
    ALLOWED_RELATIONS = { project_ids: [], upload_ids: [] }.freeze
    MODEL             = Album
    PLURAL_NAME       = 'albums'.freeze
    SINGULAR_NAME     = 'album'.freeze
  end
end
