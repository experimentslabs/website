module Member
  class CommentsController < MemberApplicationController
    before_action :set_comment, only: [:archive, :destroy]

    def archive
      respond_to do |format|
        if @comment.archive!
          format.html { redirect_to @comment.content, notice: t('.archive_success') }
          format.json { render json: @comment, status: :ok }
        else
          format.html { redirect_to @comment.content, notice: t('.error_occured') }
          format.json { render json: @comment.errors, status: :unprocessable_entity }
        end
      end
    end

    def destroy
      content = @comment.content
      @comment.destroy
      respond_to do |format|
        format.html { redirect_to content, notice: t('.destroy_success') }
        format.json { head :no_content }
      end
    end

    private

    def set_comment
      id = params.key?('comment_id') ? params['comment_id'] : params['id']
      comment = Comment.find(id)

      if comment.content.user.id == current_user.id
        @comment = comment
        return
      end

      throw ActiveRecord::RecordNotFound
    end
  end
end
