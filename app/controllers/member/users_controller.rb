module Member
  class UsersController < MemberApplicationController
    before_action :set_user, only: [:edit, :update]

    # GET /member/infos
    def edit; end

    # PATCH/PUT /member/infos
    def update
      respond_to do |format|
        if @user.update(user_params)
          format.html { redirect_to @user, notice: t('.save_success') }
          format.json { render 'edit', user: @user, status: :ok }
        else
          format.html { render :edit }
          format.json { render json: { errors: @user.errors }, status: :unprocessable_entity }
        end
      end
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = current_user
    end

    # Only allow a trusted parameter "white list" through.
    def user_params
      params.require(:user).permit(:real_name, :biography, :avatar)
    end
  end
end
