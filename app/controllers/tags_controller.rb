class TagsController < PublicController
  ALLOWED_ORDER_FROM = %w[name].freeze
  DEFAULT_ORDER      = { name: :desc }.freeze

  before_action :set_tag, only: [:show]

  # GET /tags
  # GET /tags.json
  def index
    @tags = scope_request Tag.all
  end

  # GET /tags/1
  # GET /tags/1.json
  def show; end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_tag
    @tag = Tag.find_by!(slug: params[:slug])
  end
end
