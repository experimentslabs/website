class LanguagesController < PublicController
  ALLOWED_ORDER_FROM = %w[name].freeze
  DEFAULT_ORDER      = { name: :asc }.freeze

  before_action :set_language, only: [:show]

  # GET /languages
  # GET /languages.json
  def index
    languages = scope_request Language.page(params[:page]).per(self.class::MAX_ITEMS_PER_PAGE)
    @languages = languages.with_content_only
  end

  # GET /languages/1
  # GET /languages/1.json
  def show; end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_language
    @language = Language.find_by!(iso639_1: params[:iso639_1])
  end
end
