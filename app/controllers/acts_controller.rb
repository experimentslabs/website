class ActsController < PublicController
  ALLOWED_ORDER_FROM = %w[created_at].freeze
  DEFAULT_ORDER = { created_at: :desc }.freeze

  # GET /acts
  # GET /acts.json
  def index
    @acts = scope_request(Act).page(params[:page]).per(self.class::MAX_ITEMS_PER_PAGE)
  end
end
