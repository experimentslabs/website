class NotesController < ContentApplicationController
  ALLOWED_NESTED_FROM = %w[language license tag user project].freeze
  ALLOWED_ORDER_FROM  = %w[published_at updated_at].freeze

  before_action :set_note, only: [:show]

  # GET /notes
  # GET /notes.json
  def index
    @notes = scope_request(Note.published).page(params[:page]).per(self.class::MAX_ITEMS_PER_PAGE)
  end

  # GET /notes/1
  # GET /notes/1.json
  def show; end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_note
    @note = Note.find_publicly_visible(params[:slug])
  end
end
