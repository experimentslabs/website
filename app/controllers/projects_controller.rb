class ProjectsController < ContentApplicationController
  ALLOWED_NESTED_FROM = %w[language license tag user].freeze
  ALLOWED_ORDER_FROM  = %w[name published_at updated_at].freeze

  before_action :set_project, only: [:show]

  # GET /projects
  # GET /projects.json
  def index
    @projects = scope_request(Project.published).page(params[:page]).per(self.class::MAX_ITEMS_PER_PAGE)
  end

  # GET /projects/1
  # GET /projects/1.json
  def show; end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_project
    @project = Project.find_publicly_visible(params[:slug])
  end
end
