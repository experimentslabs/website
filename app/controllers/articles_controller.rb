class ArticlesController < ContentApplicationController
  ALLOWED_NESTED_FROM = %w[language license tag user project].freeze
  ALLOWED_ORDER_FROM  = %w[title published_at updated_at].freeze

  before_action :set_article, only: [:show]

  # GET /articles
  # GET /articles.json
  def index
    @articles = scope_request(Article.published).page(params[:page]).per(self.class::MAX_ITEMS_PER_PAGE)
  end

  # GET /articles/1
  # GET /articles/1.json
  def show; end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_article
    @article = Article.find_publicly_visible(params[:slug])
  end
end
