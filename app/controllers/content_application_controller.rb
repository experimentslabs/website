class ContentApplicationController < PublicController
  DEFAULT_ORDER      = { published_at: :desc }.freeze
  IS_NSFW_FILTERABLE = true

  before_action :new_comment, only: [:show]

  # Show action should be implemented on children
  def show; end

  # POST /comments
  # POST /comments.json
  def create_comment # rubocop:disable Metrics/AbcSize, Metrics/MethodLength
    return if trap_dumb_bot_in 'body', t('content_application.create_comment.create_success_robot')

    build_comment

    respond_to do |format|
      if @comment.save
        success_message = t('content_application.create_comment.create_success')
        format.html { redirect_to @comment.content, notice: success_message }
        format.json { render json: { message: success_message }, status: :created }
      else
        @new_comment = @comment
        format.html { redirect_to @comment.content }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  # Never trust parameters from the scary internet, only allow the white list through.
  def comment_params
    params.require(:comment).permit(:name, :email, :comment, :allow_contact)
  end

  def new_comment
    @new_comment ||= Comment.new
  end

  def build_comment
    @comment = Comment.new(comment_params)
    params.each do |key, value|
      # find the content by slug if needed
      @comment.content_id = find_related_id_by_slug(key.chomp('_slug'), 'slug', value) if %w[album_slug article_slug note_slug project_slug upload_slug].include? key
    end
    @comment.content_type = params[:controller].classify
    @comment.user         = current_user if user_signed_in?
  end
end
