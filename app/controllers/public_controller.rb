class PublicController < ApplicationController
  include Reportable

  ALLOWED_NESTED_FROM = [].freeze
  ALLOWED_ORDER_FROM  = [].freeze
  DEFAULT_ORDER       = { id: :desc }.freeze
  IS_NSFW_FILTERABLE  = false
  MAX_ITEMS_PER_PAGE  = Rails.configuration.elabs.max_items_per_page

  private

  def scope_request(orm_request)
    orm_request = build_request_nested orm_request
    orm_request = build_request_where orm_request
    orm_request = build_request_nsfw orm_request if self.class::IS_NSFW_FILTERABLE

    build_request_order orm_request
  end

  def build_request_nested(orm_request)
    where_clauses = {}

    params.each do |p|
      # No need to do this with "album_id" as there is no action requiring it (as for a /albums/xxx/uploads)
      next unless %w[tag_slug project_slug].include? p[0]

      foreign_table                = p[0].chomp('_slug').pluralize.to_sym
      orm_request                  = orm_request.joins(foreign_table)
      id                           = find_related_id_by_slug(p[0].chomp('_slug'), 'slug', p[1])
      where_clauses[foreign_table] = { id: id }
      next
    end

    orm_request.where(where_clauses)
  end

  def build_request_where(orm_request)
    nested_resource_fields = extract_nested_resource_fields_from_params

    # Find nested resources filters in request params
    nested_resource_fields.each do |field, details|
      nested_param_value = params[field]
      # Find Id from slug
      id = find_related_id_by_slug(details[:type], details[:slug_field], nested_param_value)
      orm_request = orm_request.where("#{details[:type]}_id" => id)
    end

    orm_request
  end

  def build_request_order(orm_request)
    allowed_order_fields = self.class::ALLOWED_ORDER_FROM.map { |n| "order_by_#{n}" }
    orders               = 0

    params.each do |p|
      next unless allowed_order_fields.include? p[0]

      field       = p[0].gsub(/^order_by_/, '')
      orm_request = orm_request.order field => p[1]
      orders += 1
    end

    orm_request = orm_request.order(self.class::DEFAULT_ORDER) if orders.zero?
    orm_request
  end

  def build_request_nsfw(orm_request)
    if params['sfw_status']
      return case params['sfw_status']
             when 'sfw_only'
               orm_request.where(sfw: true)
             when 'nsfw_only'
               orm_request.where(sfw: false)
             else
               orm_request
             end
    end
    orm_request
  end

  def find_related_id_by_slug(type, slug_field, slug_value)
    type.classify.to_s.constantize.select(:id).find_by!(slug_field => slug_value).id
  end

  def extract_nested_resource_fields_from_params
    slug_parameters = {}
    self.class::ALLOWED_NESTED_FROM.each do |n|
      model      = n.classify.to_s.constantize
      slug_field = model.const_defined?(:SLUG_FIELD) ? model::SLUG_FIELD : :id
      slug_parameters["#{n}_#{slug_field}"] = { type: n, slug_field: slug_field }
    end
    # Remove HABTM possible nesting as they are handled in another place
    # and slug fields that are not present in params
    slug_parameters.reject { |field| %w[tag_slug project_slug].include?(field) || !params.include?(field) }
  end

  def trap_dumb_bot_in(field, notice)
    return false unless Rails.configuration.elabs.trap_dumb_bots

    if params[field].present?
      respond_to do |format|
        format.html { redirect_to request.referer, notice: notice }
        format.json { render json: { message: 'ok' }, status: :created }
      end

      return true
    end

    false
  end
end
