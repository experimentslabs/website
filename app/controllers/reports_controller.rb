class ReportsController < PublicController
  # POST /reports
  # POST /reports.json
  # rubocop:disable Metrics/AbcSize
  def create
    return if trap_dumb_bot_in 'body', t('.report_create_success_bot')

    report = complete_report_with_user

    respond_to do |format|
      if report.save
        format.html { redirect_to request.referer, notice: t('.report_create_success') }
        format.json { render json: { message: 'ok' }, status: :created }
      else
        format.html { redirect_to request.referer, notice: { error: report.errors } }
        format.json { render json: report.errors, status: :unprocessable_entity }
      end
    end
  end
  # rubocop:enable Metrics/AbcSize

  private

  # Never trust parameters from the scary internet, only allow the white list through.
  def report_params
    params.require(:report).permit(:name, :email, :reason, :allow_contact)
  end

  def complete_report_with_user
    report = Report.new(report_params)
    report.url = request.referer
    report.user = current_user if user_signed_in?

    report
  end
end
