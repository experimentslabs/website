# frozen_string_literal: true

module Auth
  class ConfirmationsController < Devise::ConfirmationsController
    include ElabsController
    include Reportable
    include Localizable

    # GET /resource/confirmation/new
    # def new
    #   super
    # end

    # POST /resource/confirmation
    # def create
    #   super
    # end

    # GET /resource/confirmation?confirmation_token=abcdef
    # def show
    #   super
    # end

    protected

    # Override the default "new_session_path" method to return a known path
    def new_session_path(*)
      new_user_session_path
    end

    # The path used after resending confirmation instructions.
    # def after_resending_confirmation_instructions_path_for(resource_name)
    #   super(resource_name)
    # end

    # The path used after confirmation.
    # def after_confirmation_path_for(resource_name, resource)
    #   super(resource_name, resource)
    # end
  end
end
