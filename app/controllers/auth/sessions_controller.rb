module Auth
  class SessionsController < ::Devise::SessionsController
    include ElabsController
    include Reportable
    include Localizable

    # before_action :configure_sign_in_params, only: [:create]

    # GET /resource/sign_in
    # def new
    #   super
    # end

    # POST /resource/sign_in
    def create
      super

      session[:show_nsfw] = current_user.preference.show_nsfw
      session[:locale]    = current_user.preference.locale
    end

    # DELETE /resource/sign_out
    # def destroy
    #   super
    # end

    # protected

    # If you have extra params to permit, append them to the sanitizer.
    # def configure_sign_in_params
    #   devise_parameter_sanitizer.permit(:sign_in, keys: [:attribute])
    # end
  end
end
