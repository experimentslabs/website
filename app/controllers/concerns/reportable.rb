module Reportable
  extend ActiveSupport::Concern

  included do
    before_action :prepare_empty_report
  end

  private

  def prepare_empty_report
    @new_report = Report.new
  end
end
