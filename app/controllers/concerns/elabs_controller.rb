# This concern is meant to be included in controllers extending
# other plugins/engines controllers (i.e.: Devise)
# This should not be used on Elabs controllers, as everything here is declared
# in ElabsApplicationController.
module ElabsController
  extend ActiveSupport::Concern

  included do
    helper ApplicationHelper
    helper IconsHelper
    helper ContentFiltersHelper
    helper NotificationHelper
    helper ContentRendererHelper
    helper ShortcodesHelper
  end
end
