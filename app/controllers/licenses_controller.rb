class LicensesController < PublicController
  ALLOWED_ORDER_FROM = %w[name].freeze
  DEFAULT_ORDER      = { name: :asc }.freeze

  before_action :set_license, only: [:show]

  # GET /licenses
  # GET /licenses.json
  def index
    @licenses = scope_request License.all
  end

  # GET /licenses/1
  # GET /licenses/1.json
  def show; end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_license
    @license = License.find_by!(slug: params[:slug])
  end
end
