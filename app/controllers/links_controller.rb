class LinksController < ContentApplicationController
  ALLOWED_NESTED_FROM = %w[language tag user].freeze
  ALLOWED_ORDER_FROM  = %w[title url published_at updated_at].freeze

  # GET /links
  def index
    @links = scope_request(Link.published).page(params[:page]).per(self.class::MAX_ITEMS_PER_PAGE)
  end
end
