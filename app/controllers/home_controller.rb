class HomeController < ApplicationController
  include Reportable

  def index
    @projects = Project.publicly_visible.order(published_at: :desc).first(9)
  end
end
