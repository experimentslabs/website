class UploadsController < ContentApplicationController
  ALLOWED_NESTED_FROM = %w[language license tag user album project].freeze
  ALLOWED_ORDER_FROM  = %w[title published_at updated_at].freeze

  before_action :set_upload, only: [:show]

  # GET /uploads
  # GET /uploads.json
  def index
    @uploads = scope_request(Upload.published).page(params[:page]).per(self.class::MAX_ITEMS_PER_PAGE)
  end

  # GET /uploads/1/edit
  def show; end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_upload
    @upload = Upload.find_publicly_visible(params[:slug])
  end
end
