# Roadmap

## [Issues]

See the issues on the project's page for a list. Contributions are welcome!

<!-- Once you're done with a release, clean this up and move the changes to
     the CHANGELOG -->
## [Next] - Already done

### Added

- Added base tools to get started

### Changed

### Improved

### Removed

### Fixed

## Planned

Note that the elements in this list may change during development; it' a
general idea of what will be done next.
<!-- You may list the next milestones and the changes you plan for them,
     or use your favorite VCS platform to track them -->

- [1.0.0] - Working application with all the base features

## Wonderland
<!-- You may list all the ideas you have, or use the issue system of your
     favorite VCS platform to track them -->
