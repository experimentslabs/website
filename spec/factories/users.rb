FactoryBot.define do
  factory :user do
    username { Faker::Internet.unique.username(specifier: 4...30, separators: %w[- _]) }
    email { Faker::Internet.unique.email }
    password { 'password' }

    factory :user_known do
      email { 'member@example.com' }
      confirmed_at { Time.zone.now }
    end

    factory :user_active do
      confirmed_at { Time.zone.now }
    end

    factory :user_admin do
      role { 'admin' }

      factory :user_known_admin do
        email { 'admin@example.com' }
        confirmed_at { Time.zone.now }
      end

      factory :user_admin_active do
        confirmed_at { Time.zone.now }
      end
    end
  end
end
