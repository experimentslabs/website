FactoryBot.define do
  factory :note, class: 'Note' do
    content { Faker::Lorem.paragraphs(number: 2).join("\n\n") }
    association :user, factory: :user_active
    license
    language

    factory :note_published do
      published { true }
      published_at { Time.zone.now }

      factory :note_published_and_safe do
        sfw { true }
      end
    end
  end
end
