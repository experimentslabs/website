FactoryBot.define do
  factory :report, class: 'Report' do
    name { Faker::Name.name }
    email { Faker::Internet.email }
    url { '/articles/my-first-articles' }
    reason { Faker::Books::Lovecraft.fhtagn(number: 3) }

    factory :report_user do
      name { nil }
      email { nil }
      association :user, factory: :user_active
    end
  end
end
