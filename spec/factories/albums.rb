FactoryBot.define do
  factory :album, class: 'Album' do
    name { Faker::Lorem.sentence }
    description { Faker::Lorem.paragraph }
    association :user, factory: :user_active
    language
    license

    factory :album_published do
      published { true }
      published_at { Time.zone.now }

      factory :album_published_and_safe do
        sfw { true }
      end

      factory :album_published_and_locked do
        locked { true }
      end
    end
  end
end
