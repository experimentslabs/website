FactoryBot.define do
  factory :upload, class: 'Upload' do
    title { Faker::Lorem.sentence }
    description { Faker::Lorem.paragraphs(number: 6).join("\n\n") }
    language
    association :user, factory: :user_active
    license
    file { Rack::Test::UploadedFile.new Rails.root.join('spec', 'fixtures', 'files', 'Woody.jpg'), 'image/jpeg' }

    factory :upload_published do
      published { true }
      published_at { Time.zone.now }

      factory :upload_published_and_safe do
        sfw { true }
      end
    end
  end
end
