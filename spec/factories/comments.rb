FactoryBot.define do
  factory :comment, class: 'Comment' do
    name { Faker::Internet.username }
    email { Faker::Internet.email }
    comment { Faker::Lorem.paragraph }
    association :content, factory: :album

    factory :comment_on_album do
      association :content, factory: :album_published
    end

    factory :comment_on_article do
      association :content, factory: :article_published
    end

    factory :comment_on_note do
      association :content, factory: :note_published
    end

    factory :comment_on_project do
      association :content, factory: :project_published
    end

    factory :comment_on_upload do
      association :content, factory: :upload_published
    end
  end
end
