FactoryBot.define do
  factory :tag, class: 'Tag' do
    name { Faker::Alphanumeric.unique.alpha number: 12 }
  end
end
