FactoryBot.define do
  factory :link, class: 'Link' do
    title { Faker::Lorem.sentence }
    url { Faker::Internet.url }
    association :user, factory: :user_active
    language

    factory :link_published do
      published { true }
      published_at { Time.zone.now }

      factory :link_published_and_safe do
        sfw { true }
      end
    end
  end
end
