FactoryBot.define do
  factory :notification, class: 'Notification' do
    association :content, factory: :album_published
    association :user, factory: :user_active
    association :source_user, factory: :user_admin_active

    factory :notification_comment do
      event { 'comment' }
      association :content, factory: :comment_on_album
      source_user { nil }
    end

    factory :notification_lock do
      event { 'lock' }
      source_user { nil }
      message { "We locked your album as it's badly tagged NSFW; please review." }
      association :content, factory: :album_published_and_locked
    end

    factory :notification_report do
      event { 'report' }
      association :content, factory: :report
    end
  end
end
