FactoryBot.define do
  factory :language, class: 'Language' do
    iso639_1 { Faker::Alphanumeric.unique.alpha number: 2 }
    name { Faker::Alphanumeric.unique.alpha number: 10 }
  end
end
