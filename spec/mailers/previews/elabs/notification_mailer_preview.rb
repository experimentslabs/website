module Elabs
  # Preview all emails at http://localhost:3000/rails/mailers/elabs/notification_mailer
  class NotificationMailerPreview < ActionMailer::Preview
    # Preview at http://localhost:3000/rails/mailers/elabs/notification_mailer/comment_notification_email
    def comment_notification_email
      notification = Notification.where(content_type: 'Comment').first
      NotificationMailer.with(notification: notification).comment_notification_email
    end

    # Preview at http://localhost:3000/rails/mailers/elabs/notification_mailer/lock_notification_email
    def lock_notification_email
      content = Album.first
      notification = Notification.new(
        content:     content,
        user:        content.user,
        source_user: User.where(role: 'admin').first,
        event:       'lock',
        message:     'Your content has been locked because it seems unfinished.'
      )
      NotificationMailer.with(notification: notification).lock_notification_email
    end

    # Preview at http://localhost:3000/rails/mailers/elabs/notification_mailer/report_notification_email
    def report_notification_email
      notification = Notification.where(content_type: 'Report').first
      NotificationMailer.with(notification: notification).report_notification_email
    end

    private

    def subject_element
      user = User.where(role: 'user').first
      FactoryBot.create :album_published,
                        user:     user,
                        language: Language.first,
                        license:  License.first
    end
  end
end
