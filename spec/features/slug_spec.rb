require 'rails_helper'

RSpec.describe 'Slugs', type: :model do
  it 'Creates a slug from entity', doing: true do
    license = FactoryBot.create :license, name: 'MIT License'

    expect(license.slug).to eq('mit-license')
  end

  it 'creates a logical suite of number if slugs are already taken' do
    FactoryBot.create :license, name: 'MIT License'
    license = FactoryBot.create :license, name: 'MIT License'
    expect(license.slug).to eq('mit-license-1')
    FactoryBot.create :license, name: 'MIT License'
    license = FactoryBot.create :license, name: 'MIT License 8'
    expect(license.slug).to eq('mit-license-8')
    license = FactoryBot.create :license, name: 'MIT License 8'
    expect(license.slug).to eq('mit-license-8-1')
    license = FactoryBot.create :license, name: 'MIT License'
    expect(license.slug).to eq('mit-license-4')
  end

  it 'creates a slug based on current time when field is created_at' do
    note = FactoryBot.create :note
    time = Time.current
    expect(note.slug).to match(/^#{time.strftime("%Y-%m-%d-%H")}-(.*)$/)
  end
end
