require 'rails_helper'

RSpec.describe 'Counter caches', type: :model do
  it 'increments counter caches when creating a full published upload' do
    user   = create_user_with_published_content
    upload = user.uploads.first

    expect(user.uploads_count).to eq(1)
    expect(upload.language.uploads_count).to eq(1)
    expect(upload.license.uploads_count).to eq(1)
    expect(upload.tags.first.uploads_count).to eq(1)
    expect(upload.projects.first.uploads_count).to eq(1)
    expect(upload.albums.first.uploads_count).to eq(1)
  end

  it 'doesn\'t change counter caches when creating a full unpublished upload' do
    user   = create_user_with_unpublished_content
    upload = user.uploads.first

    expect(user.uploads_count).to eq(0)
    expect(upload.language.uploads_count).to eq(0)
    expect(upload.license.uploads_count).to eq(0)
    expect(upload.tags.first.uploads_count).to eq(0)
    expect(upload.projects.first.uploads_count).to eq(0)
    expect(upload.albums.first.uploads_count).to eq(0)
  end

  it 'decrements counter caches when deleting a full published upload' do
    user     = create_user_with_published_content
    upload   = user.uploads.first
    language = upload.language
    license  = upload.license
    tag      = upload.tags.first

    upload.destroy

    user.reload
    language.reload
    license.reload
    tag.reload

    expect(user.uploads_count).to eq(0)
    expect(language.uploads_count).to eq(0)
    expect(license.uploads_count).to eq(0)
    expect(tag.uploads_count).to eq(0)
    expect(user.projects.first.uploads_count).to eq(0)
    expect(user.albums.first.uploads_count).to eq(0)
  end

  it 'decrements counter caches when unpublishing a full published upload' do
    user   = create_user_with_published_content
    upload = user.uploads.first

    upload.update(published: false)
    upload.reload

    expect(upload.user.uploads_count).to eq(0)
    expect(upload.language.uploads_count).to eq(0)
    expect(upload.license.uploads_count).to eq(0)
    expect(upload.tags.first.uploads_count).to eq(0)
    expect(upload.projects.first.uploads_count).to eq(0)
    expect(upload.albums.first.uploads_count).to eq(0)
  end

  it 'decrements counter caches when locking a full published upload' do
    user   = create_user_with_published_content
    upload = user.uploads.first

    upload.update(locked: true)
    upload.reload

    expect(upload.user.uploads_count).to eq(0)
    expect(upload.language.uploads_count).to eq(0)
    expect(upload.license.uploads_count).to eq(0)
    expect(upload.tags.first.uploads_count).to eq(0)
    expect(upload.projects.first.uploads_count).to eq(0)
    expect(upload.albums.first.uploads_count).to eq(0)
  end

  it 'doesn\'t change counter caches when locking a full unpublished upload' do
    user   = create_user_with_unpublished_content
    upload = user.uploads.first

    upload.update(locked: true)
    upload.reload

    expect(upload.user.uploads_count).to eq(0)
    expect(upload.language.uploads_count).to eq(0)
    expect(upload.license.uploads_count).to eq(0)
    expect(upload.tags.first.uploads_count).to eq(0)
    expect(upload.projects.first.uploads_count).to eq(0)
    expect(upload.albums.first.uploads_count).to eq(0)
  end
end
