require 'rails_helper'

# Specs in this file have access to a helper object that includes
# the StringHelper. For example:
#
# describe StringHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       expect(helper.concat_strings("this","that")).to eq("this that")
#     end
#   end
# end
RSpec.describe StringHelper, type: :helper do
  describe 'ellipsis' do
    it 'truncates at the right length' do
      expect(helper.ellipsis('hello world', 5)).to eq('hell…')
    end

    it 'does not truncates string if not necessary' do
      expect(helper.ellipsis('hello world', 100)).to eq('hello world')
    end

    it 'support custom symbol' do
      expect(helper.ellipsis('some sentence', 12, '[redacted]')).to eq('so[redacted]')
    end
  end

  describe 'human_model_name' do
    it 'returns a correct name' do
      expect(helper.human_model_name('albums')).to eq('Album')
      expect(helper.human_model_name('album', 2)).to eq('Albums')
    end

    it 'handles symbols' do
      expect(helper.human_model_name(:albums)).to eq('Album')
      expect(helper.human_model_name(:album, 2)).to eq('Albums')
    end
  end
end
