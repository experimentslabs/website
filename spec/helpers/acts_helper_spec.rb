require 'rails_helper'

# Specs in this file have access to a helper object that includes
# the ActsHelper. For example:
#
# describe ActsHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       expect(helper.concat_strings("this","that")).to eq("this that")
#     end
#   end
# end
RSpec.describe ActsHelper, type: :helper do
  describe 'event string' do
    it 'returns a correct string' do
      expect(helper.act_action('publish')).to eq('published')
    end

    it 'returns a notice when the action is unknown' do
      expect(helper.act_action('exterminate')).to eq('[missing: exterminate]')
    end
  end
end
