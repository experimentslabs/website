require 'rails_helper'

RSpec.describe Notification, type: :model do
  include ActiveJob::TestHelper

  describe 'send notification emails' do
    let(:user) { FactoryBot.create :user_active }
    let(:entity) { FactoryBot.create :album_published, user: user }
    let(:admin) { FactoryBot.create :user_admin_active }

    it 'notifies the content author by mail if commenter is not author' do
      expect do
        perform_enqueued_jobs do
          FactoryBot.create :comment, content: entity
        end
      end.to change(ActionMailer::Base.deliveries, :count).by(1)
    end

    it 'does not notifies the content author by mail if commenter is the author' do
      expect do
        perform_enqueued_jobs do
          FactoryBot.create :comment, content: entity, user: user
        end
      end.to change(ActionMailer::Base.deliveries, :count).by(0)
    end

    it 'notifies the content author by mail when content is locked' do
      expect do
        perform_enqueued_jobs do
          entity.update(locked: true, changed_by: admin)
        end
      end.to change(ActionMailer::Base.deliveries, :count).by(1)
    end

    it 'notifies admins when content is reported' do
      FactoryBot.create_list :user_admin_active, 3
      admins_count = User.where(role: 'admin').count
      expect do
        perform_enqueued_jobs do
          FactoryBot.create :report
        end
      end.to change(ActionMailer::Base.deliveries, :count).by(admins_count)
    end
  end
end
