require 'rails_helper'

RSpec.describe Comment, type: :model do
  describe 'create comments' do
    %w[album article note project upload].each do |singular_entity|
      it 'notifies the content author if commenter is not author' do
        entity    = FactoryBot.create "#{singular_entity}_published"
        commenter = FactoryBot.create :user_active
        expect do
          described_class.create comment: 'Some fine comment', content: entity, user: commenter
        end.to change(Notification, :count).by(1)
      end

      it 'notifies the content author if commenter is a visitor' do
        entity = FactoryBot.create "#{singular_entity}_published"
        expect do
          described_class.create comment: 'Some fine comment', content: entity, name: 'John Doe', email: 'john@doe.com'
        end.to change(Notification, :count).by(1)
      end

      it 'does not notify the content author if poster' do
        entity = FactoryBot.create "#{singular_entity}_published"
        expect do
          described_class.create comment: 'Some fine comment', content: entity, user: entity.user
        end.to change(Notification, :count).by(0)
      end
    end
  end
end
