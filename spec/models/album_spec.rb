require 'rails_helper'

RSpec.describe Album, type: :model do
  describe 'updates the acts table' do
    it 'creates an entry when publishing an album' do
      expect do
        FactoryBot.create :album_published
      end.to change(Act, :count).by(1)
    end

    it 'creates an entry when updating a published album' do
      expect do
        album = FactoryBot.create :album_published
        album.reload
        album.update!(name: 'New title')
        album.reload
        album.update!(description: 'New description')
      end.to change(Act, :count).by(3)
    end

    it 'deletes all entries when locking a published album' do
      expect do
        album = FactoryBot.create :album_published
        album.update(locked: true)
      end.to change(Act, :count).by(1) # removes all, creates a new act
    end

    it 'deletes all entries when unpublishing a published album' do
      expect do
        album = FactoryBot.create :album_published
        album.update(published: false)
      end.to change(Act, :count).by(1) # removes all, creates a new act
    end

    it 'deletes all entries when deleting a published album' do
      expect do
        album = FactoryBot.create :album_published
        album.destroy
      end.to change(Act, :count).by(0)
    end

    it 'does nothing when saving a draft' do
      expect do
        FactoryBot.create :album
      end.to change(Act, :count).by(0)
    end

    it 'does nothing when deleting a draft' do
      expect do
        album = FactoryBot.create :album
        album.destroy
      end.to change(Act, :count).by(0)
    end

    it 'does nothing when locking a draft' do
      expect do
        album = FactoryBot.create :album
        album.update(locked: true)
      end.to change(Act, :count).by(0)
    end
  end
end
