RSpec.shared_context 'with authenticated member', shared_context: :metadata do
  before do
    # @request.env['devise.mapping'] = Devise.mappings[:user]
    sign_in User.find_by(email: 'member@example.com')
  end
end

RSpec.shared_context 'with authenticated admin', shared_context: :metadata do
  before do
    # @request.env['devise.mapping'] = Devise.mappings[:user]
    sign_in User.find_by(email: 'admin@example.com')
  end
end

# rubocop:disable Metrics/BlockLength
RSpec.shared_context 'with full content', shared_context: :metadata do
  # Known content
  user     = FactoryBot.create :user
  license  = FactoryBot.create :license
  tag      = FactoryBot.create :tag
  language = FactoryBot.create :language
  project  = FactoryBot.create :project_published, user: user, language: language, license: license, tags: [tag]
  album    = FactoryBot.create :album_published,   user: user, language: language, license: license, tags: [tag], projects: [project]
  article  = FactoryBot.create :article_published, user: user, language: language, license: license, tags: [tag], projects: [project]
  note     = FactoryBot.create :note_published,    user: user, language: language, license: license, tags: [tag], projects: [project]
  upload   = FactoryBot.create :upload_published,  user: user, language: language, license: license, tags: [tag], projects: [project], albums: [album]
  link     = FactoryBot.create :link_published,    user: user, language: language, tags: [tag]

  # Unknown content only here to verify the filters
  user2     = FactoryBot.create :user
  license2  = FactoryBot.create :license
  tag2      = FactoryBot.create :tag
  language2 = FactoryBot.create :language
  project2  = FactoryBot.create :project_published, user: user2, language: language2, license: license2, tags: [tag2]
  album2    = FactoryBot.create :album_published,   user: user2, language: language2, license: license2, tags: [tag2], projects: [project2]
  FactoryBot.create :article_published, user: user2, language: language2, license: license2, tags: [tag2], projects: [project2]
  FactoryBot.create :note_published,    user: user2, language: language2, license: license2, tags: [tag2], projects: [project2]
  FactoryBot.create :upload_published,  user: user2, language: language2, license: license2, tags: [tag2], projects: [project2], albums: [album2]
  FactoryBot.create :link_published,    user: user2, language: language2, tags: [tag2]

  let(:filter_values) do
    {
      user:     user,
      license:  license,
      tag:      tag,
      language: language,
      album:    album,
      article:  article,
      note:     note,
      project:  project,
      upload:   upload,
      link:     link,
    }
  end
end
# rubocop:enable Metrics/BlockLength
