RSpec.shared_examples 'member index entity' do |model, valid_session = {}|
  routes { Rails.application.routes }
  let(:singular_entity) { model.name.demodulize.downcase }

  include_context 'with authenticated member'

  it 'returns a success response' do
    FactoryBot.create singular_entity, user: controller.current_user
    get :index, params: {}, session: valid_session
    expect(response).to be_successful
  end
end

RSpec.shared_examples 'member show entity' do |model, valid_session = {}|
  routes { Rails.application.routes }
  let(:singular_entity) { model.name.demodulize.downcase }
  let(:ident_field) { model.const_defined?(:SLUG_FIELD) ? model::SLUG_FIELD : :id }

  include_context 'with authenticated member'

  describe 'GET #show' do
    it 'returns a success response' do
      entity = FactoryBot.create singular_entity, user: controller.current_user
      get :show, params: { ident_field => entity.to_param }, session: valid_session
      expect(response).to be_successful
    end
  end
end

RSpec.shared_examples 'member create entity form' do |valid_session = {}|
  routes { Rails.application.routes }
  include_context 'with authenticated member'

  it 'returns a success response' do
    get :new, params: {}, session: valid_session
    expect(response).to be_successful
  end
end

RSpec.shared_examples 'member create entity' do |model, invalid_attributes = {}, valid_session = {}, attachment_field = nil|
  routes { Rails.application.routes }
  let(:singular_entity) { model.name.demodulize.downcase }
  let(:fixture_file) { File.join 'files', 'Woody.jpg' }

  include_context 'with authenticated member'

  describe 'POST #create' do
    context 'with valid params' do
      it "creates a new #{model.name}" do
        entity                   = FactoryBot.build(singular_entity).attributes.symbolize_keys
        entity[attachment_field] = fixture_file_upload(fixture_file, 'image/jpeg', true) unless attachment_field.nil?
        expect do
          post :create, params: { singular_entity => entity }, session: valid_session
        end.to change(model, :count).by(1)
      end

      it "redirects to the #{model.name.pluralize} list" do
        entity                   = FactoryBot.build(singular_entity).attributes.symbolize_keys
        entity[attachment_field] = fixture_file_upload(fixture_file, 'image/jpeg', true) unless attachment_field.nil?
        post :create, params: { singular_entity => entity }, session: valid_session
        expect(response).to redirect_to("member_#{singular_entity.pluralize}".to_sym)
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'new' template)" do
        post :create, params: { singular_entity => invalid_attributes }, session: valid_session
        expect(response).to be_successful
      end
    end
  end
end

RSpec.shared_examples 'member update entity form' do |model, valid_session = {}|
  routes { Rails.application.routes }
  let(:singular_entity) { model.name.demodulize.downcase }
  let(:ident_field) { model.const_defined?(:SLUG_FIELD) ? model::SLUG_FIELD : :id }

  include_context 'with authenticated member'

  describe 'GET #edit' do
    it 'returns a success response' do
      entity = FactoryBot.create singular_entity, user: controller.current_user
      get :edit, params: { ident_field => entity.to_param }, session: valid_session
      expect(response).to be_successful
    end
  end
end

RSpec.shared_examples 'member update entity' do |model, invalid_attributes = {}, valid_session = {}|
  routes { Rails.application.routes }
  let(:singular_entity) { model.name.demodulize.downcase }
  let(:ident_field) { model.const_defined?(:SLUG_FIELD) ? model::SLUG_FIELD : :id }

  include_context 'with authenticated member'

  describe 'PUT #update' do
    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'edit' template)" do
        entity = FactoryBot.create singular_entity, user: controller.current_user
        put :update, params: { ident_field => entity.to_param, singular_entity => invalid_attributes }, session: valid_session
        expect(response).to be_successful
      end
    end
  end
end

RSpec.shared_examples 'member destroy entity' do |model, valid_session = {}|
  routes { Rails.application.routes }
  let(:singular_entity) { model.name.demodulize.downcase }
  let(:ident_field) { model.const_defined?(:SLUG_FIELD) ? model::SLUG_FIELD : :id }

  include_context 'with authenticated member'

  it "destroys the requested #{model.name}" do
    entity = FactoryBot.create singular_entity, user: controller.current_user
    expect do
      delete :destroy, params: { ident_field => entity.to_param }, session: valid_session
    end.to change(model, :count).by(-1)
  end

  it "redirects to the #{model.name.pluralize} list" do
    entity = FactoryBot.create singular_entity, user: controller.current_user
    delete :destroy, params: { ident_field => entity.to_param }, session: valid_session
    expect(response).to redirect_to("member_#{singular_entity.pluralize}".to_sym)
  end
end
