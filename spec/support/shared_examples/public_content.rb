RSpec.shared_examples 'public index content' do |model, valid_session = {}, valid_nested_routes = []|
  routes { Rails.application.routes }
  let(:singular_entity) { model.name.demodulize.downcase }

  describe 'GET #index' do
    it 'returns a success response' do
      FactoryBot.create "#{singular_entity}_published"
      get :index, params: {}, session: valid_session
      expect(response).to be_successful
    end

    describe 'has nested resources' do
      include_context 'with full content'
      valid_nested_routes.each do |sub_model|
        it "filters by #{sub_model}" do
          sub_model_class = sub_model.classify.to_s.constantize
          sub_model_ident_field = sub_model_class.const_defined?(:SLUG_FIELD) ? sub_model_class::SLUG_FIELD : :id
          get :index, params: { "#{sub_model}_#{sub_model_ident_field}" => filter_values[sub_model.to_sym][sub_model_ident_field] }, session: valid_session
          expect(assigns(singular_entity.pluralize.to_sym)).to eq([filter_values[singular_entity.to_sym]])
        end
      end
    end
  end
end

RSpec.shared_examples 'public show content' do |model, valid_session = {}|
  routes { Rails.application.routes }
  let(:singular_entity) { model.name.demodulize.downcase }
  let(:ident_field) { model.const_defined?(:SLUG_FIELD) ? model::SLUG_FIELD : :id }

  describe 'GET #show' do
    it 'returns a success response when entity is published' do
      entity = FactoryBot.create "#{singular_entity}_published"
      get :show, params: { ident_field => entity.to_param }, session: valid_session
      expect(response).to be_successful
    end

    it 'returns a success response when entity is published and locked' do
      entity = FactoryBot.create "#{singular_entity}_published", locked: true
      get :show, params: { ident_field => entity.to_param }, session: valid_session
      expect(response).to be_successful
    end

    it 'returns a 404 response when entity is unpublished' do
      entity = FactoryBot.create singular_entity
      expect do
        get :show, params: { ident_field => entity.to_param }, session: valid_session
      end.to raise_error(ActiveRecord::RecordNotFound)
    end
  end
end
