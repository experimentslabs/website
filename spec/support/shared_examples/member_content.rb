RSpec.shared_examples 'member create content' do |model, valid_session = {}, belongs_to_content = [], attachment_field = nil|
  routes { Rails.application.routes }
  let(:singular_entity) { model.name.demodulize.downcase }
  let(:fixture_file) { File.join 'files', 'Woody.jpg' }

  include_context 'with authenticated member'

  describe 'with some tags' do
    it 'adds tags' do
      FactoryBot.create :tag, name: 'c++'
      entity                   = FactoryBot.build(singular_entity).attributes.symbolize_keys
      entity[attachment_field] = fixture_file_upload(fixture_file, 'image/jpeg', true) unless attachment_field.nil?
      entity[:tags_list]       = 'php, c++, tag with space'
      expect do
        post :create, params: { singular_entity => entity }, session: valid_session
      end.to change(Tag, :count).by(2)
    end
  end

  describe 'attached to relations' do
    belongs_to_content.each do |relation|
      it "is saved with owned related #{relation}" do
        owned_element = FactoryBot.create relation, user: controller.current_user
        expect do
          entity                   = FactoryBot.build(singular_entity, user: controller.current_user).attributes.symbolize_keys
          entity[attachment_field] = fixture_file_upload(fixture_file, 'image/jpeg', true) unless attachment_field.nil?
          post :create,
               params:  {
                 singular_entity => entity.merge("#{relation}_ids": [owned_element.id]),
               },
               session: valid_session
          expect(response).to redirect_to("member_#{singular_entity.pluralize}".to_sym)
        end.to change(model, :count).by(1)
        expect(model.last.send(relation.pluralize).first).to eq(owned_element)
      end

      it "is not saved with non-owned related #{relation}" do
        non_owned_element = FactoryBot.create relation.to_sym
        expect do
          entity                   = FactoryBot.build(singular_entity, user: controller.current_user).attributes.symbolize_keys
          entity[attachment_field] = fixture_file_upload(fixture_file, 'image/jpeg', true) unless attachment_field.nil?
          post :create,
               params:  {
                 singular_entity => entity.merge("#{relation}_ids": [non_owned_element.id]),
               },
               session: valid_session
          expect(response).to be_successful
        end.to change(model, :count).by(0)
      end
    end
  end

  describe 'hidden in history' do
    [0, 1].each do |i|
      it "#{i == 0 ? 'not ' : ''}creates acts when #{i == 0 ? 'hidden' : 'not hidden'}" do
        entity                     = FactoryBot.build("#{singular_entity}_published".to_sym).attributes.symbolize_keys
        entity[attachment_field]   = fixture_file_upload(fixture_file, 'image/jpeg', true) unless attachment_field.nil?
        entity[:hidden_in_history] = i == 0
        expect do
          post :create, params: { singular_entity => entity }, session: valid_session
        end.to change(Act, :count).by(i)
      end
    end
  end
end

RSpec.shared_examples 'member update content' do |model, new_attributes = {}, valid_session = {}, belongs_to_content = []|
  routes { Rails.application.routes }
  let(:singular_entity) { model.name.demodulize.downcase }
  let(:ident_field) { model.const_defined?(:SLUG_FIELD) ? model::SLUG_FIELD : :id }

  include_context 'with authenticated member'

  describe 'with some tags' do
    it 'updates attached tags' do
      tags                   = FactoryBot.create_list :tag, 2
      entity                 = FactoryBot.create singular_entity, user: controller.current_user, tags: tags
      new_entity             = entity.attributes.symbolize_keys.merge(new_attributes)
      new_entity[:tags_list] = "#{tags[0].name}, newtag, othernewtag"
      put :update, params: { ident_field => entity.to_param, singular_entity => new_entity }, session: valid_session
      entity.reload
      expect(entity.tags.count).to eq(3)
    end
  end

  describe 'attached to relations' do
    belongs_to_content.each do |relation|
      it "is saved with owned related #{relation}" do
        owned_element = FactoryBot.create relation.to_sym, user: controller.current_user
        entity        = FactoryBot.create singular_entity, user: controller.current_user
        element       = entity.attributes.symbolize_keys.merge("#{relation}_ids": [owned_element.id])
        put :update, params: { ident_field => entity.to_param, singular_entity => element }, session: valid_session
        expect(response).to redirect_to("member_#{singular_entity.pluralize}".to_sym)
        expect(entity.send(relation.pluralize).count).to eq(1)
      end

      it "is not saved with non-owned related #{relation}" do
        non_owned_element = FactoryBot.create relation.to_sym
        entity            = FactoryBot.create singular_entity, user: controller.current_user
        element           = entity.attributes.symbolize_keys.merge("#{relation}_ids": [non_owned_element.id])
        put :update, params: { ident_field => entity.to_param, singular_entity => element }, session: valid_session
        expect(entity.send(relation.pluralize).count).to eq(0)
      end
    end
  end

  describe 'hidden in history' do
    if model::ACT_UPDATES
      [0, 1].each do |i|
        it "#{i == 0 ? 'not ' : ''}creates acts when #{i == 0 ? 'hidden' : 'not hidden'}" do
          entity           = FactoryBot.create "#{singular_entity}_published".to_sym, user: controller.current_user, hidden_in_history: (i == 0)
          new_entity       = entity.attributes.symbolize_keys.merge(new_attributes).merge(sfw: false)
          expect do
            put :update, params: { ident_field => entity.to_param, singular_entity => new_entity }, session: valid_session
          end.to change(Act, :count).by(i)
        end
      end
    else
      puts "Act update ignored for model #{model}"
    end
  end
end

RSpec.shared_examples 'member publish content' do |model, valid_session = {}|
  routes { Rails.application.routes }
  let(:singular_entity) { model.name.demodulize.downcase }
  let(:ident_field) { model.const_defined?(:SLUG_FIELD) ? model::SLUG_FIELD : :id }

  include_context 'with authenticated member'

  describe 'PUT #toggle_publication' do
    it "toggles the requested #{model.name}" do
      entity = FactoryBot.create singular_entity, user: controller.current_user
      put :toggle_publication, params: { "#{singular_entity}_#{ident_field}" => entity.to_param }, session: valid_session
      entity.reload
      expect(entity).to be_published
    end

    it "redirects to the #{model.name.pluralize} list" do
      album = FactoryBot.create singular_entity, user: controller.current_user
      put :toggle_publication, params: { "#{singular_entity}_#{ident_field}" => album.to_param }, session: valid_session
      expect(response).to redirect_to("member_#{singular_entity.pluralize}".to_sym)
    end

    describe 'hidden in history' do
      [0, 1].each do |i|
        it "#{i == 0 ? 'not ' : ''}creates acts when #{i == 0 ? 'hidden' : 'not hidden'}" do
          entity = FactoryBot.create singular_entity.to_s.to_sym, user: controller.current_user, hidden_in_history: (i == 0)
          expect do
            put :toggle_publication, params: { "#{singular_entity}_#{ident_field}" => entity.to_param }, session: valid_session
          end.to change(Act, :count).by(i)
        end
      end
    end
  end
end
