RSpec.shared_examples 'public index entity' do |model, valid_session = {}, valid_nested_routes = []|
  routes { Rails.application.routes }
  let(:singular_entity) { model.name.demodulize.downcase }

  it 'returns a success response' do
    FactoryBot.create singular_entity
    get :index, params: {}, session: valid_session
    expect(response).to be_successful
  end

  describe 'has nested resources' do
    include_context 'with full content'
    valid_nested_routes.each do |sub_model|
      it "filters by #{sub_model}" do
        sub_model_class = sub_model.classify.to_s.constantize
        sub_model_ident_field = sub_model_class.const_defined?(:SLUG_FIELD) ? sub_model_class::SLUG_FIELD : :id
        get :index, params: { sub_model_ident_field => filter_values[sub_model.to_sym].send(sub_model_ident_field) }, session: valid_session
        expect(assigns(singular_entity.pluralize.to_sym)).to eq([filter_values[singular_entity.to_sym]])
      end
    end
  end
end

RSpec.shared_examples 'public show entity' do |model, valid_session = {}|
  routes { Rails.application.routes }
  let(:singular_entity) { model.name.demodulize.downcase }
  let(:ident_field) { model.const_defined?(:SLUG_FIELD) ? model::SLUG_FIELD : :id }

  describe 'GET #show' do
    it 'returns a success response' do
      entity = FactoryBot.create singular_entity
      get :show, params: { ident_field => entity.to_param }, session: valid_session
      expect(response).to be_successful
    end
  end
end
