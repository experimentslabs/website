RSpec.shared_examples 'admin index entity' do |model, valid_session = {}|
  routes { Rails.application.routes }
  let(:singular_entity) { model.name.demodulize.downcase }

  include_context 'with authenticated admin'

  describe 'GET #index' do
    it 'returns a success response' do
      FactoryBot.create singular_entity
      get :index, params: {}, session: valid_session
      expect(response).to be_successful
    end
  end
end

RSpec.shared_examples 'admin show entity' do |model, valid_session = {}|
  routes { Rails.application.routes }
  let(:singular_entity) { model.name.demodulize.downcase }
  let(:ident_field) { model.const_defined?(:SLUG_FIELD) ? model::SLUG_FIELD : :id }

  include_context 'with authenticated admin'

  describe 'GET #show' do
    it 'returns a success response' do
      entity = FactoryBot.create singular_entity
      get :show, params: { ident_field => entity.to_param }, session: valid_session
      expect(response).to be_successful
    end
  end
end

RSpec.shared_examples 'admin create entity form' do |valid_session = {}|
  routes { Rails.application.routes }
  include_context 'with authenticated admin'

  it 'returns a success response' do
    get :new, params: {}, session: valid_session
    expect(response).to be_successful
  end
end

RSpec.shared_examples 'admin create entity' do |model, invalid_attributes = {}, valid_session = {}|
  routes { Rails.application.routes }
  let(:singular_entity) { model.name.demodulize.downcase }

  include_context 'with authenticated admin'

  describe 'POST #create' do
    context 'with valid params' do
      it "creates a new #{model.name.demodulize.downcase}" do
        expect do
          post :create, params: { singular_entity => FactoryBot.build(singular_entity).attributes.symbolize_keys }, session: valid_session
        end.to change(model, :count).by(1)
      end

      it "redirects to the #{model.name.demodulize.downcase.pluralize} list" do
        post :create, params: { singular_entity => FactoryBot.build(singular_entity).attributes.symbolize_keys }, session: valid_session
        expect(response).to redirect_to("admin_#{singular_entity.pluralize}".to_sym)
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'new' template)" do
        post :create, params: { singular_entity => invalid_attributes }, session: valid_session
        expect(response).to be_successful
      end
    end
  end
end

RSpec.shared_examples 'admin update entity form' do |model, valid_session = {}|
  routes { Rails.application.routes }
  let(:singular_entity) { model.name.demodulize.downcase }
  let(:ident_field) { model.const_defined?(:SLUG_FIELD) ? model::SLUG_FIELD : :id }

  include_context 'with authenticated admin'

  describe 'GET #edit' do
    it 'returns a success response' do
      entity = FactoryBot.create singular_entity
      get :edit, params: { ident_field => entity.to_param }, session: valid_session
      expect(response).to be_successful
    end
  end
end

RSpec.shared_examples 'admin update entity' do |model, invalid_attributes = {}, valid_session = {}|
  routes { Rails.application.routes }
  let(:singular_entity) { model.name.demodulize.downcase }
  let(:ident_field) { model.const_defined?(:SLUG_FIELD) ? model::SLUG_FIELD : :id }

  include_context 'with authenticated admin'

  describe 'PUT #update' do
    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'edit' template)" do
        entity = FactoryBot.create singular_entity
        put :update, params: { ident_field => entity.to_param, singular_entity => invalid_attributes }, session: valid_session
        expect(response).to be_successful
      end
    end
  end
end

RSpec.shared_examples 'admin destroy entity' do |model, valid_session = {}|
  routes { Rails.application.routes }
  let(:singular_entity) { model.name.demodulize.downcase }
  let(:ident_field) { model.const_defined?(:SLUG_FIELD) ? model::SLUG_FIELD : :id }

  include_context 'with authenticated admin'

  it "destroys the requested #{model.name.demodulize.downcase}" do
    entity = FactoryBot.create singular_entity
    expect do
      delete :destroy, params: { ident_field => entity.to_param }, session: valid_session
    end.to change(model, :count).by(-1)
  end

  it "redirects to the #{model.name.demodulize.downcase.pluralize} list" do
    entity = FactoryBot.create singular_entity
    delete :destroy, params: { ident_field => entity.to_param }, session: valid_session
    expect(response).to redirect_to("admin_#{singular_entity.pluralize}".to_sym)
  end
end
