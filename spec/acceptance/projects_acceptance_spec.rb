require 'acceptance_helper'

RSpec.describe ProjectsController do
  resource 'Public - Project', 'Published projects'

  entity :project,
         id:                { type: :integer, description: 'Project identifier' },
         name:              { type: :string, description: 'Name' },
         short_description: { type: :string, description: 'Short description' },
         description:       { type: :string, description: 'Description' },
         main_url:          { type: :string, required: false, description: 'Homepage URL' },
         sources_url:       { type: :string, required: false, description: 'Sources URL' },
         docs_url:          { type: :string, required: false, description: 'Documentation URL' },
         sfw:               { type: :boolean, description: 'Is the project safe for work?' },
         albums_count:      { type: :integer, description: 'Related albums amount' },
         articles_count:    { type: :integer, description: 'Related articles amount' },
         notes_count:       { type: :integer, description: 'Related notes amount' },
         uploads_count:     { type: :integer, description: 'Related uploads amount' },
         slug:              { type: :string, description: 'Slug for URLs' },
         user_id:           { type: :integer, description: 'Author identifier' },
         license_id:        { type: :integer, description: 'License identifier' },
         language_id:       { type: :integer, description: 'Language identifier' },
         tag_ids:           { type: :array, description: 'Tags identifiers' },
         album_ids:         { type: :array, description: 'Associated articles identifiers' },
         article_ids:       { type: :array, description: 'Associated albums identifiers' },
         note_ids:          { type: :array, description: 'Associated notes identifiers' },
         upload_ids:        { type: :array, description: 'Associated uploads identifiers' },
         published_at:      { type: :datetime, required: false, description: 'Publication date' },
         created_at:        { type: :datetime, description: 'Creation date' },
         updated_at:        { type: :datetime, description: 'Last update date' }

  entity :paginated_response,
         content:  { type: :array, description: 'Projects list', of: :project },
         page:     { type: :integer, description: 'Current page' },
         pages:    { type: :integer, description: 'Total amount of pages' },
         per_page: { type: :integer, description: 'Amount of projects per page' },
         total:    { type: :integer, description: 'Total amount of projects' }

  entity :error,
         error: { type: :string, description: 'Error message' }

  entity :comment_response,
         message: { type: :string, description: 'Success message' }

  entity :comment_form_errors,
         user:    { type: :array, required: false, description: 'User-related errors when user is signed in' },
         name:    { type: :array, required: false, description: 'User name errors when user is a visitor' },
         comment: { type: :array, required: false, description: 'Comment related errors' }

  parameters :common_path_params,
             slug: { type: :string, description: 'Project slug' }

  parameters :comment_form_attributes,
             name:          { type: :string, required: false, description: "Commenter name. No need to specify it if user is logged in. It's required for visitors" },
             email:         { type: :string, required: false, description: "Commenter email. No need to specify it if user is logged in. It's NOT required for anyone" },
             comment:       { type: :string, description: 'The comment' },
             allow_contact: { type: :boolean, required: false, description: 'Does commenter accepts to be contacted by project author? It will be true for logged in users' }

  let(:user) { FactoryBot.create :user_active }
  let(:license) { FactoryBot.create :license }
  let(:language) { FactoryBot.create :language }
  let(:tag) { FactoryBot.create :tag }
  let(:project) { FactoryBot.create :project_published, tags: [tag], user: user, license: license, language: language }

  let(:slug) { project.slug }
  let(:username) { user.username }
  let(:license_slug) { license.slug }
  let(:iso639_1) { language.iso639_1 }
  let(:tag_slug) { tag.slug }

  before do
    project
  end

  on_get '/projects', 'List projects' do
    for_code 200 do |url|
      visit url
      expect(response).to have_one defined :paginated_response
    end
  end

  on_get '/users/:username/projects', "List user's projects" do
    path_params fields: { username: { type: :string, description: 'An user' } }

    for_code 200 do |url|
      visit url
      expect(response).to have_one defined :paginated_response
    end
  end

  on_get '/licenses/:license_slug/projects', 'List projects with a given license' do
    path_params fields: { license_slug: { type: :string, description: 'A license' } }

    for_code 200 do |url|
      visit url
      expect(response).to have_one defined :paginated_response
    end
  end

  on_get '/languages/:iso639_1/projects', 'List projects with a given language' do
    path_params fields: { iso639_1: { type: :string, description: 'A language' } }

    for_code 200 do |url|
      visit url
      expect(response).to have_one defined :paginated_response
    end
  end

  on_get '/tags/:tag_slug/projects', 'List projects with a given tag' do
    path_params fields: { tag_slug: { type: :string, description: 'A tag' } }

    for_code 200 do |url|
      visit url
      expect(response).to have_one defined :paginated_response
    end
  end

  on_get '/projects/:slug', 'Display one project' do
    path_params defined: :common_path_params

    for_code 200 do |url|
      visit url

      expect(response).to have_one defined :project
    end

    for_code 404 do |url|
      visit url, path_params: { slug: '0' }

      expect(response).to have_one defined :error
    end
  end

  on_post '/projects/:slug/comment', 'Leave a comment' do
    path_params defined: :common_path_params
    request_params defined: :comment_form_attributes

    for_code 201 do |url|
      visit url, payload: { comment: { name: 'John Doe', comment: 'Thank you!' } }

      expect(response).to have_one defined :comment_response
    end

    for_code 422 do |url|
      visit url, payload: { comment: { name: nil, comment: 'Does it works?' } }

      expect(response).to have_one defined :comment_form_errors
    end

    for_code 404 do |url|
      visit url, path_params: { slug: '0' }, payload: { comment: { name: 'John Doe', comment: 'Thank you!' } }

      expect(response).to have_one defined :error
    end
  end
end
