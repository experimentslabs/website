require 'acceptance_helper'

RSpec.describe Member::NotificationsController do
  resource 'Member - Notification', 'Notification management'

  entity :notification,
         id:             { type: :integer, description: 'Notification identifier' },
         content:        { type: :object, description: 'Related entity (comment, report,...)' },
         event:          { type: :string, description: 'Event type, can be "comment", "report", "lock" or "unlock"' },
         user_id:        { type: :integer, description: 'Concerned user identifier' },
         source_user_id: { type: :integer, required: false, description: 'User source of the notification, if any' },
         created_at:     { type: :datetime, description: 'Creation date' },
         updated_at:     { type: :datetime, description: 'Update date' }

  entity :error,
         error: { type: :string, description: 'Error message' }

  let(:user) { FactoryBot.create :user_active }
  let(:article) { FactoryBot.create :article_published, user: user }
  let(:notification) do
    FactoryBot.create :comment, content: article
    Notification.last
  end

  before do
    notification

    sign_in user
  end

  on_get '/member/notifications', 'List notifications' do
    for_code 200 do |url|
      visit url

      expect(response).to have_many defined :notification
    end
  end

  on_delete '/member/notifications/:id', 'Destroys a notification' do
    path_params fields: {
      id: { type: :integer, description: 'Notification identifier' },
    }
    for_code 204 do |url|
      visit url, path_params: { id: notification.id }
    end

    for_code 404 do |url|
      visit url, path_params: { id: 0 }

      expect(response).to have_one defined :error
    end
  end
end
