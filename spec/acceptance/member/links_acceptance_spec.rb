require 'acceptance_helper'

RSpec.describe Member::LinksController do
  resource 'Member - Link', 'Manage your links'

  entity :link,
         id:                { type: :integer, description: 'Link identifier' },
         title:             { type: :string, description: 'Title' },
         description:       { type: :string, required: false, description: 'Description' },
         url:               { type: :string, description: 'URL' },
         sfw:               { type: :boolean, description: 'Is the link safe for work?' },
         published:         { type: :boolean, description: 'Is the link published by the author?' },
         locked:            { type: :boolean, description: 'Is the link locked by administrators?' },
         hidden_in_history: { type: :boolean, description: 'Are the changes shown in activity list?' },
         user_id:           { type: :integer, description: 'Author identifier' },
         language_id:       { type: :integer, description: 'Language identifier' },
         published_at:      { type: :datetime, required: false, description: 'Publication date' },
         created_at:        { type: :datetime, description: 'Creation date' },
         updated_at:        { type: :datetime, description: 'Last update date' }

  entity :form_error,
         title:    { type: :array, required: false, description: 'Link name errors' },
         url:      { type: :array, required: false, description: 'Link URL errors' },
         language: { type: :array, required: false, description: 'Related language identifier errors' }

  entity :error,
         error: { type: :string, description: 'Error message' }

  parameters :common_path_params,
             id: { type: :string, description: 'Link identifier' }

  # The base form attributes, they are different in POST and PUT
  base_attributes = {
    title:             { type: :string, description: 'Title' },
    description:       { type: :string, description: 'Link description' },
    url:               { type: :string, description: 'URL' },
    sfw:               { type: :boolean, description: 'Is the link safe for work?' },
    published:         { type: :boolean, description: 'Is the link published?' },
    hidden_in_history: { type: :boolean, description: 'Are the changes shown in activity list?' },
    language_id:       { type: :integer, description: 'Related language identifier' },
    tags_list:         { type: :string, description: 'Tags list, comma-separated' },
  }

  parameters :new_form_attributes,
             base_attributes

  parameters :update_form_attributes, base_attributes.merge(
    minor_update:       { type: :boolean, description: 'Minor updates are not recorded in activities' },
    update_description: { type: :string, description: 'If not a minor update, a small description of the changes' }
  )

  let(:user) { FactoryBot.create :user_active }
  let(:links) { FactoryBot.create_list :link, 2, user: user }
  let(:id) { user.links.first.id }

  before do
    # Create content
    links

    sign_in user
  end

  on_get '/member/links', 'List owned links' do
    for_code 200 do |url|
      visit url

      expect(response).to have_many defined :link
    end
  end

  on_post '/member/links', 'Create a link' do
    request_params defined: :new_form_attributes

    for_code 201 do |url|
      payload = { link: {
        title:       'Example domain',
        description: 'This domain is used as example for anything a web dev explains',
        url:         'http://example.com',
        language_id: Language.first.id,
      } }

      visit url, payload: payload

      expect(response).to have_one defined :link
    end

    for_code 422 do |url|
      visit url, payload: { link: { url: '' } }

      expect(response).to have_one defined :form_error
    end
  end

  on_put '/member/links/:id', 'Updates a link' do
    path_params defined: :common_path_params
    request_params defined: :update_form_attributes

    for_code 200 do |url|
      visit url, payload: { link: { url: 'https://example.com' } }

      expect(response).to have_one defined :link
    end

    for_code 422 do |url|
      visit url, payload: { link: { url: '' } }

      expect(response).to have_one defined :form_error
    end

    for_code 404 do |url|
      visit url, path_params: { id: 0 }

      expect(response).to have_one defined :error
    end
  end

  on_put '/member/links/:id/toggle_publication', 'Invert publication state' do
    path_params defined: :common_path_params
    for_code 200 do |url|
      visit url

      expect(response).to have_one defined :link
    end

    for_code 404 do |url|
      visit url, path_params: { id: 0 }

      expect(response).to have_one defined :error
    end
  end

  on_delete '/member/links/:id', 'Destroys a link' do
    path_params defined: :common_path_params

    for_code 204 do |url|
      visit url
    end

    for_code 404 do |url|
      visit url, path_params: { id: 0 }

      expect(response).to have_one defined :error
    end
  end
end
