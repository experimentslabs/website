require 'acceptance_helper'

RSpec.describe Member::UploadsController do
  resource 'Member - Upload', 'Manage your uploads'

  entity :file,
         content_type: { type: :string, description: 'File type' },
         filename:     { type: :string, description: 'File name' },
         metadata:     { type: :object, description: 'ActiveStorage metadata' },
         url:          { type: :string, description: 'URL to access the file' }

  entity :upload,
         id:                { type: :integer, description: 'Upload identifier' },
         title:             { type: :string, description: 'Title' },
         description:       { type: :string, description: 'Description' },
         file:              { type: :object, description: 'File infos', attributes: :file },
         sfw:               { type: :boolean, description: 'Is the upload safe for work?' },
         published:         { type: :boolean, description: 'Is the upload published by the author?' },
         locked:            { type: :boolean, description: 'Is the upload locked by administrators?' },
         hidden_in_history: { type: :boolean, description: 'Are the changes shown in activity list?' },
         slug:              { type: :string, description: 'Slug for URLs' },
         user_id:           { type: :integer, description: 'Author identifier' },
         license_id:        { type: :integer, description: 'License identifier' },
         language_id:       { type: :integer, description: 'Language identifier' },
         published_at:      { type: :datetime, required: false, description: 'Publication date' },
         created_at:        { type: :datetime, description: 'Creation date' },
         updated_at:        { type: :datetime, description: 'Last update date' }

  entity :form_error,
         title:       { type: :array, required: false, description: 'Upload title errors' },
         description: { type: :array, required: false, description: 'Upload description errors' },
         file:        { type: :array, required: false, description: 'Upload file errors' },
         language:    { type: :array, required: false, description: 'Related language identifier errors' },
         license:     { type: :array, required: false, description: 'Related license identifier errors' },
         slug:        { type: :array, required: false, description: 'This errors comes with name errors' }

  entity :error,
         error: { type: :string, description: 'Error message' }

  parameters :common_path_params,
             slug: { type: :string, description: 'Upload slug' }

  # The base form attributes, they are different in POST and PUT
  base_attributes = {
    title:             { type: :string, description: 'Upload name' },
    description:       { type: :string, description: 'Upload description' },
    # file:              { type: :file, description: 'Uploaded file' },
    sfw:               { type: :boolean, description: 'Is the upload safe for work?' },
    published:         { type: :boolean, description: 'Is the upload published?' },
    hidden_in_history: { type: :boolean, description: 'Are the changes shown in activity list?' },
    language_id:       { type: :integer, description: 'Related language identifier' },
    license_id:        { type: :integer, description: 'Related license identifier' },
    tags_list:         { type: :string, description: 'Tags list, comma-separated' },
  }

  parameters :new_form_attributes,
             base_attributes

  parameters :update_form_attributes, base_attributes.merge(
    minor_update:       { type: :boolean, description: 'Minor updates are not recorded in activities' },
    update_description: { type: :string, description: 'If not a minor update, a small description of the changes' }
  )

  let(:user) { FactoryBot.create :user_active }
  let(:uploads) { FactoryBot.create_list :upload, 2, user: user }
  let(:slug) { user.uploads.first.slug }

  before do
    # Create content
    uploads

    sign_in user
  end

  on_get '/member/uploads', 'List owned uploads' do
    for_code 200 do |url|
      visit url

      expect(response).to have_many defined :upload
    end
  end

  on_post '/member/uploads', 'Create an upload' do
    request_params defined: :new_form_attributes

    # FIXME: Add test for code 201 when rspec-rails-api supports file types
    # and form encoded data

    for_code 422 do |url|
      visit url, payload: { upload: { title: '' } }

      expect(response).to have_one defined :form_error
    end
  end

  on_put '/member/uploads/:slug', 'Updates an upload' do
    path_params defined: :common_path_params
    request_params defined: :update_form_attributes

    for_code 200 do |url|
      visit url, payload: { upload: { title: 'Another cat pic' } }

      expect(response).to have_one defined :upload
    end

    for_code 422 do |url|
      visit url, payload: { upload: { title: '' } }

      expect(response).to have_one defined :form_error
    end

    for_code 404 do |url|
      visit url, path_params: { slug: '00' }

      expect(response).to have_one defined :error
    end
  end

  on_put '/member/uploads/:slug/toggle_publication', 'Invert publication state' do
    path_params defined: :common_path_params
    for_code 200 do |url|
      visit url

      expect(response).to have_one defined :upload
    end

    for_code 404 do |url|
      visit url, path_params: { slug: '00' }

      expect(response).to have_one defined :error
    end
  end

  on_delete '/member/uploads/:slug', 'Destroys an upload' do
    path_params defined: :common_path_params

    for_code 204 do |url|
      visit url
    end

    for_code 404 do |url|
      visit url, path_params: { slug: '00' }

      expect(response).to have_one defined :error
    end
  end
end
