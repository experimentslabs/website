require 'acceptance_helper'

RSpec.describe Member::NotesController do
  resource 'Member - Note', 'Manage your notes'

  entity :note,
         id:                { type: :integer, description: 'Note identifier' },
         content:           { type: :string, description: 'Content' },
         sfw:               { type: :boolean, description: 'Is the note safe for work?' },
         published:         { type: :boolean, description: 'Is the note published by the author?' },
         locked:            { type: :boolean, description: 'Is the note locked by administrators?' },
         hidden_in_history: { type: :boolean, description: 'Are the changes shown in activity list?' },
         slug:              { type: :string, description: 'Slug for URLs' },
         user_id:           { type: :integer, description: 'Author identifier' },
         license_id:        { type: :integer, description: 'License identifier' },
         language_id:       { type: :integer, description: 'Language identifier' },
         published_at:      { type: :datetime, required: false, description: 'Publication date' },
         created_at:        { type: :datetime, description: 'Creation date' },
         updated_at:        { type: :datetime, description: 'Last update date' }

  entity :form_error,
         content:  { type: :array, required: false, description: 'Note name errors' },
         language: { type: :array, required: false, description: 'Related language identifier errors' },
         license:  { type: :array, required: false, description: 'Related license identifier errors' },
         slug:     { type: :array, required: false, description: 'This errors comes with name errors' }

  entity :error,
         error: { type: :string, description: 'Error message' }

  parameters :common_path_params,
             slug: { type: :string, description: 'Note slug' }

  # The base form attributes, they are different in POST and PUT
  base_attributes = {
    content:           { type: :string, description: 'Content' },
    sfw:               { type: :boolean, description: 'Is the note safe for work?' },
    published:         { type: :boolean, description: 'Is the note published?' },
    hidden_in_history: { type: :boolean, description: 'Are the changes shown in activity list?' },
    language_id:       { type: :integer, description: 'Related language identifier' },
    license_id:        { type: :integer, description: 'Related license identifier' },
    tags_list:         { type: :string, description: 'Tags list, comma-separated' },
  }

  parameters :new_form_attributes,
             base_attributes

  parameters :update_form_attributes, base_attributes.merge(
    minor_update:       { type: :boolean, description: 'Minor updates are not recorded in activities' },
    update_description: { type: :string, description: 'If not a minor update, a small description of the changes' }
  )

  let(:user) { FactoryBot.create :user_active }
  let(:notes) { FactoryBot.create_list :note, 2, user: user }
  let(:slug) { user.notes.first.slug }

  before do
    # Create content
    notes

    sign_in user
  end

  on_get '/member/notes', 'List owned notes' do
    for_code 200 do |url|
      visit url

      expect(response).to have_many defined :note
    end
  end

  on_post '/member/notes', 'Create a note' do
    request_params defined: :new_form_attributes

    for_code 201 do |url|
      payload = { note: {
        content:     'My first note',
        language_id: Language.first.id,
        license_id:  License.first.id,
      } }
      visit url, payload: payload

      expect(response).to have_one defined :note
    end

    for_code 422 do |url|
      visit url, payload: { note: { content: '' } }

      expect(response).to have_one defined :form_error
    end
  end

  on_put '/member/notes/:slug', 'Updates a note' do
    path_params defined: :common_path_params
    request_params defined: :update_form_attributes

    for_code 200 do |url|
      visit url, payload: { note: { content: 'Cats are evil' } }

      expect(response).to have_one defined :note
    end

    for_code 422 do |url|
      visit url, payload: { note: { content: '' } }

      expect(response).to have_one defined :form_error
    end

    for_code 404 do |url|
      visit url, path_params: { slug: '00' }

      expect(response).to have_one defined :error
    end
  end

  on_put '/member/notes/:slug/toggle_publication', 'Invert publication state' do
    path_params defined: :common_path_params
    for_code 200 do |url|
      visit url

      expect(response).to have_one defined :note
    end

    for_code 404 do |url|
      visit url, path_params: { slug: '00' }

      expect(response).to have_one defined :error
    end
  end

  on_delete '/member/notes/:slug', 'Destroys a note' do
    path_params defined: :common_path_params

    for_code 204 do |url|
      visit url
    end

    for_code 404 do |url|
      visit url, path_params: { slug: '00' }

      expect(response).to have_one defined :error
    end
  end
end
