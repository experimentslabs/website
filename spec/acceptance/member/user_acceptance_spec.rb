require 'acceptance_helper'

RSpec.describe Member::UsersController do
  resource 'User', 'User information'

  entity :user,
         id:                { type: :integer, description: 'User identifier' },
         email:             { type: :string, description: 'Email' },
         confirmed_at:      { type: :datetime, description: 'Account confirmation date' },
         unconfirmed_email: { type: :string, required: false, description: 'Pending email validation' },
         username:          { type: :string, description: 'Username' },
         real_name:         { type: :string, required: false, description: 'Real name' },
         biography:         { type: :string, required: false, description: 'Biography (markdown)' },
         albums_count:      { type: :integer, description: 'Amount of published albums' },
         articles_count:    { type: :integer, description: 'Amount of published articles' },
         notes_count:       { type: :integer, description: 'Amount of published notes' },
         projects_count:    { type: :integer, description: 'Amount of published projects' },
         uploads_count:     { type: :integer, description: 'Amount of published uploads' },
         links_count:       { type: :integer, description: 'Amount of published links' },
         created_at:        { type: :datetime, description: 'Account creation date' },
         updated_at:        { type: :datetime, description: 'Last update date' }

  parameters :user_form_attributes,
             real_name: { type: :string, required: false, description: 'Real name' },
             biography: { type: :string, required: false, description: 'Biography (markdown)' }

  let(:user) { FactoryBot.create :user_active }

  before do
    sign_in user
  end

  on_put '/member/infos', 'Update account details' do
    request_params defined: :user_form_attributes

    for_code 200 do |url|
      visit url, payload: { user: { biography: 'I secretly raise otters to take over the world.' } }

      expect(response).to have_one defined :user
    end
  end
end
