require 'acceptance_helper'

RSpec.describe Member::ArticlesController do
  resource 'Member - Article', 'Manage your articles'

  entity :article,
         id:                { type: :integer, description: 'Article identifier' },
         title:             { type: :string, description: 'Title' },
         excerpt:           { type: :string, description: 'Introduction' },
         content:           { type: :string, description: 'Description' },
         sfw:               { type: :boolean, description: 'Is the article safe for work?' },
         published:         { type: :boolean, description: 'Is the article published by the author?' },
         locked:            { type: :boolean, description: 'Is the article locked by administrators?' },
         hidden_in_history: { type: :boolean, description: 'Are the changes shown in activity list?' },
         slug:              { type: :string, description: 'Slug for URLs' },
         user_id:           { type: :integer, description: 'Author identifier' },
         license_id:        { type: :integer, description: 'License identifier' },
         language_id:       { type: :integer, description: 'Language identifier' },
         published_at:      { type: :datetime, required: false, description: 'Publication date' },
         created_at:        { type: :datetime, description: 'Creation date' },
         updated_at:        { type: :datetime, description: 'Last update date' }

  entity :form_error,
         title:    { type: :array, required: false, description: 'Article title errors' },
         excerpt:  { type: :array, required: false, description: 'Article excerpt errors' },
         content:  { type: :array, required: false, description: 'Article content errors' },
         language: { type: :array, required: false, description: 'Related language identifier errors' },
         license:  { type: :array, required: false, description: 'Related license identifier errors' },
         slug:     { type: :array, required: false, description: 'This errors comes with name errors' }

  entity :error,
         error: { type: :string, description: 'Error message' }

  parameters :common_path_params,
             slug: { type: :string, description: 'Article slug' }

  # The base form attributes, they are different in POST and PUT
  base_attributes = {
    title:             { type: :string, description: 'Article name' },
    excerpt:           { type: :string, description: 'Article introduction' },
    content:           { type: :string, description: 'Article content' },
    sfw:               { type: :boolean, description: 'Is the article safe for work?' },
    published:         { type: :boolean, description: 'Is the article published?' },
    hidden_in_history: { type: :boolean, description: 'Are the changes shown in activity list?' },
    language_id:       { type: :integer, description: 'Related language identifier' },
    license_id:        { type: :integer, description: 'Related license identifier' },
    tags_list:         { type: :string, description: 'Tags list, comma-separated' },
  }

  parameters :new_form_attributes,
             base_attributes

  parameters :update_form_attributes, base_attributes.merge(
    minor_update:       { type: :boolean, description: 'Minor updates are not recorded in activities' },
    update_description: { type: :string, description: 'If not a minor update, a small description of the changes' }
  )

  let(:user) { FactoryBot.create :user_active }
  let(:articles) { FactoryBot.create_list :article, 2, user: user }
  let(:slug) { user.articles.first.slug }

  before do
    # Create content
    articles

    sign_in user
  end

  on_get '/member/articles', 'List owned articles' do
    for_code 200 do |url|
      visit url

      expect(response).to have_many defined :article
    end
  end

  on_post '/member/articles', 'Create an article' do
    request_params defined: :new_form_attributes

    for_code 201 do |url|
      payload = { article: {
        title:       'She is 51 and looks 22',
        excerpt:     'Dermatologists hate her',
        content:     'Greatest article of all time',
        language_id: Language.first.id,
        license_id:  License.first.id,
      } }
      visit url, payload: payload

      expect(response).to have_one defined :article
    end

    for_code 422 do |url|
      visit url, payload: { article: { title: '' } }

      expect(response).to have_one defined :form_error
    end
  end

  on_put '/member/articles/:slug', 'Updates an article' do
    path_params defined: :common_path_params
    request_params defined: :update_form_attributes

    for_code 200 do |url|
      visit url, payload: { article: { title: 'Cats' } }

      expect(response).to have_one defined :article
    end

    for_code 422 do |url|
      visit url, payload: { article: { title: '' } }

      expect(response).to have_one defined :form_error
    end

    for_code 404 do |url|
      visit url, path_params: { slug: '00' }

      expect(response).to have_one defined :error
    end
  end

  on_put '/member/articles/:slug/toggle_publication', 'Invert publication state' do
    path_params defined: :common_path_params
    for_code 200 do |url|
      visit url

      expect(response).to have_one defined :article
    end

    for_code 404 do |url|
      visit url, path_params: { slug: '00' }

      expect(response).to have_one defined :error
    end
  end

  on_delete '/member/articles/:slug', 'Destroys an article' do
    path_params defined: :common_path_params

    for_code 204 do |url|
      visit url
    end

    for_code 404 do |url|
      visit url, path_params: { slug: '00' }

      expect(response).to have_one defined :error
    end
  end
end
