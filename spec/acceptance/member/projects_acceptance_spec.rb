require 'acceptance_helper'

RSpec.describe Member::ProjectsController do
  resource 'Member - Project', 'Manage your projects'

  entity :project,
         id:                { type: :integer, description: 'Project identifier' },
         name:              { type: :string, description: 'Name' },
         description:       { type: :string, description: 'Description' },
         short_description: { type: :string, description: 'Short description' },
         main_url:          { type: :array, required: false, description: 'Home page URL' },
         sources_url:       { type: :array, required: false, description: 'Source URL' },
         docs_url:          { type: :array, required: false, description: 'Documentation URL' },
         sfw:               { type: :boolean, description: 'Is the project safe for work?' },
         published:         { type: :boolean, description: 'Is the project published by the author?' },
         locked:            { type: :boolean, description: 'Is the project locked by administrators?' },
         hidden_in_history: { type: :boolean, description: 'Are the changes shown in activity list?' },
         albums_count:      { type: :integer, description: 'Related albums amount' },
         articles_count:    { type: :integer, description: 'Related articles amount' },
         notes_count:       { type: :integer, description: 'Related notes amount' },
         uploads_count:     { type: :integer, description: 'Related uploads amount' },
         slug:              { type: :string, description: 'Slug for URLs' },
         user_id:           { type: :integer, description: 'Author identifier' },
         license_id:        { type: :integer, description: 'License identifier' },
         language_id:       { type: :integer, description: 'Language identifier' },
         published_at:      { type: :datetime, required: false, description: 'Publication date' },
         created_at:        { type: :datetime, description: 'Creation date' },
         updated_at:        { type: :datetime, description: 'Last update date' }

  entity :form_error,
         name:              { type: :array, required: false, description: 'Project name errors' },
         description:       { type: :array, required: false, description: 'Project description errors' },
         short_description: { type: :array, required: false, description: 'Sort description errors' },
         main_url:          { type: :array, required: false, description: 'Home page URL errors' },
         sources_url:       { type: :array, required: false, description: 'Source URL errors' },
         docs_url:          { type: :array, required: false, description: 'Documentation URL errors' },
         language:          { type: :array, required: false, description: 'Related language identifier errors' },
         license:           { type: :array, required: false, description: 'Related license identifier errors' },
         slug:              { type: :array, required: false, description: 'This errors comes with name errors' }

  entity :error,
         error: { type: :string, description: 'Error message' }

  parameters :common_path_params,
             slug: { type: :string, description: 'Project slug' }

  # The base form attributes, they are different in POST and PUT
  base_attributes = {
    name:              { type: :string, description: 'Project name' },
    short_description: { type: :string, description: 'Short description' },
    description:       { type: :string, description: 'Project description' },
    main_url:          { type: :string, required: false, description: 'Project home pate' },
    sources_url:       { type: :string, required: false, description: 'Project sources URL' },
    docs_url:          { type: :string, required: false, description: 'Project documentation URL' },
    sfw:               { type: :boolean, description: 'Is the project safe for work?' },
    published:         { type: :boolean, description: 'Is the project published?' },
    hidden_in_history: { type: :boolean, description: 'Are the changes shown in activity list?' },
    language_id:       { type: :integer, description: 'Related language identifier' },
    license_id:        { type: :integer, description: 'Related license identifier' },
    tags_list:         { type: :string, description: 'Tags list, comma-separated' },
  }

  parameters :new_form_attributes,
             base_attributes

  parameters :update_form_attributes, base_attributes.merge(
    minor_update:       { type: :boolean, description: 'Minor updates are not recorded in activities' },
    update_description: { type: :string, description: 'If not a minor update, a small description of the changes' }
  )

  let(:user) { FactoryBot.create :user_active }
  let(:projects) { FactoryBot.create_list :project, 2, user: user }
  let(:slug) { user.projects.first.slug }

  before do
    # Create content
    projects

    sign_in user
  end

  on_get '/member/projects', 'List owned projects' do
    for_code 200 do |url|
      visit url

      expect(response).to have_many defined :project
    end
  end

  on_post '/member/projects', 'Create a project' do
    request_params defined: :new_form_attributes

    for_code 201 do |url|
      payload = { project: {
        name:              'My first project',
        description:       'Elabs engine',
        short_description: 'A project-related CMS engine',
        language_id:       Language.first.id,
        license_id:        License.first.id,
      } }
      visit url, payload: payload

      expect(response).to have_one defined :project
    end

    for_code 422 do |url|
      visit url, payload: { project: { name: '' } }

      expect(response).to have_one defined :form_error
    end
  end

  on_put '/member/projects/:slug', 'Updates a project' do
    path_params defined: :common_path_params
    request_params defined: :update_form_attributes

    for_code 200 do |url|
      visit url, payload: { project: { name: 'Another content management engine' } }

      expect(response).to have_one defined :project
    end

    for_code 422 do |url|
      visit url, payload: { project: { name: '' } }

      expect(response).to have_one defined :form_error
    end

    for_code 404 do |url|
      visit url, path_params: { slug: '00' }

      expect(response).to have_one defined :error
    end
  end

  on_put '/member/projects/:slug/toggle_publication', 'Invert publication state' do
    path_params defined: :common_path_params
    for_code 200 do |url|
      visit url

      expect(response).to have_one defined :project
    end

    for_code 404 do |url|
      visit url, path_params: { slug: '00' }

      expect(response).to have_one defined :error
    end
  end

  on_delete '/member/projects/:slug', 'Destroys a project' do
    path_params defined: :common_path_params

    for_code 204 do |url|
      visit url
    end

    for_code 404 do |url|
      visit url, path_params: { slug: '00' }

      expect(response).to have_one defined :error
    end
  end
end
