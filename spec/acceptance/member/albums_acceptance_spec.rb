require 'acceptance_helper'

RSpec.describe Member::AlbumsController do
  resource 'Member - Album', 'Manage your albums'

  entity :album,
         id:                { type: :integer, description: 'Album identifier' },
         name:              { type: :string, description: 'Name' },
         description:       { type: :string, description: 'Description' },
         sfw:               { type: :boolean, description: 'Is the album safe for work?' },
         published:         { type: :boolean, description: 'Is the album published by the author?' },
         locked:            { type: :boolean, description: 'Is the album locked by administrators?' },
         hidden_in_history: { type: :boolean, description: 'Are the changes shown in activity list?' },
         uploads_count:     { type: :integer, description: 'Related uploads amount' },
         slug:              { type: :string, description: 'Slug for URLs' },
         user_id:           { type: :integer, description: 'Author identifier' },
         license_id:        { type: :integer, description: 'License identifier' },
         language_id:       { type: :integer, description: 'Language identifier' },
         published_at:      { type: :datetime, required: false, description: 'Publication date' },
         created_at:        { type: :datetime, description: 'Creation date' },
         updated_at:        { type: :datetime, description: 'Last update date' }

  entity :form_error,
         name:        { type: :array, required: false, description: 'Album name errors' },
         description: { type: :array, required: false, description: 'Album description errors' },
         language:    { type: :array, required: false, description: 'Related language identifier errors' },
         license:     { type: :array, required: false, description: 'Related license identifier errors' },
         slug:        { type: :array, required: false, description: 'This errors comes with name errors' }

  entity :error,
         error: { type: :string, description: 'Error message' }

  parameters :common_path_params,
             slug: { type: :string, description: 'Album slug' }

  # The base form attributes, they are different in POST and PUT
  base_attributes = {
    name:              { type: :string, description: 'Album name' },
    description:       { type: :string, description: 'Album description' },
    sfw:               { type: :boolean, description: 'Is the album safe for work?' },
    published:         { type: :boolean, description: 'Is the album published?' },
    hidden_in_history: { type: :boolean, description: 'Are the changes shown in activity list?' },
    language_id:       { type: :integer, description: 'Related language identifier' },
    license_id:        { type: :integer, description: 'Related license identifier' },
    tags_list:         { type: :string, description: 'Tags list, comma-separated' },
  }

  parameters :new_form_attributes,
             base_attributes

  parameters :update_form_attributes, base_attributes.merge(
    minor_update:       { type: :boolean, description: 'Minor updates are not recorded in activities' },
    update_description: { type: :string, description: 'If not a minor update, a small description of the changes' }
  )

  let(:user) { FactoryBot.create :user_active }
  let(:albums) { FactoryBot.create_list :album, 2, user: user }
  let(:slug) { user.albums.first.slug }

  before do
    # Create content
    albums

    sign_in user
  end

  on_get '/member/albums', 'List owned albums' do
    for_code 200 do |url|
      visit url

      expect(response).to have_many defined :album
    end
  end

  on_post '/member/albums', 'Create an album' do
    request_params defined: :new_form_attributes

    for_code 201 do |url|
      payload = { album: {
        name:        'My first album',
        description: 'Some fine drawings',
        language_id: Language.first.id,
        license_id:  License.first.id,
      } }
      visit url, payload: payload

      expect(response).to have_one defined :album
    end

    for_code 422 do |url|
      visit url, payload: { album: { name: '' } }

      expect(response).to have_one defined :form_error
    end
  end

  on_put '/member/albums/:slug', 'Updates an album' do
    path_params defined: :common_path_params
    request_params defined: :update_form_attributes

    for_code 200 do |url|
      visit url, payload: { album: { name: 'Cats' } }

      expect(response).to have_one defined :album
    end

    for_code 422 do |url|
      visit url, payload: { album: { name: '' } }

      expect(response).to have_one defined :form_error
    end

    for_code 404 do |url|
      visit url, path_params: { slug: '00' }

      expect(response).to have_one defined :error
    end
  end

  on_put '/member/albums/:slug/toggle_publication', 'Invert publication state' do
    path_params defined: :common_path_params
    for_code 200 do |url|
      visit url

      expect(response).to have_one defined :album
    end

    for_code 404 do |url|
      visit url, path_params: { slug: '00' }

      expect(response).to have_one defined :error
    end
  end

  on_delete '/member/albums/:slug', 'Destroys an album' do
    path_params defined: :common_path_params

    for_code 204 do |url|
      visit url
    end

    for_code 404 do |url|
      visit url, path_params: { slug: '00' }

      expect(response).to have_one defined :error
    end
  end
end
