require 'acceptance_helper'

RSpec.describe Member::PreferencesController do
  resource 'Member - Preference', 'Preferences'

  entity :preferences,
         show_nsfw:                     { type: :boolean, description: 'Show NSFW content by default' },
         locale:                        { type: :string, required: false, description: 'Display locale' },
         writing_language_id:           { type: :integer, required: false, description: 'Default language for new content' },
         writing_license_id:            { type: :integer, required: false, description: 'Default license for new content' },
         receive_comment_notifications: { type: :boolean, description: 'Receive emails when someone comments my content' },
         receive_content_notifications: { type: :boolean, description: 'Receive emails when admins change my content' },
         receive_admin_notifications:   { type: :boolean, description: 'Receive emails about administrative notifications' }

  parameters :preferences_form_attributes,
             show_nsfw:                     { type: :boolean, description: 'Show NSFW content by default' },
             locale:                        { type: :string, required: false, description: 'Display locale' },
             writing_language_id:           { type: :integer, required: false, description: 'Default language for new content' },
             writing_license_id:            { type: :integer, required: false, description: 'Default license for new content' },
             receive_comment_notifications: { type: :boolean, description: 'Receive emails when someone comments my content' },
             receive_content_notifications: { type: :boolean, description: 'Receive emails when admins change my content' },
             receive_admin_notifications:   { type: :boolean, description: 'Receive emails about administrative notifications' }

  let(:user) { FactoryBot.create :user_active }

  before do
    sign_in user
  end

  on_get '/member/preferences', 'Display preferences' do
    for_code 200 do |url|
      visit url

      expect(response).to have_one defined :preferences
    end
  end

  on_put '/member/preferences', 'Update preferences' do
    request_params defined: :preferences_form_attributes

    for_code 200 do |url|
      visit url, payload: { show_nsfw: true }

      expect(response).to have_one defined :preferences
    end
  end
end
