require 'acceptance_helper'

RSpec.describe Member::MarkdownPreviewerController do
  resource 'Rendered HTML', 'Utility endpoint to render HTML as the application would do.'

  entity :html_text,
         html_content: { type: :string, description: 'Rendered text' }

  parameters :payload,
             text: { type: :string, description: 'Markdown text' }

  on_post '/member/markdown_previewer', 'Render HTML from markdown' do
    request_params defined: :payload

    for_code 200 do |url|
      visit url, payload: { text: '# Hello world' }

      expect(response).to have_one defined :html_text
    end
  end
end
