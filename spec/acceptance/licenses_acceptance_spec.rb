require 'acceptance_helper'

RSpec.describe LicensesController do
  resource 'Public - License', 'Available licenses'

  entity :license,
         id:             { type: :integer, description: 'License identifier' },
         name:           { type: :string, description: 'Name' },
         url:            { type: :string, description: 'Description' },
         tldr_url:       { type: :string, required: false, description: 'Description' },
         icon:           { type: :string, description: 'Description' },
         albums_count:   { type: :integer, description: 'Amount of articles with this license' },
         articles_count: { type: :integer, description: 'Amount of articles with this license' },
         notes_count:    { type: :integer, description: 'Amount of articles with this license' },
         projects_count: { type: :integer, description: 'Amount of articles with this license' },
         uploads_count:  { type: :integer, description: 'Amount of articles with this license' },
         slug:           { type: :string, description: 'Slug for URLs' }

  entity :error,
         error: { type: :string, description: 'Error message' }

  parameters :common_path_params,
             slug: { type: :string, description: 'License slug' }

  parameters :comment_form_attributes,
             name:          { type: :string, required: false, description: "Commenter name. No need to specify it if user is logged in. It's required for visitors" },
             email:         { type: :string, required: false, description: "Commenter email. No need to specify it if user is logged in. It's NOT required for anyone" },
             comment:       { type: :string, description: 'The comment' },
             allow_contact: { type: :boolean, required: false, description: 'Does commenter accepts to be contacted by license author? It will be true for logged in users' }

  let(:license) { FactoryBot.create :license }

  let(:slug) { license.slug }

  before do
    FactoryBot.create :project_published, license: license
  end

  on_get '/licenses', 'List licenses' do
    for_code 200 do |url|
      visit url
      expect(response).to have_many defined :license
    end
  end

  on_get '/licenses/:slug', 'Display one license' do
    path_params defined: :common_path_params

    for_code 200 do |url|
      visit url

      expect(response).to have_one defined :license
    end

    for_code 404 do |url|
      visit url, path_params: { slug: '0' }

      expect(response).to have_one defined :error
    end
  end
end
