require 'acceptance_helper'

RSpec.describe Admin::NotesController do
  resource 'Admin - Notes', 'Manage notes as admin'

  entity :note,
         id:                { type: :integer, description: 'Note identifier' },
         content:           { type: :string, description: 'Note content' },
         sfw:               { type: :boolean, description: 'Is the note safe for work?' },
         published:         { type: :boolean, description: 'Is the note published by the author?' },
         locked:            { type: :boolean, description: 'Is the note locked by administrators?' },
         hidden_in_history: { type: :boolean, description: 'Are the changes shown in activity list?' },
         slug:              { type: :string, description: 'Slug for URLs' },
         user_id:           { type: :integer, description: 'Author identifier' },
         license_id:        { type: :integer, description: 'License identifier' },
         language_id:       { type: :integer, description: 'Language identifier' },
         published_at:      { type: :datetime, required: false, description: 'Publication date' },
         created_at:        { type: :datetime, description: 'Creation date' },
         updated_at:        { type: :datetime, description: 'Last update date' }

  parameters :common_path_params,
             slug: { type: :string, description: 'Note slug' }

  let(:admin) { FactoryBot.create :user_admin_active }
  let(:notes) { FactoryBot.create_list :note_published, 2 }
  let(:slug) { Note.first.slug }

  before do
    # Create notes
    notes

    sign_in admin
  end

  on_get '/admin/notes', 'List notes' do
    for_code 200 do |url|
      visit url

      expect(response).to have_many defined :note
    end
  end

  on_put '/admin/notes/:slug/toggle_lock', 'Locks or unlocks a note' do
    path_params defined: :common_path_params

    for_code 200 do |url|
      visit url, path_params: { slug: slug }

      expect(response).to have_one defined :note
    end
  end

  on_delete '/admin/notes/:slug', 'Destroy note' do
    path_params defined: :common_path_params

    for_code 204 do |url|
      visit url, path_params: { slug: slug }
    end
  end
end
