require 'acceptance_helper'

RSpec.describe Admin::UploadsController do
  resource 'Admin - Uploads', 'Manage uploads as admin'

  entity :file,
         content_type: { type: :string, description: 'Mime type' },
         filename:     { type: :string, description: 'File name' },
         metadata:     { type: :object, description: 'File metadata' },
         url:          { type: :string, description: 'URL to access this file' }

  entity :upload,
         id:                { type: :integer, description: 'Upload identifier' },
         title:             { type: :string, description: 'Title' },
         description:       { type: :string, description: 'Description' },
         file:              { type: :object, description: 'File information', attributes: :file },
         sfw:               { type: :boolean, description: 'Is the upload safe for work?' },
         published:         { type: :boolean, description: 'Is the upload published by the author?' },
         locked:            { type: :boolean, description: 'Is the upload locked by administrators?' },
         hidden_in_history: { type: :boolean, description: 'Are the changes shown in activity list?' },
         slug:              { type: :string, description: 'Slug for URLs' },
         user_id:           { type: :integer, description: 'Author identifier' },
         license_id:        { type: :integer, description: 'License identifier' },
         language_id:       { type: :integer, description: 'Language identifier' },
         published_at:      { type: :datetime, required: false, description: 'Publication date' },
         created_at:        { type: :datetime, description: 'Creation date' },
         updated_at:        { type: :datetime, description: 'Last update date' }

  parameters :common_path_params,
             slug: { type: :string, description: 'Upload slug' }

  let(:admin) { FactoryBot.create :user_admin_active }
  let(:uploads) { FactoryBot.create_list :upload_published, 2 }
  let(:slug) { Upload.first.slug }

  before do
    # Create uploads
    uploads

    sign_in admin
  end

  on_get '/admin/uploads', 'List uploads' do
    for_code 200 do |url|
      visit url

      expect(response).to have_many defined :upload
    end
  end

  on_put '/admin/uploads/:slug/toggle_lock', 'Locks or unlocks an upload' do
    path_params defined: :common_path_params

    for_code 200 do |url|
      visit url, path_params: { slug: slug }

      expect(response).to have_one defined :upload
    end
  end

  on_delete '/admin/uploads/:slug', 'Destroy upload' do
    path_params defined: :common_path_params

    for_code 204 do |url|
      visit url, path_params: { slug: slug }
    end
  end
end
