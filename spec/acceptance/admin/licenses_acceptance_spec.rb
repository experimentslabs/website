require 'acceptance_helper'

RSpec.describe Admin::LicensesController do
  resource 'Admin - License', 'Manage licenses'

  entity :license,
         id:             { type: :integer, description: 'License identifier' },
         name:           { type: :string, description: 'Name' },
         url:            { type: :string, description: 'Official URL' },
         tldr_url:       { type: :string, required: false, description: 'URL on tldrlegal.com' },
         icon:           { type: :string, description: 'License icon from licenses icon pack' },
         albums_count:   { type: :integer, description: 'Amount albums with this license' },
         articles_count: { type: :integer, description: 'Amount articles with this license' },
         notes_count:    { type: :integer, description: 'Amount notes with this license' },
         projects_count: { type: :integer, description: 'Amount projects with this license' },
         uploads_count:  { type: :integer, description: 'Amount uploads with this license' },
         slug:           { type: :string, description: 'Slug for URLs' }

  entity :form_error,
         name:     { type: :array, required: false, description: 'Name errors' },
         url:      { type: :array, required: false, description: 'Official URL errors' },
         tldr_url: { type: :array, required: false, description: 'TL;DR URL errors errors' },
         icon:     { type: :array, required: false, description: 'Icon errors' }

  entity :error,
         error: { type: :string, description: 'Error message' }

  parameters :common_path_params,
             slug: { type: :string, description: 'License slug' }

  parameters :form_attributes,
             name:     { type: :string, description: 'Name' },
             url:      { type: :string, description: 'URL to official documentation' },
             tldr_url: { type: :string, required: false, description: 'URL to tldrlegal.com' },
             icon:     { type: :string, description: 'License icon' }

  let(:admin) { FactoryBot.create :user_admin_active }
  let(:licenses) { FactoryBot.create_list :license, 2 }
  let(:slug) { License.first.slug }

  before do
    # Create content
    licenses

    sign_in admin
  end

  on_get '/admin/licenses', 'List licenses' do
    for_code 200 do |url|
      visit url

      expect(response).to have_many defined :license
    end
  end

  on_post '/admin/licenses', 'Create a license' do
    request_params defined: :form_attributes

    for_code 201 do |url|
      visit url, payload: { license: { name: 'MIT', url: 'https://opensource.org/licenses/MIT', icon: 'mit' } }

      expect(response).to have_one defined :license
    end

    for_code 422 do |url|
      visit url, payload: { license: { name: 'Nothing more' } }

      expect(response).to have_one defined :form_error
    end
  end

  on_put '/admin/licenses/:slug', 'Update license' do
    path_params defined: :common_path_params

    request_params defined: :form_attributes

    for_code 200 do |url|
      visit url, payload: { license: { name: 'WTFPL' } }

      expect(response).to have_one defined :license
    end

    for_code 422 do |url|
      visit url, payload: { license: { url: '' } }

      expect(response).to have_one defined :form_error
    end

    for_code 404 do |url|
      visit url, path_params: { slug: 'nope' }

      expect(response).to have_one defined :error
    end
  end

  on_delete '/admin/licenses/:slug', 'Destroy license' do
    path_params defined: :common_path_params

    for_code 204 do |url|
      visit url
    end

    for_code 404 do |url|
      visit url, path_params: { slug: 'nope' }

      expect(response).to have_one defined :error
    end
  end
end
