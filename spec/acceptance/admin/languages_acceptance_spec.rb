require 'acceptance_helper'

RSpec.describe Admin::LanguagesController do
  resource 'Admin - Language', 'Manage content languages'

  entity :language,
         id:             { type: :integer, description: 'Language identifier' },
         name:           { type: :string, description: 'Name' },
         iso639_1:       { type: :string, description: 'Country code' },
         albums_count:   { type: :integer, description: 'Amount of albums with this language' },
         articles_count: { type: :integer, description: 'Amount of articles with this language' },
         links_count:    { type: :integer, description: 'Amount of links with this language' },
         notes_count:    { type: :integer, description: 'Amount of notes with this language' },
         projects_count: { type: :integer, description: 'Amount of projects with this language' },
         uploads_count:  { type: :integer, description: 'Amount of uploads with this language' }

  entity :form_error,
         name:     { type: :array, required: false, description: 'Name errors' },
         iso639_1: { type: :array, required: false, description: 'Country code errors' }

  entity :error,
         error: { type: :string, description: 'Error message' }

  parameters :form_attributes,
             name:     { type: :string, required: false, description: 'Language name' },
             iso639_1: { type: :string, required: false, description: 'Country code' }

  parameters :common_path_params,
             iso639_1: { type: :string, description: 'Country code' }

  let(:admin) { FactoryBot.create :user_admin_active }
  let(:languages) { FactoryBot.create_list :language, 2 }
  let(:iso639_1) { Language.first.iso639_1 }

  before do
    # Create content
    languages

    sign_in admin
  end

  on_get '/admin/languages', 'List languages' do
    for_code 200 do |url|
      visit url

      expect(response).to have_many defined :language
    end
  end

  on_post '/admin/languages', 'Create a language' do
    request_params defined: :form_attributes

    for_code 201 do |url|
      visit url, payload: { language: { name: 'English', iso639_1: 'en' } }

      expect(response).to have_one defined :language
    end

    for_code 422 do |url|
      visit url, payload: { language: { name: '' } }

      expect(response).to have_one defined :form_error
    end
  end

  on_put '/admin/languages/:iso639_1', 'Update language' do
    request_params defined: :form_attributes

    path_params defined: :common_path_params

    for_code 200 do |url|
      visit url, payload: { language: { name: 'Français' } }

      expect(response).to have_one defined :language
    end

    for_code 422 do |url|
      visit url, payload: { language: { name: '' } }

      expect(response).to have_one defined :form_error
    end

    for_code 404 do |url|
      visit url, path_params: { iso639_1: '00' }

      expect(response).to have_one defined :error
    end
  end

  on_delete '/admin/languages/:iso639_1', 'Destroy language' do
    path_params defined: :common_path_params

    for_code 204 do |url|
      visit url
    end

    for_code 404 do |url|
      visit url, path_params: { iso639_1: '00' }

      expect(response).to have_one defined :error
    end
  end
end
