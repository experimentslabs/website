require 'acceptance_helper'

RSpec.describe Admin::ArticlesController do
  resource 'Admin - Articles', 'Manage articles as admin'

  entity :article,
         id:                { type: :integer, description: 'Article identifier' },
         title:             { type: :string, description: 'Title' },
         content:           { type: :string, description: 'Main article' },
         excerpt:           { type: :string, description: 'Excerpt' },
         sfw:               { type: :boolean, description: 'Is the article safe for work?' },
         published:         { type: :boolean, description: 'Is the article published by the author?' },
         locked:            { type: :boolean, description: 'Is the article locked by administrators?' },
         hidden_in_history: { type: :boolean, description: 'Are the changes shown in activity list?' },
         slug:              { type: :string, description: 'Slug for URLs' },
         user_id:           { type: :integer, description: 'Author identifier' },
         license_id:        { type: :integer, description: 'License identifier' },
         language_id:       { type: :integer, description: 'Language identifier' },
         published_at:      { type: :datetime, required: false, description: 'Publication date' },
         created_at:        { type: :datetime, description: 'Creation date' },
         updated_at:        { type: :datetime, description: 'Last update date' }

  parameters :common_path_params,
             slug: { type: :string, description: 'Article slug' }

  let(:admin) { FactoryBot.create :user_admin_active }
  let(:articles) { FactoryBot.create_list :article_published, 2 }
  let(:slug) { Article.first.slug }

  before do
    # Create articles
    articles

    sign_in admin
  end

  on_get '/admin/articles', 'List articles' do
    for_code 200 do |url|
      visit url

      expect(response).to have_many defined :article
    end
  end

  on_put '/admin/articles/:slug/toggle_lock', 'Locks or unlocks an article' do
    path_params defined: :common_path_params

    for_code 200 do |url|
      visit url, path_params: { slug: slug }

      expect(response).to have_one defined :article
    end
  end

  on_delete '/admin/articles/:slug', 'Destroy article' do
    path_params defined: :common_path_params

    for_code 204 do |url|
      visit url, path_params: { slug: slug }
    end
  end
end
