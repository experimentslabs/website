require 'acceptance_helper'

RSpec.describe Admin::ProjectsController do
  resource 'Admin - Projects', 'Manage projects as admin'

  entity :project,
         id:                { type: :integer, description: 'Project identifier' },
         name:              { type: :string, description: 'Name' },
         description:       { type: :string, description: 'Full description' },
         short_description: { type: :string, description: 'Short description' },
         main_url:          { type: :string, required: false, description: 'Project homepage URL' },
         sources_url:       { type: :string, required: false, description: 'Project sources URL' },
         docs_url:          { type: :string, required: false, description: 'Project documentation URL' },
         sfw:               { type: :boolean, description: 'Is the project safe for work?' },
         published:         { type: :boolean, description: 'Is the project published by the author?' },
         locked:            { type: :boolean, description: 'Is the project locked by administrators?' },
         hidden_in_history: { type: :boolean, description: 'Are the changes shown in activity list?' },
         albums_count:      { type: :integer, description: 'Amount of albums linked to the project' },
         articles_count:    { type: :integer, description: 'Amount of articles linked to the project' },
         notes_count:       { type: :integer, description: 'Amount of notes linked to the project' },
         uploads_count:     { type: :integer, description: 'Amount of uploads linked to the project' },
         slug:              { type: :string, description: 'Slug for URLs' },
         user_id:           { type: :integer, description: 'Author identifier' },
         license_id:        { type: :integer, description: 'License identifier' },
         language_id:       { type: :integer, description: 'Language identifier' },
         published_at:      { type: :datetime, required: false, description: 'Publication date' },
         created_at:        { type: :datetime, description: 'Creation date' },
         updated_at:        { type: :datetime, description: 'Last update date' }

  parameters :common_path_params,
             slug: { type: :string, description: 'Project slug' }

  let(:admin) { FactoryBot.create :user_admin_active }
  let(:projects) { FactoryBot.create_list :project_published, 2 }
  let(:slug) { Project.first.slug }

  before do
    # Create projects
    projects

    sign_in admin
  end

  on_get '/admin/projects', 'List projects' do
    for_code 200 do |url|
      visit url

      expect(response).to have_many defined :project
    end
  end

  on_put '/admin/projects/:slug/toggle_lock', 'Locks or unlocks a project' do
    path_params defined: :common_path_params

    for_code 200 do |url|
      visit url, path_params: { slug: slug }

      expect(response).to have_one defined :project
    end
  end

  on_delete '/admin/projects/:slug', 'Destroy project' do
    path_params defined: :common_path_params

    for_code 204 do |url|
      visit url, path_params: { slug: slug }
    end
  end
end
