require 'acceptance_helper'

RSpec.describe Admin::UsersController do
  resource 'Admin - User', 'Manage users'

  entity :user,
         id:             { type: :integer, description: 'User identifier' },
         username:       { type: :string, description: 'Pseudo' },
         real_name:      { type: :string, required: false, description: 'Real name' },
         biography:      { type: :string, required: false, description: 'Biography' },
         role:           { type: :string, description: 'User role: "user" or "admin"' },
         confirmed_at:   { type: :datetime, required: false, description: 'Account validation date' },
         albums_count:   { type: :integer, description: 'Amount of albums by this user' },
         articles_count: { type: :integer, description: 'Amount of articles by this user' },
         links_count:    { type: :integer, description: 'Amount of links by this user' },
         notes_count:    { type: :integer, description: 'Amount of notes by this user' },
         projects_count: { type: :integer, description: 'Amount of projects by this user' },
         uploads_count:  { type: :integer, description: 'Amount of uploads by this user' },
         created_at:     { type: :datetime, description: 'Creation date' },
         updated_at:     { type: :datetime, description: 'Last modification date' }

  entity :error,
         error: { type: :string, description: 'Error message' }

  parameters :common_path_params,
             username: { type: :string, description: "User's username" }

  let(:admin) { FactoryBot.create :user_admin_active }
  let(:users) { FactoryBot.create_list :user, 2 }
  let(:username) { User.first.username }

  before do
    # Create content
    users

    sign_in admin
  end

  on_get '/admin/users', 'List users' do
    for_code 200 do |url|
      visit url

      expect(response).to have_many defined :user
    end
  end

  on_get '/admin/users/:username', 'One user' do
    path_params defined: :common_path_params
    for_code 200 do |url|
      visit url

      expect(response).to have_one defined :user
    end
  end

  on_delete '/admin/users/:username', 'Destroy user and its content' do
    path_params defined: :common_path_params

    for_code 204 do |url|
      visit url
    end

    for_code 404 do |url|
      visit url, path_params: { username: 'nope' }

      expect(response).to have_one defined :error
    end
  end
end
