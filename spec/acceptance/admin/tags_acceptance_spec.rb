require 'acceptance_helper'

RSpec.describe Admin::TagsController do
  resource 'Admin - Tag', 'Manage tags'

  entity :tag,
         id:             { type: :integer, description: 'Tag identifier' },
         name:           { type: :string, description: 'Tag' },
         slug:           { type: :string, description: 'Tag slug for URLs' },
         albums_count:   { type: :integer, description: 'Amount of albums with this tag' },
         articles_count: { type: :integer, description: 'Amount of articles with this tag' },
         notes_count:    { type: :integer, description: 'Amount of notes with this tag' },
         projects_count: { type: :integer, description: 'Amount of projects with this tag' },
         uploads_count:  { type: :integer, description: 'Amount of uploads with this tag' },
         links_count:    { type: :integer, description: 'Amount of links with this tag' }

  entity :form_error,
         name: { type: :array, required: false, description: 'Name errors' }

  entity :error,
         error: { type: :string, description: 'Error message' }

  parameters :common_path_params,
             slug: { type: :string, description: 'Tag slug' }

  parameters :form_attributes,
             name: { type: :string, description: 'Tag' }

  let(:admin) { FactoryBot.create :user_admin_active }
  let(:tags) { FactoryBot.create_list :tag, 2 }
  let(:slug) { Tag.first.slug }

  before do
    # Create content
    tags

    sign_in admin
  end

  on_get '/admin/tags', 'List tags' do
    for_code 200 do |url|
      visit url

      expect(response).to have_many defined :tag
    end
  end

  on_put '/admin/tags/:slug', 'Update tag' do
    path_params defined: :common_path_params

    request_params defined: :form_attributes

    for_code 200 do |url|
      visit url, payload: { tag: { name: 'greatness' } }

      expect(response).to have_one defined :tag
    end

    for_code 422 do |url|
      visit url, payload: { tag: { name: '' } }

      expect(response).to have_one defined :form_error
    end

    for_code 404 do |url|
      visit url, path_params: { slug: 'nope' }

      expect(response).to have_one defined :error
    end
  end

  on_delete '/admin/tags/:slug', 'Destroy tag' do
    path_params defined: :common_path_params

    for_code 204 do |url|
      visit url
    end

    for_code 404 do |url|
      visit url, path_params: { slug: 'nope' }

      expect(response).to have_one defined :error
    end
  end
end
