require 'acceptance_helper'

RSpec.describe Admin::ReportsController do
  resource 'Admin - Report', 'Manage reports'

  entity :report,
         id:            { type: :integer, description: 'Report identifier' },
         name:          { type: :string, required: false, description: 'Visitor name' },
         email:         { type: :string, required: false, description: 'Visitor email' },
         url:           { type: :string, description: 'Page on which the report was made' },
         reason:        { type: :string, description: 'Message' },
         allow_contact: { type: :string, required: false, description: 'Allow admins to recontact visitor' },
         user_id:       { type: :integer, required: false, description: 'User identifier, if report was submitted by an user' },
         created_at:    { type: :datetime, description: 'Creation date' },
         updated_at:    { type: :datetime, description: 'Last modification date' }

  entity :error,
         error: { type: :string, description: 'Error message' }

  parameters :common_path_params,
             id: { type: :string, description: 'Report identifier' }

  let(:admin) { FactoryBot.create :user_admin_active }
  let(:reports) { FactoryBot.create_list :report, 2 }
  let(:id) { Report.first.id }

  before do
    # Create content
    reports

    sign_in admin
  end

  on_get '/admin/reports', 'List reports' do
    for_code 200 do |url|
      visit url

      expect(response).to have_many defined :report
    end
  end

  on_delete '/admin/reports/:id', 'Destroy report' do
    path_params defined: :common_path_params

    for_code 204 do |url|
      visit url
    end

    for_code 404 do |url|
      visit url, path_params: { id: 0 }

      expect(response).to have_one defined :error
    end
  end
end
