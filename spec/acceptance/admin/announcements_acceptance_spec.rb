require 'acceptance_helper'

RSpec.describe Admin::AnnouncementsController do
  resource 'Admin - Announcement', 'Manage announces'

  entity :announcement,
         id:         { type: :integer, description: 'Announce identifier' },
         start_at:   { type: :datetime, required: false, description: 'Date at which the announcement is shown' },
         end_at:     { type: :datetime, required: false, description: 'Date at which the announcement is hidden' },
         content:    { type: :string, description: 'Message' },
         level:      { type: :string, description: 'Severity level: "info", "warning" or "danger"' },
         target:     { type: :string, description: 'Users target: "global", "public", "member" or "admin"' },
         user_id:    { type: :integer, description: 'Author identifier' },
         created_at: { type: :datetime, description: 'Creation date' },
         updated_at: { type: :datetime, description: 'Last modification date' }

  entity :form_error,
         start_at: { type: :array, required: false, description: 'Start date errors' },
         end_at:   { type: :array, required: false, description: 'End date errors' },
         content:  { type: :array, required: false, description: 'Content errors' },
         level:    { type: :array, required: false, description: 'Level errors' },
         target:   { type: :array, required: false, description: 'Target errors' },
         user_id:  { type: :array, required: false, description: 'User errors' }

  entity :error,
         error: { type: :string, description: 'Error message' }

  parameters :common_path_params,
             id: { type: :string, description: 'Announcement identifier' }

  parameters :form_request_params,
             start_at: { type: :string, required: false, description: 'Date and time at which the announcement is shown' },
             end_at:   { type: :string, required: false, description: 'Date and time at which the announcement is hidden' },
             content:  { type: :string, description: 'Message' },
             level:    { type: :string, description: 'Severity level: "info", "warning" or "danger"' },
             target:   { type: :string, description: 'Users target: "global", "public", "member" or "admin"' }

  let(:admin) { FactoryBot.create :user_admin_active }
  let(:announcements) { FactoryBot.create_list :announcement, 2 }
  let(:id) { Announcement.first.id }

  before do
    # Create content
    announcements

    sign_in admin
  end

  on_get '/admin/announcements', 'List announcements' do
    for_code 200 do |url|
      visit url

      expect(response).to have_many defined :announcement
    end
  end

  on_post '/admin/announcements', 'Create an announcement' do
    request_params defined: :form_request_params

    for_code 201 do |url|
      visit url, payload: { announcement: { content: 'Hello you!', level: 'info', target: 'global' } }

      expect(response).to have_one defined :announcement
    end

    for_code 422 do |url|
      visit url, payload: { announcement: { content: '' } }

      expect(response).to have_one defined :form_error
    end
  end

  on_put '/admin/announcements/:id', 'Update announcement' do
    path_params defined: :common_path_params
    request_params defined: :form_request_params

    for_code 200 do |url|
      visit url, payload: { announcement: { content: 'New content' } }

      expect(response).to have_one defined :announcement
    end

    for_code 422 do |url|
      visit url, payload: { announcement: { content: '' } }

      expect(response).to have_one defined :form_error
    end

    for_code 404 do |url|
      visit url, path_params: { id: 0 }

      expect(response).to have_one defined :error
    end
  end

  on_delete '/admin/announcements/:id', 'Destroy announcement' do
    path_params defined: :common_path_params

    for_code 204 do |url|
      visit url
    end

    for_code 404 do |url|
      visit url, path_params: { id: 0 }

      expect(response).to have_one defined :error
    end
  end
end
