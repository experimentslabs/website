require 'acceptance_helper'

RSpec.describe Admin::AlbumsController do
  resource 'Admin - Albums', 'Manage albums as admin'

  entity :album,
         id:                { type: :integer, description: 'Album identifier' },
         name:              { type: :string, description: 'Name' },
         description:       { type: :string, description: 'Description' },
         sfw:               { type: :boolean, description: 'Is the album safe for work?' },
         published:         { type: :boolean, description: 'Is the album published by the author?' },
         locked:            { type: :boolean, description: 'Is the album locked by administrators?' },
         hidden_in_history: { type: :boolean, description: 'Are the changes shown in activity list?' },
         uploads_count:     { type: :integer, description: 'Related uploads amount' },
         slug:              { type: :string, description: 'Slug for URLs' },
         user_id:           { type: :integer, description: 'Author identifier' },
         license_id:        { type: :integer, description: 'License identifier' },
         language_id:       { type: :integer, description: 'Language identifier' },
         published_at:      { type: :datetime, required: false, description: 'Publication date' },
         created_at:        { type: :datetime, description: 'Creation date' },
         updated_at:        { type: :datetime, description: 'Last update date' }

  parameters :common_path_params,
             slug: { type: :string, description: 'Album slug' }

  let(:admin) { FactoryBot.create :user_admin_active }
  let(:albums) { FactoryBot.create_list :album_published, 2 }
  let(:slug) { Album.first.slug }

  before do
    # Create albums
    albums

    sign_in admin
  end

  on_get '/admin/albums', 'List albums' do
    for_code 200 do |url|
      visit url

      expect(response).to have_many defined :album
    end
  end

  on_put '/admin/albums/:slug/toggle_lock', 'Locks or unlocks an album' do
    path_params defined: :common_path_params

    for_code 200 do |url|
      visit url, path_params: { slug: slug }

      expect(response).to have_one defined :album
    end
  end

  on_delete '/admin/albums/:slug', 'Destroy album' do
    path_params defined: :common_path_params

    for_code 204 do |url|
      visit url, path_params: { slug: slug }
    end
  end
end
