require 'acceptance_helper'

RSpec.describe Admin::LinksController do
  resource 'Admin - Links', 'Manage links as admin'

  entity :link,
         id:                { type: :integer, description: 'Link identifier' },
         title:             { type: :string, description: 'Link title' },
         description:       { type: :string, required: false, description: 'Description' },
         sfw:               { type: :boolean, description: 'Is the link safe for work?' },
         published:         { type: :boolean, description: 'Is the link published by the author?' },
         locked:            { type: :boolean, description: 'Is the link locked by administrators?' },
         hidden_in_history: { type: :boolean, description: 'Are the changes shown in activity list?' },
         url:               { type: :string, description: 'The URL' },
         user_id:           { type: :integer, description: 'Author identifier' },
         language_id:       { type: :integer, description: 'Language identifier' },
         published_at:      { type: :datetime, description: 'Publication date' },
         created_at:        { type: :datetime, description: 'Creation date' },
         updated_at:        { type: :datetime, description: 'Last update date' }

  parameters :common_path_params,
             id: { type: :string, description: 'Link identifier' }

  let(:admin) { FactoryBot.create :user_admin_active }
  let(:links) { FactoryBot.create_list :link_published, 2 }
  let(:id) { Link.first.id }

  before do
    # Create links
    links

    sign_in admin
  end

  on_get '/admin/links', 'List links' do
    for_code 200 do |url|
      visit url

      expect(response).to have_many defined :link
    end
  end

  on_put '/admin/links/:id/toggle_lock', 'Locks or unlocks a link' do
    path_params defined: :common_path_params

    for_code 200 do |url|
      visit url, path_params: { id: id }

      expect(response).to have_one defined :link
    end
  end

  on_delete '/admin/links/:id', 'Destroy link' do
    path_params defined: :common_path_params

    for_code 204 do |url|
      visit url, path_params: { id: id }
    end
  end
end
