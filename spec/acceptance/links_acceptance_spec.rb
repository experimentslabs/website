require 'acceptance_helper'

RSpec.describe LinksController do
  resource 'Public - Link', 'Published links'

  entity :link,
         id:           { type: :integer, description: 'Link identifier' },
         title:        { type: :string, description: 'Title' },
         description:  { type: :string, required: false, description: 'Description' },
         url:          { type: :string, required: false, description: 'The URL' },
         sfw:          { type: :boolean, description: 'Is the link safe for work?' },
         user_id:      { type: :integer, description: 'Author identifier' },
         language_id:  { type: :integer, description: 'Language identifier' },
         tag_ids:      { type: :array, description: 'Tags identifiers' },
         published_at: { type: :datetime, required: false, description: 'Publication date' },
         created_at:   { type: :datetime, description: 'Creation date' },
         updated_at:   { type: :datetime, description: 'Last update date' }

  entity :paginated_response,
         content:  { type: :array, description: 'Links list', of: :link },
         page:     { type: :integer, description: 'Current page' },
         pages:    { type: :integer, description: 'Total amount of pages' },
         per_page: { type: :integer, description: 'Amount of links per page' },
         total:    { type: :integer, description: 'Total amount of acts' }

  entity :error,
         error: { type: :string, description: 'Error message' }

  let(:user) { FactoryBot.create :user_active }
  let(:language) { FactoryBot.create :language }
  let(:tag) { FactoryBot.create :tag }
  let(:link) { FactoryBot.create :link_published, tags: [tag], user: user, language: language }

  let(:slug) { link.slug }
  let(:username) { user.username }
  let(:iso639_1) { language.iso639_1 }
  let(:tag_slug) { tag.slug }

  before do
    link
  end

  on_get '/links', 'List links' do
    for_code 200 do |url|
      visit url
      expect(response).to have_one defined :paginated_response
    end
  end

  on_get '/users/:username/links', "List user's links" do
    path_params fields: { username: { type: :string, description: 'An user' } }

    for_code 200 do |url|
      visit url
      expect(response).to have_one defined :paginated_response
    end
  end

  on_get '/languages/:iso639_1/links', 'List links with a given language' do
    path_params fields: { iso639_1: { type: :string, description: 'A language' } }

    for_code 200 do |url|
      visit url
      expect(response).to have_one defined :paginated_response
    end
  end

  on_get '/tags/:tag_slug/links', 'List links with a given tag' do
    path_params fields: { tag_slug: { type: :string, description: 'A tag' } }

    for_code 200 do |url|
      visit url
      expect(response).to have_one defined :paginated_response
    end
  end
end
