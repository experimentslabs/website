require 'acceptance_helper'

RSpec.describe UsersController do
  resource 'Public - User', 'Published users'

  entity :user,
         id:             { type: :integer, description: 'User identifier' },
         username:       { type: :string, description: 'Username' },
         real_name:      { type: :string, required: false, description: 'Real name' },
         biography:      { type: :string, required: false, description: 'Biography' },
         albums_count:   { type: :integer, description: 'Amount of published albums' },
         articles_count: { type: :integer, description: 'Amount of published articles' },
         links_count:    { type: :integer, description: 'Amount of published links' },
         notes_count:    { type: :integer, description: 'Amount of published notes' },
         projects_count: { type: :integer, description: 'Amount of published projects' },
         uploads_count:  { type: :integer, description: 'Amount of published uploads' },
         created_at:     { type: :datetime, description: 'Creation date' }

  entity :paginated_response,
         content:  { type: :array, description: 'Users list', of: :user },
         page:     { type: :integer, description: 'Current page' },
         pages:    { type: :integer, description: 'Total amount of pages' },
         per_page: { type: :integer, description: 'Amount of users per page' },
         total:    { type: :integer, description: 'Total amount of users' }

  entity :error,
         error: { type: :string, description: 'Error message' }

  parameters :common_path_params,
             username: { type: :string, description: 'Username' }

  let(:user) { FactoryBot.create :user_active }

  let(:username) { user.username }

  before do
    user
  end

  on_get '/users', 'List users' do
    for_code 200 do |url|
      visit url
      expect(response).to have_one defined :paginated_response
    end
  end

  on_get '/users/:username', 'Display one user' do
    path_params defined: :common_path_params

    for_code 200 do |url|
      visit url

      expect(response).to have_one defined :user
    end

    for_code 404 do |url|
      visit url, path_params: { username: '0' }

      expect(response).to have_one defined :error
    end
  end
end
