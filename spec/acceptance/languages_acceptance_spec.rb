require 'acceptance_helper'

RSpec.describe LanguagesController do
  resource 'Public - Language', 'Available languages'

  entity :language,
         id:             { type: :integer, description: 'Language identifier' },
         name:           { type: :string, description: 'Name' },
         iso639_1:       { type: :string, description: 'Description' },
         albums_count:   { type: :integer, description: 'Amount of articles with this language' },
         articles_count: { type: :integer, description: 'Amount of articles with this language' },
         notes_count:    { type: :integer, description: 'Amount of articles with this language' },
         projects_count: { type: :integer, description: 'Amount of articles with this language' },
         uploads_count:  { type: :integer, description: 'Amount of articles with this language' },
         links_count:    { type: :integer, description: 'Amount of articles with this language' }

  entity :paginated_response,
         content:  { type: :array, description: 'Language list', of: :language },
         page:     { type: :integer, description: 'Current page' },
         pages:    { type: :integer, description: 'Total amount of pages' },
         per_page: { type: :integer, description: 'Amount of languages per page' },
         total:    { type: :integer, description: 'Total amount of languages' }

  entity :error,
         error: { type: :string, description: 'Error message' }

  parameters :common_path_params,
             iso639_1: { type: :string, description: 'Language code' }

  parameters :comment_form_attributes,
             name:          { type: :string, required: false, description: "Commenter name. No need to specify it if user is logged in. It's required for visitors" },
             email:         { type: :string, required: false, description: "Commenter email. No need to specify it if user is logged in. It's NOT required for anyone" },
             comment:       { type: :string, description: 'The comment' },
             allow_contact: { type: :boolean, required: false, description: 'Does commenter accepts to be contacted by language author? It will be true for logged in users' }

  let(:language) { FactoryBot.create :language }

  let(:iso639_1) { language.iso639_1 }

  before do
    FactoryBot.create :project_published, language: language
  end

  on_get '/languages', 'List languages' do
    for_code 200 do |url|
      visit url
      expect(response).to have_one defined :paginated_response
    end
  end

  on_get '/languages/:iso639_1', 'Display one language' do
    path_params defined: :common_path_params

    for_code 200 do |url|
      visit url

      expect(response).to have_one defined :language
    end

    for_code 404 do |url|
      visit url, path_params: { iso639_1: '0' }

      expect(response).to have_one defined :error
    end
  end
end
