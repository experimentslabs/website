require 'acceptance_helper'

RSpec.describe TagsController do
  resource 'Public - Tag', 'Available tags'

  entity :tag,
         id:             { type: :integer, description: 'Tag identifier' },
         name:           { type: :string, description: 'Name' },
         albums_count:   { type: :integer, description: 'Amount of articles with this tag' },
         articles_count: { type: :integer, description: 'Amount of articles with this tag' },
         links_count:    { type: :integer, description: 'Amount of links with this tag' },
         notes_count:    { type: :integer, description: 'Amount of articles with this tag' },
         projects_count: { type: :integer, description: 'Amount of articles with this tag' },
         uploads_count:  { type: :integer, description: 'Amount of articles with this tag' },
         slug:           { type: :string, description: 'Slug for URLs' }

  entity :error,
         error: { type: :string, description: 'Error message' }

  parameters :common_path_params,
             slug: { type: :string, description: 'Tag slug' }

  parameters :comment_form_attributes,
             name:          { type: :string, required: false, description: "Commenter name. No need to specify it if user is logged in. It's required for visitors" },
             email:         { type: :string, required: false, description: "Commenter email. No need to specify it if user is logged in. It's NOT required for anyone" },
             comment:       { type: :string, description: 'The comment' },
             allow_contact: { type: :boolean, required: false, description: 'Does commenter accepts to be contacted by tag author? It will be true for logged in users' }

  let(:tag) { FactoryBot.create :tag }

  let(:slug) { tag.slug }

  before do
    tag
  end

  on_get '/tags', 'List tags' do
    for_code 200 do |url|
      visit url
      expect(response).to have_many defined :tag
    end
  end

  on_get '/tags/:slug', 'Display one tag' do
    path_params defined: :common_path_params

    for_code 200 do |url|
      visit url

      expect(response).to have_one defined :tag
    end

    for_code 404 do |url|
      visit url, path_params: { slug: '0' }

      expect(response).to have_one defined :error
    end
  end
end
