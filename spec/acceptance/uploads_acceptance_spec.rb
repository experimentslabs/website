require 'acceptance_helper'

RSpec.describe UploadsController do
  resource 'Public - Upload', 'Published uploads'

  entity :file,
         content_type: { type: :string, description: 'File type' },
         filename:     { type: :string, description: 'File name' },
         metadata:     { type: :object, description: 'ActiveStorage metadata' },
         url:          { type: :string, description: 'URL to access the file' }

  entity :upload,
         id:           { type: :integer, description: 'Upload identifier' },
         title:        { type: :string, description: 'Title' },
         description:  { type: :string, description: 'Description' },
         file:         { type: :object, description: 'File infos', attributes: :file },
         sfw:          { type: :boolean, description: 'Is the upload safe for work?' },
         slug:         { type: :string, description: 'Slug for URLs' },
         user_id:      { type: :integer, description: 'Author identifier' },
         license_id:   { type: :integer, description: 'License identifier' },
         language_id:  { type: :integer, description: 'Language identifier' },
         album_ids:    { type: :array, description: 'Albums containing this file' },
         project_ids:  { type: :array, description: 'Projects containing this file' },
         tag_ids:      { type: :array, description: 'Tags identifiers' },
         published_at: { type: :datetime, required: false, description: 'Publication date' },
         created_at:   { type: :datetime, description: 'Creation date' },
         updated_at:   { type: :datetime, description: 'Last update date' }

  entity :paginated_response,
         content:  { type: :array, description: 'Uploads list', of: :upload },
         page:     { type: :integer, description: 'Current page' },
         pages:    { type: :integer, description: 'Total amount of pages' },
         per_page: { type: :integer, description: 'Amount of uploads per page' },
         total:    { type: :integer, description: 'Total amount of uploads' }

  entity :error,
         error: { type: :string, description: 'Error message' }

  entity :comment_response,
         message: { type: :string, description: 'Success message' }

  entity :comment_form_errors,
         user:    { type: :array, required: false, description: 'User-related errors when user is signed in' },
         name:    { type: :array, required: false, description: 'User name errors when user is a visitor' },
         comment: { type: :array, required: false, description: 'Comment related errors' }

  parameters :common_path_params,
             slug: { type: :string, description: 'Upload slug' }

  parameters :comment_form_attributes,
             name:          { type: :string, required: false, description: "Commenter name. No need to specify it if user is logged in. It's required for visitors" },
             email:         { type: :string, required: false, description: "Commenter email. No need to specify it if user is logged in. It's NOT required for anyone" },
             comment:       { type: :string, description: 'The comment' },
             allow_contact: { type: :boolean, required: false, description: 'Does commenter accepts to be contacted by upload author? It will be true for logged in users' }

  let(:user) { FactoryBot.create :user_active }
  let(:license) { FactoryBot.create :license }
  let(:language) { FactoryBot.create :language }
  let(:tag) { FactoryBot.create :tag }
  let(:project) { FactoryBot.create :project_published, user: user }
  let(:upload) { FactoryBot.create :upload_published, projects: [project], tags: [tag], user: user, language: language, license: license }

  let(:slug) { upload.slug }
  let(:project_slug) { project.slug }
  let(:username) { user.username }
  let(:license_slug) { license.slug }
  let(:iso639_1) { language.iso639_1 }
  let(:tag_slug) { tag.slug }

  before do
    upload
  end

  on_get '/uploads', 'List uploads' do
    for_code 200 do |url|
      visit url
      expect(response).to have_one defined :paginated_response
    end
  end

  on_get '/projects/:project_slug/uploads', 'List uploads in a given project' do
    path_params fields: { project_slug: { type: :string, description: 'A project' } }

    for_code 200 do |url|
      visit url
      expect(response).to have_one defined :paginated_response
    end
  end

  on_get '/users/:username/uploads', "List user's uploads" do
    path_params fields: { username: { type: :string, description: 'An user' } }

    for_code 200 do |url|
      visit url
      expect(response).to have_one defined :paginated_response
    end
  end

  on_get '/licenses/:license_slug/uploads', 'List uploads with a given license' do
    path_params fields: { license_slug: { type: :string, description: 'A license' } }

    for_code 200 do |url|
      visit url
      expect(response).to have_one defined :paginated_response
    end
  end

  on_get '/languages/:iso639_1/uploads', 'List uploads with a given language' do
    path_params fields: { iso639_1: { type: :string, description: 'A language' } }

    for_code 200 do |url|
      visit url
      expect(response).to have_one defined :paginated_response
    end
  end

  on_get '/tags/:tag_slug/uploads', 'List uploads with a given tag' do
    path_params fields: { tag_slug: { type: :string, description: 'A tag' } }

    for_code 200 do |url|
      visit url
      expect(response).to have_one defined :paginated_response
    end
  end

  on_get '/uploads/:slug', 'Display one upload' do
    path_params defined: :common_path_params

    for_code 200 do |url|
      visit url

      expect(response).to have_one defined :upload
    end

    for_code 404 do |url|
      visit url, path_params: { slug: '0' }

      expect(response).to have_one defined :error
    end
  end

  on_post '/uploads/:slug/comment', 'Leave a comment' do
    path_params defined: :common_path_params
    request_params defined: :comment_form_attributes

    for_code 201 do |url|
      visit url, payload: { comment: { name: 'John Doe', comment: 'Thank you!' } }

      expect(response).to have_one defined :comment_response
    end

    for_code 422 do |url|
      visit url, payload: { comment: { name: nil, comment: 'Does it works?' } }

      expect(response).to have_one defined :comment_form_errors
    end

    for_code 404 do |url|
      visit url, path_params: { slug: '0' }, payload: { comment: { name: 'John Doe', comment: 'Thank you!' } }

      expect(response).to have_one defined :error
    end
  end
end
