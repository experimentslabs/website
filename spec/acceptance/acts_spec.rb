require 'acceptance_helper'

RSpec.describe ActsController do
  resource 'Acts', 'Overall activity'

  entity :act,
         id:           { type: :integer, description: 'Act identifier' },
         content_type: { type: :string, description: 'Related content type' },
         content_id:   { type: :integer, description: 'Related content id' },
         event:        { type: :string, description: 'Event type' },
         created_at:   { type: :datetime, description: 'Event type' },
         content:      { type: :object, description: 'The related content entity' }

  entity :paginated_response,
         content:  { type: :array, description: 'Acts list', of: :act },
         page:     { type: :integer, description: 'Current page' },
         pages:    { type: :integer, description: 'Total amount of pages' },
         per_page: { type: :integer, description: 'Amount of acts per page' },
         total:    { type: :integer, description: 'Total amount of acts' }

  before do
    FactoryBot.create :album_published
    FactoryBot.create :article_published
    FactoryBot.create :link_published
    FactoryBot.create :note_published
    FactoryBot.create :project_published
    FactoryBot.create :upload_published
  end

  on_get '/activity', 'Last activity list' do
    for_code 200, 'success' do |example|
      visit example

      expect(response).to have_one defined :paginated_response
    end
  end
end
