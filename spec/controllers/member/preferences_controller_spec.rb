require 'rails_helper'

RSpec.describe Member::PreferencesController, type: :controller do
  routes { Rails.application.routes }

  let(:valid_attributes) do
    { show_nsfw: false }
  end

  let(:invalid_attributes) do
    { locale: 'de' }
  end

  let(:valid_session) { {} }

  context 'as a member' do
    include_context 'with authenticated member'

    describe 'GET #edit' do
      it 'returns a success response' do
        get :edit, session: valid_session
        expect(response).to be_successful
      end
    end

    describe 'PUT #update' do
      context 'with valid params' do
        let(:new_attributes) do
          { show_nsfw: true }
        end

        it 'updates the requested preference' do
          put :update, params: { preference: new_attributes }, session: valid_session
          preferences = User.find_by(email: 'member@example.com').preference
          expect(preferences.show_nsfw).to eq(true)
        end

        it 'redirects to the preferences form' do
          put :update, params: { preference: valid_attributes }, session: valid_session
          expect(response).to redirect_to(member_edit_preferences_path)
        end
      end

      context 'with invalid params' do
        it "returns a success response (i.e. to display the 'edit' template)" do
          put :update, params: { preference: invalid_attributes }, session: valid_session
          expect(response).to be_successful
        end
      end
    end
  end
end
