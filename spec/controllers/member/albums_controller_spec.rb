require 'rails_helper'

RSpec.describe Member::AlbumsController, type: :controller do
  model              = Album
  invalid_attributes = { name: nil, description: nil }
  new_attributes     = { name: 'A new name', description: 'A new description' }
  valid_session      = {}
  belongs_to_content = %w[upload project]

  it_behaves_like 'member index entity', model, valid_session
  it_behaves_like 'member create entity form', valid_session
  it_behaves_like 'member create entity', model, invalid_attributes, valid_session
  it_behaves_like 'member create content', model, valid_session, belongs_to_content
  it_behaves_like 'member update entity form', model, valid_session
  it_behaves_like 'member update entity', model, invalid_attributes, valid_session
  it_behaves_like 'member update content', model, new_attributes, valid_session, belongs_to_content
  it_behaves_like 'member destroy entity', model, valid_session
  it_behaves_like 'member publish content', model, valid_session
end
