require 'rails_helper'

RSpec.describe Member::UploadsController, type: :controller do
  model              = Upload
  invalid_attributes = { title: nil }
  new_attributes     = { title: 'A new title' }
  valid_session      = {}
  belongs_to_content = %w[album project]

  it_behaves_like 'member index entity', model, valid_session
  it_behaves_like 'member create entity form', valid_session
  it_behaves_like 'member create entity', model, invalid_attributes, valid_session, :file
  it_behaves_like 'member create content', model, valid_session, belongs_to_content, :file
  it_behaves_like 'member update entity form', model, valid_session
  it_behaves_like 'member update entity', model, invalid_attributes, valid_session
  it_behaves_like 'member update content', model, new_attributes, valid_session, belongs_to_content
  it_behaves_like 'member destroy entity', model, valid_session
  it_behaves_like 'member publish content', model, valid_session
end
