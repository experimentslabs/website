require 'rails_helper'

RSpec.describe Member::ProjectsController, type: :controller do
  model              = Project
  invalid_attributes = { name: nil, short_description: nil, description: nil }
  new_attributes     = { name: 'A new name', short_description: 'A new excerpt' }
  valid_session      = {}
  belongs_to_content = [] # %w[album article note upload]

  it_behaves_like 'member index entity', model, valid_session
  it_behaves_like 'member create entity form', valid_session
  it_behaves_like 'member create entity', model, invalid_attributes, valid_session
  it_behaves_like 'member create content', model, valid_session, belongs_to_content
  it_behaves_like 'member update entity form', model, valid_session
  it_behaves_like 'member update entity', model, invalid_attributes, valid_session
  it_behaves_like 'member update content', model, new_attributes, valid_session, belongs_to_content
  it_behaves_like 'member destroy entity', model, valid_session
  it_behaves_like 'member publish content', model, valid_session
end
