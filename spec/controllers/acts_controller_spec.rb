require 'rails_helper'

RSpec.describe ActsController, type: :controller do
  let(:valid_session) { {} }

  describe 'GET #index' do
    it 'returns a success response' do
      FactoryBot.create :article_published
      get :index, params: {}, session: valid_session
      expect(response).to be_successful
    end
  end
end
