require 'rails_helper'

RSpec.describe Admin::ReportsController, type: :controller do
  model            = Report
  valid_session    = {}

  it_behaves_like 'admin index entity', model, valid_session
  it_behaves_like 'admin destroy entity', model, valid_session
end
