require 'rails_helper'

RSpec.describe Admin::TagsController, type: :controller do
  model              = Tag
  invalid_attributes = { name: nil }
  valid_session      = {}

  it_behaves_like 'admin index entity', model, valid_session
  it_behaves_like 'admin update entity form', model, valid_session
  it_behaves_like 'admin update entity', model, invalid_attributes, valid_session
  it_behaves_like 'admin destroy entity', model, valid_session
end
