require 'rails_helper'
RSpec.describe Admin::UsersController, type: :controller do
  model              = User
  valid_session      = {}

  it_behaves_like 'admin index entity', model, valid_session
  it_behaves_like 'admin show entity', model, valid_session
  it_behaves_like 'admin destroy entity', model, valid_session
end
