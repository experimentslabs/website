require 'rails_helper'

RSpec.describe Admin::ArticlesController, type: :controller do
  model         = Article
  valid_session = {}

  it_behaves_like 'admin index entity', model, valid_session
  it_behaves_like 'admin destroy entity', model, valid_session
  it_behaves_like 'admin lock content', model, valid_session
  it_behaves_like 'admin destroy content', model, valid_session
end
