require 'rails_helper'

RSpec.describe Admin::AnnouncementsController, type: :controller do
  model              = Announcement
  invalid_attributes = { level: 'invalid' }
  valid_session      = {}

  it_behaves_like 'admin index entity', model, valid_session
  it_behaves_like 'admin create entity form', valid_session
  it_behaves_like 'admin create entity', model, invalid_attributes, valid_session
  it_behaves_like 'admin update entity form', model, valid_session
  it_behaves_like 'admin update entity', model, invalid_attributes, valid_session
  it_behaves_like 'admin destroy entity', model, valid_session
end
