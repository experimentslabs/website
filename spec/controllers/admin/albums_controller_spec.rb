require 'rails_helper'

RSpec.describe Admin::AlbumsController, type: :controller do
  model         = Album
  valid_session = {}

  it_behaves_like 'admin index entity', model, valid_session
  it_behaves_like 'admin destroy entity', model, valid_session
  it_behaves_like 'admin lock content', model, valid_session
  it_behaves_like 'admin destroy content', model, valid_session
end
