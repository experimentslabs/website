# Changelog

All notable changes to this project will be documented in this file.

<!-- Format:
## 2019-01-01 - Description

### Added

### Changed

### Improved

### Removed

### Fixed
-->

## 2020-05-18 - Fixes and maintenance

- Re-enable SimpleCov
- Use webpacker to generate styles
- Improve content cards
- Reduce size of JS bundle
- Fix modals not showing

## 2020-02-26 - Beginning of UI rework

- Rework projects views
- Reorder main menu
- Change home page to the last x projects
- Add logos to projects
- Add a sitemap using `sitemap_generator` gem
- Fixed licenses fonticon

## 2020-02-21 - Initial creation
Refer to the old
[engine CHANGELOG](https://gitlab.com/experimentslabs/engine_elabs/-/blob/develop/CHANGELOG.md).
