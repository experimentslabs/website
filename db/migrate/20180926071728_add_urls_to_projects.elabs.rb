# This migration comes from elabs (originally 20180917133442)
class AddUrlsToProjects < ActiveRecord::Migration[5.2]
  def change
    change_table :projects, bulk: true do |t|
      t.string :sources_url
      t.string :docs_url
    end
  end
end
