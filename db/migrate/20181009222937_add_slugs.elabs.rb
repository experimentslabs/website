# This migration comes from elabs (originally 20181006073541)
class AddSlugs < ActiveRecord::Migration[5.2]
  def up
    [:licenses, :tags, :albums, :articles, :notes, :projects, :uploads].each do |table|
      add_column table, :slug, :string, null: true
    end

    ActiveRecord::Base.record_timestamps = false
    begin
      [
        { model: License, actable: false },
        { model: Tag, actable: false },
        { model: Album, actable: true },
        { model: Article, actable: true },
        { model: Note, actable: true },
        { model: Project, actable: true },
        { model: Upload, actable: true },
      ].each do |table|
        table[:model].all.each do |record|
          record.fill_slug
          record.minor_update = true if table[:actable]
          record.save!
        end
      end
    ensure
      ActiveRecord::Base.record_timestamps = true
    end

    [:licenses, :tags, :albums, :articles, :notes, :projects, :uploads].each do |table|
      change_column table, :slug, :string, null: false
      add_index table, :slug, unique: true
    end
  end

  def down
    [:licenses, :tags, :albums, :articles, :notes, :projects, :uploads].each do |table|
      remove_column table, :slug
    end
  end
end
