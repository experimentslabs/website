# This migration comes from elabs (originally 20181025084232)
class AddArchivedFieldToComments < ActiveRecord::Migration[5.2]
  def change
    add_column :comments, :archived, :boolean, null: false, default: false
  end
end
