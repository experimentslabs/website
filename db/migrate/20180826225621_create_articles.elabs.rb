# This migration comes from elabs (originally 20180813000011)
class CreateArticles < ActiveRecord::Migration[5.2]
  def change
    create_table :articles do |t|
      t.string  :title,             null: false, default: nil
      t.string  :excerpt,           null: false, default: nil
      t.text    :content,           null: false, default: nil
      t.boolean :sfw,               null: false, default: false
      t.boolean :published,         null: false, default: false
      t.boolean :locked,            null: false, default: false
      t.boolean :hidden_in_history, null: false, default: false

      t.references :user,     foreign_key: true
      t.references :license,  foreign_key: true
      t.references :language, foreign_key: true

      t.datetime :published_at,     null: true
      t.timestamps
    end

    create_join_table :articles, :tags
    add_index :articles_tags, [:article_id, :tag_id], unique: true
  end
end
