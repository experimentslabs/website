# This migration comes from elabs (originally 20190501163041)
class AddNotificationPreferences < ActiveRecord::Migration[5.2]
  def change
    change_table :preferences, bulk: true do |t|
      t.boolean :receive_comment_notifications, null: false, default: true
      t.boolean :receive_content_notifications, null: false, default: true
      t.boolean :receive_admin_notifications, null: false, default: true
    end
  end
end
