# This migration comes from elabs (originally 20180813000002)
class CreateTags < ActiveRecord::Migration[5.2]
  def change
    create_table :tags do |t| # rubocop:disable Rails/CreateTableWithTimestamps
      t.string :name, null: false
      t.integer :albums_count,   default: 0, null: false
      t.integer :articles_count, default: 0, null: false
      t.integer :notes_count,    default: 0, null: false
      t.integer :projects_count, default: 0, null: false
      t.integer :uploads_count,  default: 0, null: false

      t.index :name, unique: true
    end
  end
end
