# This migration comes from elabs (originally 20180910000015)
class RemoveLicenseTimestamps < ActiveRecord::Migration[5.2]
  def change
    reversible do |dir|
      dir.up do
        remove_column :licenses, :created_at
        remove_column :licenses, :updated_at
      end

      dir.down do
        change_table :licenses, &:timestamps
      end
    end
  end
end
