# This migration comes from elabs (originally 20181026071743)
class DeleteCommentNotifications < ActiveRecord::Migration[5.2]
  def change
    Notification.where(event: :comment).destroy_all
  end
end
