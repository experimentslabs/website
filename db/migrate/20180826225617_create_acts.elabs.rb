# This migration comes from elabs (originally 20180813000007)
class CreateActs < ActiveRecord::Migration[5.2]
  def change
    create_table :acts do |t|
      t.references :content, polymorphic: true
      t.string :event

      t.timestamps
    end
  end
end
