# This migration comes from elabs (originally 20180813000013)
class CreateUploads < ActiveRecord::Migration[5.2]
  def change
    create_table :uploads do |t|
      t.string  :title,             null: false, default: nil
      t.text    :description,       null: false, default: nil
      t.boolean :sfw,               null: false, default: false
      t.boolean :published,         null: false, default: false
      t.boolean :locked,            null: false, default: false
      t.boolean :hidden_in_history, null: false, default: false

      t.references :user,     foreign_key: true
      t.references :license,  foreign_key: true
      t.references :language, foreign_key: true

      t.datetime :published_at,     null: true
      t.timestamps
    end

    create_join_table :albums, :uploads
    add_index :albums_uploads, [:album_id, :upload_id], unique: true

    create_join_table :uploads, :tags
    rename_table :tags_uploads, :uploads_tags
    add_index :uploads_tags, [:upload_id, :tag_id], unique: true
  end
end
