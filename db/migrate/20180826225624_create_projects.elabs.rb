# This migration comes from elabs (originally 20180813000014)
class CreateProjects < ActiveRecord::Migration[5.2]
  def change
    create_table :projects do |t|
      t.string :name,              null: false, default: nil
      t.string :short_description, null: false, default: nil
      t.text :description,         null: false, default: nil
      t.string :main_url
      t.boolean :sfw,               null: false, default: false
      t.boolean :published,         null: false, default: false
      t.boolean :locked,            null: false, default: false
      t.boolean :hidden_in_history, null: false, default: false

      t.integer :albums_count,   null: false, default: 0
      t.integer :articles_count, null: false, default: 0
      t.integer :notes_count,    null: false, default: 0
      t.integer :uploads_count,  null: false, default: 0

      t.references :user,     foreign_key: true
      t.references :license,  foreign_key: true
      t.references :language, foreign_key: true

      t.datetime :published_at,     null: true
      t.timestamps
    end

    create_join_table :projects, :albums
    rename_table :albums_projects, :projects_albums
    add_index :projects_albums, [:project_id, :album_id], unique: true

    create_join_table :projects, :articles
    rename_table :articles_projects, :projects_articles
    add_index :projects_articles, [:project_id, :article_id], unique: true

    create_join_table :projects, :notes
    rename_table :notes_projects, :projects_notes
    add_index :projects_notes,    [:project_id, :note_id], unique: true

    create_join_table :projects, :uploads
    add_index :projects_uploads, [:project_id, :upload_id], unique: true

    create_join_table :projects, :tags
    add_index :projects_tags, [:project_id, :tag_id], unique: true
  end
end
