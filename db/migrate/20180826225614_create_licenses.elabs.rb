# This migration comes from elabs (originally 20180813000004)
class CreateLicenses < ActiveRecord::Migration[5.2]
  def change
    create_table :licenses do |t|
      t.string :name, null: false
      t.string :url, null: false
      t.string :tldr_url, null: true
      t.string :icon, null: false
      t.integer :albums_count,   default: 0, null: false
      t.integer :articles_count, default: 0, null: false
      t.integer :notes_count,    default: 0, null: false
      t.integer :projects_count, default: 0, null: false
      t.integer :uploads_count,  default: 0, null: false

      t.timestamps
    end
  end
end
