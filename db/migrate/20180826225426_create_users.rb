# frozen_string_literal: true

class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string  :username, null: false, default: nil
      t.string  :real_name
      t.string  :email,          null: false, default: ''
      t.string  :password,       null: false, default: ''
      t.text    :biography
      t.string  :role,           null: false, default: 'user'
      t.integer :albums_count,   null: false, default: 0
      t.integer :articles_count, null: false, default: 0
      t.integer :notes_count,    null: false, default: 0
      t.integer :projects_count, null: false, default: 0
      t.integer :uploads_count,  null: false, default: 0

      t.timestamps
    end

    add_index :users, :username, unique: true
  end
end
