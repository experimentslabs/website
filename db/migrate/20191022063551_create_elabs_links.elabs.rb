# This migration comes from elabs (originally 20190501163042)
class CreateElabsLinks < ActiveRecord::Migration[5.2]
  def change
    create_table :links do |t|
      t.string :title, null: false, default: nil
      t.string :url,   null: false, default: nil
      t.string :description
      t.boolean :sfw,               null: false, default: false
      t.boolean :published,         null: false, default: false
      t.boolean :locked,            null: false, default: false
      t.boolean :hidden_in_history, null: false, default: false

      t.references :user, foreign_key: true
      t.references :language, foreign_key: true

      t.datetime :published_at,     null: true
      t.timestamps
    end

    create_join_table :links, :tags
    add_index :links_tags, [:link_id, :tag_id], unique: true

    add_column :users,     :links_count, :integer, default: 0, null: false
    add_column :tags,      :links_count, :integer, default: 0, null: false
    add_column :languages, :links_count, :integer, default: 0, null: false
  end
end
