# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = Rails.configuration.elabs.host

SitemapGenerator::Sitemap.create do # rubocop:disable Metrics/BlockLength
  # Put links creation logic here.
  #
  # The root path '/' and sitemap index file are added automatically for you.
  # Links are added to the Sitemap in the order they are specified.
  #
  # Usage: add(path, options={})
  #        (default options are used if you don't specify)
  #
  # Defaults: :priority => 0.5, :changefreq => 'weekly',
  #           :lastmod => Time.now, :host => default_host
  #
  # Examples:
  #
  # Add '/articles'
  #
  #   add articles_path, :priority => 0.7, :changefreq => 'daily'
  #
  # Add all articles:
  #
  #   Article.find_each do |article|
  #     add article_path(article), :lastmod => article.updated_at
  #   end

  # Content
  add albums_path, priority: 0.7, changefreq: 'daily'
  add articles_path, priority: 0.7, changefreq: 'daily'
  add links_path, priority: 0.7, changefreq: 'daily'
  add notes_path, priority: 0.7, changefreq: 'daily'
  add projects_path, priority: 0.9, changefreq: 'daily'
  add uploads_path, priority: 0.7, changefreq: 'daily'

  # Classifiers
  add languages_path, priority: 0.5, changefreq: 'weekly'
  add licenses_path, priority: 0.5, changefreq: 'weekly'
  add tags_path, priority: 0.5, changefreq: 'daily'
  add users_path, priority: 0.5, changefreq: 'daily'

  # Endpoints of lesser importance
  add activities_path, priority: 0.3, changefreq: 'daily'

  # Albums
  Album.publicly_visible.find_each do |album|
    add album_path(album), lastmod: album.updated_at
  end

  # Articles
  Article.publicly_visible.find_each do |article|
    add article_path(article), lastmod: article.updated_at
  end

  # Notes
  Note.publicly_visible.find_each do |note|
    add note_path(note), lastmod: note.updated_at
  end

  # Projects
  Project.publicly_visible.find_each do |project|
    add project_path(project), lastmod: project.updated_at
  end

  # Files
  Upload.publicly_visible.find_each do |upload|
    add upload_path(upload), lastmod: upload.updated_at
  end

  # Users
  User.find_each do |user|
    add user_path(user), lastmod: user.updated_at
  end
end
