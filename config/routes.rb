Rails.application.routes.draw do
  # Devise routes: use Elabs custom controllers (adds support for username and preferences)
  devise_for :users, path: 'auth', controllers: {
    confirmations: 'auth/confirmations',
    passwords:     'auth/passwords',
    registrations: 'auth/registrations',
    sessions:      'auth/sessions',
    unlocks:       'auth/unlocks',
  }
  # Resources
  resources :albums,    only: [:index, :show], param: :slug do
    resources :uploads, only: [:index]
    post 'comment', to: 'albums#create_comment', as: 'create_comment', param: :slug
  end
  resources :articles,  only: [:index, :show], param: :slug do
    post 'comment', to: 'articles#create_comment', as: 'create_comment', param: :slug
  end
  resources :notes, only: [:index, :show], param: :slug do
    post 'comment', to: 'notes#create_comment', as: 'create_comment', param: :slug
  end
  resources :projects, only: [:index, :show], param: :slug do
    resources :articles, only: [:index]
    resources :albums,   only: [:index]
    resources :notes,    only: [:index]
    resources :uploads,  only: [:index]
    post 'comment', to: 'projects#create_comment', as: 'create_comment', param: :slug
  end
  resources :uploads, only: [:index, :show], param: :slug do
    post 'comment', to: 'uploads#create_comment', as: 'create_comment', param: :slug
  end
  resources :links, only: [:index]

  # Classifiers
  resources :languages, only: [:index, :show], param: :iso639_1 do
    resources :albums,   only: [:index]
    resources :articles, only: [:index]
    resources :links,    only: [:index]
    resources :notes,    only: [:index]
    resources :projects, only: [:index]
    resources :uploads,  only: [:index]
  end
  resources :licenses, only: [:index, :show], param: :slug do
    resources :albums,   only: [:index]
    resources :articles, only: [:index]
    resources :notes,    only: [:index]
    resources :projects, only: [:index]
    resources :uploads,  only: [:index]
  end
  resources :tags, only: [:index, :show], param: :slug do
    resources :albums,   only: [:index]
    resources :articles, only: [:index]
    resources :links,    only: [:index]
    resources :notes,    only: [:index]
    resources :projects, only: [:index]
    resources :uploads,  only: [:index]
  end
  resources :users, only: [:index, :show], param: :username do
    resources :albums,   only: [:index]
    resources :links,    only: [:index]
    resources :articles, only: [:index]
    resources :notes,    only: [:index]
    resources :projects, only: [:index]
    resources :uploads,  only: [:index]
  end

  # Reports
  post '/reports', to: 'reports#create', as: :create_report

  # Activity
  get '/activity', to: 'acts#index', as: :activities

  # Member
  namespace :member do
    resources :albums, except: [:show], param: :slug do
      put 'toggle_publication', to: 'albums#toggle_publication', as: :toggle_publication
    end
    resources :articles, except: [:show], param: :slug do
      put 'toggle_publication', to: 'articles#toggle_publication', as: :toggle_publication
    end
    resources :links, except: [:show] do
      put 'toggle_publication', to: 'links#toggle_publication', as: :toggle_publication
    end
    resources :notes, except: [:show], param: :slug do
      put 'toggle_publication', to: 'notes#toggle_publication', as: :toggle_publication
    end
    resources :projects, except: [:show], param: :slug do
      put 'toggle_publication', to: 'projects#toggle_publication', as: :toggle_publication
    end
    resources :uploads, except: [:show], param: :slug do
      put 'toggle_publication', to: 'uploads#toggle_publication', as: :toggle_publication
    end

    resources :notifications, only: [:index, :destroy]

    resources :comments, only: [:destroy] do
      put '/archive', to: 'comments#archive', as: :archive
    end

    get '/preferences', to: 'preferences#edit', as: :edit_preferences
    put '/preferences', to: 'preferences#update', as: :update_preferences

    get '/infos', to: 'users#edit', as: :edit_infos
    put '/infos', to: 'users#update', as: :update_infos

    post '/markdown_previewer', to: 'markdown_previewer#preview', as: :markdown_preview
  end

  # Admin
  namespace :admin do
    # Content resources
    resources :albums, only: [:index, :destroy], param: :slug do
      put 'toggle_lock', to: 'albums#toggle_lock', as: :toggle_lock
    end
    resources :articles, only: [:index, :destroy], param: :slug do
      put 'toggle_lock', to: 'articles#toggle_lock', as: :toggle_lock
    end
    resources :links, only: [:index, :destroy] do
      put 'toggle_lock', to: 'links#toggle_lock', as: :toggle_lock
    end
    resources :notes, only: [:index, :destroy], param: :slug do
      put 'toggle_lock', to: 'notes#toggle_lock', as: :toggle_lock
    end
    resources :projects, only: [:index, :destroy], param: :slug do
      put 'toggle_lock', to: 'projects#toggle_lock', as: :toggle_lock
    end
    resources :uploads, only: [:index, :destroy], param: :slug do
      put 'toggle_lock', to: 'uploads#toggle_lock', as: :toggle_lock
    end

    # Basic resources
    resources :announcements, except: [:show]
    resources :languages,     except: [:show], param: :iso639_1
    resources :licenses,      except: [:show], param: :slug
    resources :reports,       only:   [:index, :destroy]
    resources :tags,          except: [:create, :show], param: :slug
    resources :users,         only:   [:index, :show, :destroy], param: :username
  end

  root to: 'home#index'
end
