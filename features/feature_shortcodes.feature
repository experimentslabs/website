Feature: Shortcodes
  As a website member
  In order to reference and display data in my content
  I want to be able to write short codes that transforms into widgets


  Background:
    Given I am a member with email "bobby@example.com"

  Scenario: Shortcode to a published article
    When I create a note with a shortcode to a safe, published, unlocked article
    Then I should see the article widget in the note

  Scenario: Inline shortcode to a published article
    When I create a note with a shortcode to a safe, published, unlocked inline article
    Then I should see the inline article widget in the note

  Scenario: Shortcode to a nsfw article
    When I create a note with a shortcode to an unsafe, published, unlocked article
    Then I should see the NSFW widget in the note

  Scenario: Inline shortcode to a nsfw article
    When I create a note with a shortcode to an unsafe, published, unlocked inline article
    Then I should see the NSFW inline widget in the note

  Scenario: Shortcode to an unpublished article
    When I create a note with a shortcode to a safe, unpublished, unlocked article
    Then I should see the "not found" widget in the note

  Scenario: Inline shortcode to an unpublished article
    When I create a note with a shortcode to a safe, unpublished, unlocked inline article
    Then I should see the "not found" inline widget in the note

  Scenario: Shortcode to an locked article
    When I create a note with a shortcode to a safe, published, locked article
    Then I should see the "not found" widget in the note

  Scenario: Inline shortcode to an locked article
    When I create a note with a shortcode to a safe, published, locked inline article
    Then I should see the "not found" inline widget in the note

  Scenario: Shortcode to self-content
    When I create a note with a shortcode to itself
    Then I should see the "infinite loop" widget in the note

  Scenario: Inline shortcode to self-content
    When I create a note with an inline shortcode to itself
    Then I should see the "infinite loop" inline widget in the note

  Scenario: Shortcode to content containing shortcodes
    When I create a note with shortcodes to an article with shortcodes
    Then I should see the "display full element" widget in the note

  Scenario: Inline shortcode to content containing shortcodes
    When I create a note with shortcodes to an article with inline shortcodes
    Then I should see the user inline widget in the note
