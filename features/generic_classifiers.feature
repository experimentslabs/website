Feature: Content classifiers
  As an user
  In order to have a good accessibility to the site
  I want to be able to filter content by their language content
  I want to be able to filter content by their tags

  Background:
    Given a website with a "fr" language for content in "Français"
    And a website with an "en" language for content in "English"
    And a website with a "VueJS" tag
    And a website with a "PHP" tag
    And a website with a "GPL 3.0" license
    And a website with a "CC-BY" license
    And a website with 2 safe, published elements of every type with language "fr", "GPL 3.0" license and "VueJS" tag
    And a website with 2 safe, published elements of every type with language "en", "CC-BY" license and "PHP" tag

  Scenario Outline: Show the classifers lists
    When I visit the public <plural_type> page
    Then I should see <amount> <singular_type>

    Examples:
      | amount | singular_type | plural_type |
      | 2      | language      | languages   |
      | 2      | license       | licenses    |
      | 2      | tag           | tags        |

  Scenario Outline: Display one classifier page
    When I visit the "<title>" public <singular_type> page
    Then I should see the "<title>" <singular_type> description

    Examples:
      | title    | singular_type |
      | Français | language      |
      | GPL 3.0  | license       |
      | VueJS    | tag           |

  Scenario Outline: Filter the content shown by language
    Given I visit the "Français" public language page
    When I click on the link to display all the associated <plural_type>
    Then I should see a list of <amount> <plural_type>

    Examples:
      | amount | plural_type |
      | 2      | albums      |
      | 2      | articles    |
      | 2      | notes       |
      | 2      | projects    |
      | 2      | uploads     |
      | 2      | links       |

  Scenario Outline: Filter the content shown by license
    Given I visit the "GPL 3.0" public license page
    When I click on the link to display all the associated <plural_type>
    Then I should see a list of <amount> <plural_type>

    Examples:
      | amount | plural_type |
      | 2      | albums      |
      | 2      | articles    |
      | 2      | notes       |
      | 2      | projects    |
      | 2      | uploads     |

  Scenario Outline: Filter the content shown by tag
    Given I visit the "VueJS" public tag page
    When I click on the link to display all the associated <plural_type>
    Then I should see a list of <amount> <plural_type>

    Examples:
      | amount | plural_type |
      | 2      | albums      |
      | 2      | articles    |
      | 2      | notes       |
      | 2      | projects    |
      | 2      | uploads     |
