Feature: Content export
  As a website maintainer,
  In order to give visitors a simple way to keep the content proposed
  I want to allow them to export it in markdown format

  Background:
    Given a website with 1 safe, published element of every type
    And I am not logged in

  Scenario Outline: Export in markdown
    When I manually visit the first <content_type>
    And I click on "Markdown source"
    Then a markdown file should be presented to me

    Examples:
      | content_type |
      | album        |
      | article      |
      | note         |
      | project      |
      | upload       |

  Scenario Outline: Export in markdown for Pandoc
    When I manually visit the first <content_type>
    And I click on "Markdown for Pandoc source"
    Then a markdown file should be presented to me

    Examples:
      | content_type |
      | album        |
      | article      |
      | note         |
      | project      |
      | upload       |
