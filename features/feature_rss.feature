Feature: RSS feeds
  As a website maintainer
  In order to give the users the ability to follow content without visiting the site
  I want to provide RSS feeds for all content types

  Background:
    Given a website with an active user "Karine" with email "karine@example.com"
    And a website with 1 unsafe, published element of every type created by "karine@example.com"
    And I visit the home page

  Scenario Outline: View all elements in feed
    Given I toggle the SFW display option to display NSFW content
    When I check the "<content_type>" RSS feed
    Then I should see <amount> items in the RSS feed

    Examples:
      | content_type | amount |
      | activity     | 6      |
      | albums       | 1      |
      | articles     | 1      |
      | links        | 1      |
      | notes        | 1      |
      | projects     | 1      |
      | uploads      | 1      |

  Scenario Outline: View SFW elements only
    Given I toggle the SFW display option to hide NSFW content
    When I check the "<content_type>" RSS feed
    Then I should see <amount> items in the RSS feed
    Examples:
      | content_type | amount |
      | albums       | 0      |
      | articles     | 0      |
      | links        | 0      |
      | notes        | 0      |
      | projects     | 0      |
      | uploads      | 0      |
