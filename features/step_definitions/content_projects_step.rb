Given(/^a website with one (sfw|nsfw), (published|unpublished) project titled "(.*)"$/) do |sfw, state, title|
  factory = state == 'published' ? :project_published : :project
  FactoryBot.create factory, name: title, language: Language.first, sfw: sfw == 'sfw'
end

Given(/^a website with one (sfw|nsfw), (published|unpublished) project, "(.*)" which I created$/) do |sfw, state, title|
  user    = User.find_by(email: 'bob@example.com')
  factory = state == 'published' ? :project_published : :project
  FactoryBot.create factory, name: title, user: user, language: Language.first, sfw: sfw == 'sfw'
end

Given('I create an project with title {string}') do |name|
  visit '/member/projects'
  click_on 'New Project'

  within('#new_project') do
    fill_in 'Name', with: name
    fill_in 'Short description', with: 'Introduction'
    fill_in 'Description', with: 'Description here'
    select 'Français', from: 'project[language_id]'
    select 'Creative Commons by', from: 'project[license_id]'
  end

  click_on 'Save'
end

Given('I change project {string} to {string}') do |title, new_name|
  visit 'member/projects'

  find('.projects-list .project', text: title).click_on 'Edit'

  within('.edit_project') do
    fill_in 'Name', with: new_name
    fill_in 'Short description', with: 'Some new content'
    fill_in 'Description', with: 'Some new content'
  end

  click_on 'Save'
end
