Given(/^a website with one (sfw|nsfw), (published|unpublished) upload titled "(.*)"$/) do |sfw, state, title|
  factory = state == 'published' ? :upload_published : :upload
  FactoryBot.create factory, title: title, language: Language.first, sfw: sfw == 'sfw'
end

Given(/^a website with one (sfw|nsfw), (published|unpublished) upload, "(.*)" which I created$/) do |sfw, state, title|
  user    = User.find_by(email: 'bob@example.com')
  factory = state == 'published' ? :upload_published : :upload
  FactoryBot.create factory, title: title, user: user, language: Language.first, sfw: sfw == 'sfw'
end

Given('a website with one sfw, published upload, {string} which I created, linked to project {string} and album {string}') do |title, project_name, album_name|
  user    = User.find_by(email: 'bob@example.com')
  album   = Album.find_by(name: album_name)
  project = Project.find_by(name: project_name)

  FactoryBot.create :upload_published, title: title, user: user, language: Language.first, sfw: true, albums: [album], projects: [project]
end

Given('I create an upload with title {string}') do |string|
  visit '/member/uploads'
  click_on 'New Upload'

  within('#new_upload') do
    fill_in 'Title', with: string
    fill_in 'Description', with: 'A nice work needs a nice description'
    select 'Français', from: 'upload[language_id]'
    select 'Creative Commons by', from: 'upload[license_id]'
    file = File.realpath File.join(File.expand_path('../../spec/fixtures/files', __dir__), 'Woody.jpg')
    attach_file 'File', file
  end

  click_on 'Save'
end

Given('I change upload {string} to {string}') do |title, new_title|
  visit 'member/uploads'

  find('.uploads-list .upload', text: title).click_on 'Edit'

  within('.edit_upload') do
    fill_in 'Title', with: new_title
  end

  click_on 'Save'
end
