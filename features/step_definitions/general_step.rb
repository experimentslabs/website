Then(/^I should(?: still)? see "(.*)"$/) do |string|
  expect(page).to have_content(string)
end

Then(/^I should see a list of (album|article|language|license|link|note|project|tag|upload)s?$/) do |type|
  expect(all(".#{type}").count).to eq 1
end

Then('I can see a {string} link') do |string|
  expect(page).to have_link(string)
end

Then(/^I should see the "(.*)" (?:.*) description$/) do |name|
  expect(page).to have_content(name)
end

Then(/^I should see (\d+) (album|article|language|license|link|note|project|tag|upload)s?$/) do |amount, type|
  within(".#{type}s-list") do
    expect(all(".#{type}").count).to eq(amount)
  end
end

Given(/^a website with (\d+) (safe|unsafe), published element[s]? of every type created by "(.+)"$/) do |amount, sfw, email|
  user = if email == 'john.do@vo.id'
           FactoryBot.create :user_active, email: email
         else
           User.find_by(email: email)
         end
  language = FactoryBot.create :language, iso639_1: 'fr', name: 'Français'
  license = FactoryBot.create :license, name: 'Creative Commons by-nc-sa 4.0', url: 'https://creativecommons.org/licenses/by-nc-sa/4.0/', tldr_url: 'https://tldrlegal.com/license/creative-commons-attribution-noncommercial-sharealike-4.0-international-(cc-by-nc-sa-4.0)'
  tag = FactoryBot.create :tag, name: 'Ruby'

  default_relations = { user: user, language: language, license: license, tags: [tag] }
  # -1 because one has been created before
  FactoryBot.create_list :user,     (amount - 1)
  FactoryBot.create_list :language, (amount - 1)
  FactoryBot.create_list :license,  (amount - 1)
  FactoryBot.create_list :tag,      (amount - 1)
  FactoryBot.create_list (sfw == 'safe' ? :project_published_and_safe : :project_published), amount, default_relations
  FactoryBot.create_list (sfw == 'safe' ? :album_published_and_safe   : :album_published),   amount, default_relations
  FactoryBot.create_list (sfw == 'safe' ? :link_published_and_safe    : :link_published),    amount, user: user, language: language, tags: [tag]
  FactoryBot.create_list (sfw == 'safe' ? :article_published_and_safe : :article_published), amount, default_relations
  FactoryBot.create_list (sfw == 'safe' ? :note_published_and_safe    : :note_published),    amount, default_relations
  FactoryBot.create_list (sfw == 'safe' ? :upload_published_and_safe  : :upload_published),  amount, default_relations
end

Given(/^a website with (\d+) (safe|unsafe), published element[s]? of every type$/) do |amount, sfw|
  step "a website with #{amount} #{sfw}, published elements of every type created by \"john.do@vo.id\""
end

Given(/^a website with (\d+) random (safe|unsafe), (locked|unlocked), (published|unpublished) element[s]? of every type$/) do |amount, sfw, locked, published|
  factory_suffix = ''
  factory_suffix = '_published' if published == 'published'
  options = {
    sfw:    sfw == 'safe',
    locked: locked == 'locked',
  }

  FactoryBot.create_list "album#{factory_suffix}".to_sym,   amount, options
  FactoryBot.create_list "article#{factory_suffix}".to_sym, amount, options
  FactoryBot.create_list "link#{factory_suffix}".to_sym,    amount, options
  FactoryBot.create_list "note#{factory_suffix}".to_sym,    amount, options
  FactoryBot.create_list "project#{factory_suffix}".to_sym, amount, options
  FactoryBot.create_list "upload#{factory_suffix}".to_sym,  amount, options
end

Then('I should not see {string}') do |string|
  expect(page).not_to have_content(string)
end

Then('I should not see {string} and {string}') do |string, string2|
  expect(page).not_to have_content(string)
  expect(page).not_to have_content(string2)
end
