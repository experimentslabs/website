Given(/^a website with one (sfw|nsfw), (published|unpublished) note containing "(.*)"$/) do |sfw, state, title|
  factory = state == 'published' ? :note_published : :note
  FactoryBot.create factory, content: title, language: Language.first, sfw: sfw == 'sfw'
end

Given(/^a website with one (sfw|nsfw), (published|unpublished) note, "(.*)" which I created$/) do |sfw, state, title|
  user    = User.find_by(email: 'bob@example.com')
  factory = state == 'published' ? :note_published : :note
  FactoryBot.create factory, content: title, user: user, language: Language.first, sfw: sfw == 'sfw'
end

Given('I create an note containing {string}') do |content|
  visit '/member/notes'
  click_on 'New Note'

  within('#new_note') do
    fill_in 'Content', with: content
    select 'Français', from: 'note[language_id]'
    select 'Creative Commons by', from: 'note[license_id]'
  end

  click_on 'Save'
end

Given('I change note {string} to {string}') do |content, new_content|
  visit 'member/notes'

  find('.notes-list .note', text: content).click_on 'Edit'

  within('.edit_note') do
    fill_in 'Content', with: new_content
  end

  click_on 'Save'
end
