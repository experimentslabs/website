Given(/^a website with one (sfw|nsfw), (published|unpublished) article titled "(.*)"$/) do |sfw, state, title|
  factory = state == 'published' ? :article_published : :article
  FactoryBot.create factory, title: title, language: Language.first, sfw: sfw == 'sfw'
end

Given(/^a website with one (sfw|nsfw), (published|unpublished) article, "(.*)" which I created$/) do |sfw, state, title|
  user    = User.find_by(email: 'bob@example.com')
  factory = state == 'published' ? :article_published : :article
  FactoryBot.create factory, title: title, user: user, language: Language.first, sfw: sfw == 'sfw'
end

Given('I create an article with title {string}') do |string|
  visit '/member/articles'
  click_on 'New Article'

  within('#new_article') do
    fill_in 'Title', with: string
    fill_in 'Excerpt', with: 'An introduction'
    fill_in 'Content', with: 'Some content'
    select 'Français', from: 'article[language_id]'
    select 'Creative Commons by', from: 'article[license_id]'
  end

  click_on 'Save'
end

Given('I change article {string} to {string}') do |title, new_title|
  visit 'member/articles'

  find('.articles-list .article', text: title).click_on 'Edit'

  within('.edit_article') do
    fill_in 'Title', with: new_title
    fill_in 'Excerpt', with: 'Some new introduction'
    fill_in 'Content', with: 'Some new content'
  end

  click_on 'Save'
end
