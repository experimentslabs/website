Given(/^a website with a(?:n?) "(.*)" license[s]?$/) do |name|
  FactoryBot.create :license, name: name
end

Given('I change a license to {string}') do |name|
  visit '/admin/licenses'

  within('.license:first-child') do
    click_on 'Edit'
  end

  fill_in 'Name', with: name

  click_on 'Save'
end

Then('The license should now have the {string} name') do |name|
  visit '/admin/licenses'

  within('.license:first-child') do
    expect(current_scope).to have_content(name)
  end
end

When('I create the {string} license') do |name|
  visit '/admin/licenses'
  click_on 'New License'

  fill_in 'Name', with: name
  fill_in 'URL', with: 'http://some-url.com'
  fill_in 'Icon', with: 'an-icon'

  click_on 'Save'
end

Then('I should see the {string} license in the list') do |name|
  visit '/admin/licenses'

  within('.licenses-list') do
    expect(current_scope).to have_content(name)
  end
end

When('I delete the {string} license') do |name|
  visit '/admin/licenses'
  within('.licenses-list') do
    find('.license', text: name).click_on 'Destroy'
  end
end

Then('I should see that the {string} license is not present in the list') do |name|
  visit('/admin/licenses')
  within('.licenses-list') do
    expect(current_scope).not_to have_content(name)
  end
end
