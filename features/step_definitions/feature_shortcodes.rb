When(/^I create a note with a shortcode to an? (safe|unsafe), (published|unpublished), (locked|unlocked) (inline article|article)$/) do |safe, published, locked, widget_type|
  article = FactoryBot.create :article, sfw: safe == 'safe', published: published == 'published', locked: locked == 'locked'

  FactoryBot.create :note_published_and_safe, content: "[article#{widget_type == 'article' ? '-card' : ''}:#{article.slug}]"
end

When(/^I create a note with (a|an inline) shortcode to itself$/) do |widget_type|
  note = FactoryBot.create :note_published_and_safe
  note.update(content: "[note#{widget_type == 'a' ? '-card' : ''}:#{note.slug}]")
end

When(/^I create a note with shortcodes to an article (with|with inline) shortcodes$/) do |widget_type|
  user    = FactoryBot.create :user_active
  article = FactoryBot.create :article_published_and_safe, excerpt: "[user#{widget_type == 'with' ? '-card' : ''}:#{user.username}]"
  FactoryBot.create :note_published_and_safe, content: "[article-card:#{article.slug}]"
end

# rubocop:disable Metrics/BlockLength
Then(/^I should see the "?(.*)"? widget in the note$/) do |widget|
  widget_css_class = case widget
                     when 'article'
                       '.article-card'
                     when 'inline article'
                       '.article--inline'
                     when 'not found"'
                       '.missing.missing--large'
                     when 'not found" inline'
                       '.missing.missing--inline'
                     when 'NSFW'
                       '.nsfw'
                     when 'NSFW inline'
                       '.nsfw.nsfw--inline'
                     when 'infinite loop"'
                       '.infinite-loop.infinite-loop'
                     when 'infinite loop" inline'
                       '.infinite-loop.infinite-loop--inline'
                     when 'display full element"'
                       '.open-to-see'
                     when 'user inline'
                       '.user.user--inline'
                     end

  note = Note.last
  visit "/notes/#{note.slug}"

  within('.note.note--full .content > article') do
    expect(all(widget_css_class).count).to eq(1)
  end
end
# rubocop:enable Metrics/BlockLength
