Given('I am an administrator') do
  visit('/')
  step('I close my session') if page.has_link?('Log out')

  click_on 'Login'

  password = 'password'
  @user = FactoryBot.create :user_admin_active, password: password

  within('#new_user') do
    fill_in 'Email', with: @user.email
    fill_in 'Password', with: password
    click_button 'Log in'
  end

  expect(page).to have_css '.page__menu .fas.fa-admin.fa-fw'
end

Given(/^I am a member(?: with email "(.+)")?$/) do |email|
  email ||= 'user@example.com'
  username = 'ghost'
  step "I am a member with username \"#{username}\" and email \"#{email}\""
end

Given('I am a member with username {string} and email {string}') do |username, email|
  visit('/')
  step('I close my session') if page.has_link?('Log out')

  click_on 'Login'

  password = 'password'
  @user = FactoryBot.create :user_active, username: username, email: email, password: password

  within('#new_user') do
    fill_in 'Email', with: email
    fill_in 'Password', with: password
    click_button 'Log in'
  end

  expect(page).to have_link 'Log out'
end

Given('I close my session') do
  click_on 'Log out'
end

Given('I am not logged in') do
  visit('/')
  step('I close my session') if page.has_link?('Log out')
end

Given(/^I login (?:again )as "(.+)" with password "(.+)"$/) do |email, password|
  visit('/')
  step('I close my session') if page.has_link?('Log out')
  click_on 'Login'

  within('#new_user') do
    fill_in 'Email', with: email
    fill_in 'Password', with: password
    click_button 'Log in'
  end
end
