# Content created by members
# Matches:
#  - a website with 2 articles
#  - a website with 1 project created by other members
Given(/^a website with (\d) (?:(album|article|link|note|project|upload))[s?](?: created by other members)?$/) do |amount, type|
  language  = Language.first
  arguments = if type == 'article'
                { published_at: Time.new.getlocal, published: true, language: language }
              else
                { language: language, published: true }
              end
  FactoryBot.create_list type.to_sym, amount.to_i, arguments
end

Given('a website with {int} safe, published elements of every type with language {string}, {string} license and {string} tag') do |amount, iso_code, license_name, tag_name|
  language = Language.find_by(iso639_1: iso_code)
  license  = License.find_by(name: license_name)
  tag      = Tag.find_by(name: tag_name)

  %w[album article note project upload link].each do |model|
    params = { language: language, tags: [tag], sfw: true }
    params[:license] = license if model.classify.to_s.constantize.has_attribute?('license_id')
    FactoryBot.create_list "#{model}_published", amount, params
  end
end

# Content deletion
When(/^I delete the (\w*) containing "(.*)" via the (admin|member|public) zone/) do |type, title, scope|
  prefix = %w[admin member].include?(scope) ? "/#{scope}" : ''

  plural = type.pluralize
  visit "#{prefix}/#{plural}"

  find(".#{plural}-list .#{type}", text: title).click_on 'Destroy'
end

# Deletion verification
Then(/^I should not see the (\w*) containing "(.*)" in the (admin|member|public) zone/) do |type, title, scope|
  prefix = %w[admin member].include?(scope) ? "/#{scope}" : ''

  visit "#{prefix}/#{type.pluralize}"

  expect(page).not_to have_content(title)
end

# Creation verification
Then(/^I should see the (\w*) containing "(.*)" in the (admin|member|public) zone/) do |type, title, scope|
  prefix = %w[admin member].include?(scope) ? "/#{scope}" : ''

  visit "#{prefix}/#{type.pluralize}"

  expect(page).to have_content(title)
end

Then(/^I should see a list of (\d+) (.*[^s])s?$/) do |amount, type|
  expect(all(".#{type}").count).to eq amount
end

Then('I should see {int} element of every type') do |amount|
  %w[album article note project upload].each do |type|
    expect(all(".#{type}").count).to eq(amount)
  end
end

Then('I should see {int} unsafe elements') do |amount|
  expect(all('.nsfw').count).to eq(amount)
end

Then('I should see admin generic information for every {string} element') do |type|
  within ".#{type}s-list thead > tr" do
    expect(current_scope).to have_content('SFW')
    expect(current_scope).to have_content('Pub.')
    expect(current_scope).to have_content('Lock.')
    expect(current_scope).to have_content('Skip hist.')
    expect(current_scope).to have_content('User')
    expect(current_scope).to have_content('License') unless type == 'link'
    expect(current_scope).to have_content('Language')
    expect(current_scope).to have_content('Tags')
    expect(current_scope).to have_content('Projects') unless %w[project link].include? type
  end
  within ".#{type}:first-child" do
    expect(current_scope).to have_link('View online') unless ['link'].include? type
    expect(current_scope).to have_link('Destroy')
  end
end

Then('I should see member generic information for every {string} element') do |type|
  within ".#{type}s-list thead > tr" do
    expect(current_scope).to have_content('SFW')
    expect(current_scope).to have_content('Pub.')
    expect(current_scope).to have_content('Lock.')
    expect(current_scope).to have_content('Skip hist.')
    expect(current_scope).to have_content('License') unless ['link'].include? type
    expect(current_scope).to have_content('Language')
    expect(current_scope).to have_content('Tags')
    expect(current_scope).to have_content('Projects') unless %w[project link].include? type
  end
  within ".#{type}:first-child" do
    expect(current_scope).to have_css('a[title="View online"]') unless ['link'].include? type
    expect(current_scope).to have_link('Edit')
    expect(current_scope).to have_link('Destroy')
  end
end

Then('I should see public generic information for every {string} element') do |type|
  within find_all(".#{type}").first do
    expect(current_scope).to have_content('SFW:')
    expect(current_scope).to have_content('by')
    expect(current_scope).to have_content('License:') unless ['link'].include? type
    expect(current_scope).to have_content('Language:')
    expect(current_scope).to have_content('Tags:')
    expect(current_scope).to have_content('Projects:') unless %w[project link].include? type
    expect(current_scope).to have_css('h3 a', count: 1)
    expect(current_scope).not_to have_link('Edit')
    expect(current_scope).not_to have_link('Destroy')
  end
end

Given('a website with all content published and created by {string}, written in one language for one license and tag') do |email|
  user     = User.find_by!(email: email)
  language = FactoryBot.create :language
  license  = FactoryBot.create :license
  tag      = FactoryBot.create :tag
  project  = FactoryBot.create :project_published_and_safe, user: user, language: language, license: license, tags: [tag]
  album    = FactoryBot.create :album_published_and_safe, user: user, language: language, license: license, tags: [tag], projects: [project]

  FactoryBot.create :article_published_and_safe, user: user, language: language, license: license, tags: [tag], projects: [project]
  FactoryBot.create :link_published_and_safe, user: user, language: language, tags: [tag]
  FactoryBot.create :note_published_and_safe, user: user, language: language, license: license, tags: [tag], projects: [project]
  FactoryBot.create :upload_published_and_safe, user: user, language: language, license: license, tags: [tag], projects: [project], albums: [album]
end

Given(/^I deleted all the ((?:[a-z_]*(?:, )?)+) of "(.*)"$/) do |types, email|
  user = User.find_by email: email

  types.split(/, */).each do |t|
    user.send(t).destroy_all
  end
end

Given(/^I unpublish all the ((?:[a-z_]*(?:, )?)+) of "(.*)"$/) do |types, email|
  user = User.find_by email: email

  types.split(/, */).each do |t|
    user.send(t).each { |record| record.update published: false }
  end
end

Given(/^I lock all the ((?:[a-z_]*(?:, )?)+) of "(.*)"$/) do |types, email|
  user = User.find_by email: email

  types.split(/, */).each do |t|
    user.send(t).each { |record| record.update locked: true }
  end
end

Then('I should see the {string} details') do |type|
  # rubocop:disable Style/WordArray
  hidden_fields = ['Name', 'Title', 'Hidden in history', 'Locked', 'Created at']
  common_fields = ['SFW', 'Language', 'License', 'Tags', 'Updated at', 'Published at']
  fields        = {
    album:   ['Author', 'Uploads', 'Projects', 'Uploads'],
    article: ['Author', 'Projects'],
    note:    ['Author', 'Projects'],
    project: ['Author', 'Albums:', 'Articles:', 'Notes:', 'Uploads:'],
    upload:  ['Author', 'Projects', 'Albums', 'Download'],
  }
  # rubocop:enable Style/WordArray

  within(".#{type}") do
    elements = fields[type.to_sym] + common_fields
    elements.each do |e|
      expect(current_scope).to have_content(e)
    end
    hidden_fields.each do |e|
      expect(current_scope).not_to have_content(e)
    end
  end
end

Then('I should see the first {string} related to {string}') do |related_types, type|
  types = related_types.split(/, */)

  within(".#{type}--full") do
    types.each do |related_type|
      expect(find(".tabs__tab-content.#{related_type}-list")).to have_content('Show all')
    end
  end
end

When(/^I publish (album|article|link|note|project|upload) "(.*)"$/) do |type, title|
  find(".#{type}", text: title).click_on 'Publish'
end

When(/^I unpublish (album|article|link|note|project|upload) "(.*)"$/) do |type, title|
  find(".#{type}", text: title).click_on 'Unpublish'
end

Given(/^I set the "(.*)" (album|article|link|note|project|upload) as nsfw content$/) do |title, type|
  step "I visit the member #{type} page"

  find(".#{type}", text: title).click_on 'Edit'

  uncheck("#{type}_sfw")

  click_on 'Save'
end

Then(/^I should see only a warning on the "(.*)" (album|article|link|note|project|upload) public page$/) do |title, type|
  step "I visit the public #{type} page"
  expect(page).not_to have_content(title)

  click_on 'Show NSFW'
  step "I visit the \"#{title}\" public #{type} page"
  expect(page).to have_content(title)

  click_on 'Hide NSFW'
  expect(page).to have_content('Content not displayed')
end

Then(/^I should see only a warning on the (albums|articles|links|notes|projects|uploads) public page$/) do |type|
  visit "/#{type}"
  expect(page).to have_content('This content might not be appropriate for every situation.')
end

Given(/^I lock the "(.*)" (album|article|link|note|project|upload)$/) do |title, type|
  step "I visit the admin #{type.pluralize} page"

  find(".#{type}", text: title).click_on 'Lock'

  expect(page).to have_link 'Unlock'
end

Then(/^The "(.*)" link should be disabled for the "(.*)" (album|article|link|note|project|upload)$/) do |link_title, title, type|
  step "I visit the admin #{type.pluralize} page"
  within(".#{type}", text: title) do
    expect(current_scope).to have_css("a[title=\"#{link_title}\"][disabled=disabled]")
  end
end

Then(/^I should not be able to see the public "(.*)" (album|article|link|note|project|upload) manually$/) do |title, type|
  field  = case type
           when 'album'
             'name'
           when 'article'
             'title'
           when 'note'
             'content'
           when 'project'
             'name'
           when 'upload'
             'title'
           end
  entity = type.capitalize.to_s.constantize.find_by(field => title)
  visit "/#{type.pluralize}/#{entity.slug}"
  expect(page).to have_content('This content needs review and have been retired for now.')
end

Then(/^I should not see the "(.*)" in the public (album|article|link|note|project|upload)s? list$/) do |title, type|
  step "I visit the public #{type.pluralize} page"
  expect(page).not_to have_css(".#{type}", text: title)
end

Given(/^the first (album|article|link|note|project|upload) has been updated$/) do |type|
  field = if %w[album project].include? type
            'name'
          elsif type == 'note'
            'content'
          else
            'title'
          end
  type.capitalize.to_s.constantize.first.update(field => 'A new value')
end

Given(/^the first (album|article|link|note|project|upload) has been (locked|unlocked)$/) do |type, action|
  type.capitalize.to_s.constantize.first.update(locked: action == 'locked')
end

Given(/^the first (album|article|link|note|project|upload) has been (removed from publication|published)/) do |type, action|
  type.capitalize.to_s.constantize.first.update(published: action == 'published')
end

Given(/^I do a minor update on (album|article|link|note|project|upload) "(.*)", changing title to "(.*)"$/) do |type, title, new_content|
  visit "member/#{type.pluralize}"
  find(".#{type.pluralize}-list .#{type}", text: title).click_on 'Edit'

  field = 'Name' if %w[album project].include? type
  field ||= 'Title' if %w[article upload link].include? type
  field ||= 'Content' if %w[note].include? type

  fill_in field, with: new_content
  check 'This is a minor edition'

  click_on 'Save'
end

Given(/^I update the (album|article|link|note|project|upload) "(.*)" title to "(.*)" in order to "(.*)"$/) do |type, title, new_title, reason|
  visit "member/#{type.pluralize}"
  find(".#{type.pluralize}-list .#{type}", text: title).click_on 'Edit'

  field = 'Name' if %w[album project].include? type
  field ||= 'Title' if %w[article upload link].include? type
  field ||= 'Content' if %w[note].include? type

  fill_in field, with: new_title
  fill_in 'Update message', with: reason

  click_on 'Save'
end
