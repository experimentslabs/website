Given(/^a website with a(?:n?) "(.*)" language for content in "(.*)"$/) do |code, name|
  FactoryBot.create :language, iso639_1: code, name: name
end

Given('I change a language to {string}, {string}') do |code, name|
  visit '/admin/languages'

  within('.language:first-child') do
    click_on 'Edit'
  end

  fill_in 'Iso639 1', with: code
  fill_in 'Name', with: name

  click_on 'Save'
end

Then('The language should now have the {string}, {string} values') do |code, name|
  visit '/admin/languages'

  within('.language:first-child') do
    expect(current_scope).to have_content(code)
    expect(current_scope).to have_content(name)
  end
end

When('I create the {string} language with {string} code') do |name, code|
  visit '/admin/languages'
  click_on 'New Language'

  fill_in 'Iso639 1', with: code
  fill_in 'Name', with: name

  click_on 'Save'
end

Then('I should see the {string}, {string} language in the list') do |code, name|
  visit '/admin/languages'

  within('.languages-list') do
    expect(current_scope).to have_content(code)
    expect(current_scope).to have_content(name)
  end
end

When('I delete the {string} language') do |string|
  visit '/admin/languages'
  within('.languages-list') do
    find('.language', text: string).click_on 'Destroy'
  end
end

Then('I should see that the {string} language is not present in the list') do |name|
  visit('/admin/languages')
  within('.languages-list') do
    expect(current_scope).not_to have_content(name)
  end
end
