# Matches "I visit the login page"
#         "I visit the public login page"
#         "I visit the public login page again"
#         "I visit the languages page"
#         "I visit the admin languages page"
#         "I visit the "English" public language page"
#         "I visit the "English" language page again"
# ...
#                      | title    | scope                  | page
When(/^I visit the (?:"(.*)" )?(?:(admin|public|member)? )?(.*[^s])s? page(?: again)?$/) do |title, scope, page|
  prefix = %w[admin member].include?(scope) ? "/#{scope}" : ''

  url = case page
        when 'home'
          '/'
        when 'activity'
          '/activity'
        when 'login'
          "#{prefix}/users/sign_in"
        else
          "#{prefix}/#{page.pluralize}"
        end

  visit url

  find(".#{page}s-list .#{page}", text: title).click_on title if title && title != 'first' && page != 'note'
  if title && title != 'first' && page == 'note'
    within(".#{page}s-list .#{page}", text: title) do
      find('header h3 a').click
    end
  end
  find_all(".#{page}s-list .#{page} .title a").first.click if title == 'first'
end

When(/^I manually visit the first (album|article|link|note|project|upload)$/) do |type|
  entity = type.classify.to_s.constantize.first
  visit "/#{type.pluralize}/#{entity.slug}"
end

When(/^I visit the member form to create a new (album|article|link|note|project|upload)$/) do |type|
  visit "/member/#{type}s/new"
end

Then(/^I should be able to visit the (album|article|link|note|project|upload) page for "(.*)"$/) do |type, title|
  step "I visit the \"#{title}\" public #{type} page"

  expect(page).to have_content(title)
end

Then(/^I should not be able to visit the (album|article|link|note|project|upload) page for "(.*)"$/) do |type, title|
  step "I visit the public #{type} page"
  expect(page).not_to have_content(title)

  # Find the entity and try to access it directly
  key    = if %w[album project].include? type
             'name'
           elsif type == 'note'
             'content'
           else
             'title'
           end
  entity = type.capitalize.to_s.constantize.find_by(key => title)
  expect do
    visit("/#{type.pluralize}/#{entity.id}").to raise_error(ActiveRecord::RecordNotFound)
  end
end

When(/^I click on the link to display all the associated (albums|articles|links|notes|projects|uploads)$/) do |type|
  within("#tab-content-#{type} .actions") do
    click_on 'Show all'
  end
end

When('I click on {string}') do |text|
  click_on text
end
