Given(/^a website with one (sfw|nsfw), (published|unpublished) album titled "(.*)"$/) do |sfw, state, title|
  factory = state == 'published' ? :album_published : :album
  FactoryBot.create factory, name: title, language: Language.first, sfw: sfw == 'sfw'
end

Given(/^a website with one (sfw|nsfw), (published|unpublished) album, "(.*)" which I created$/) do |sfw, state, title|
  user    = User.find_by(email: 'bob@example.com')
  factory = state == 'published' ? :album_published : :album
  FactoryBot.create factory, name: title, user: user, language: Language.first, sfw: sfw == 'sfw'
end

Given('I create an album with title {string}') do |name|
  visit '/member/albums'
  click_on 'New Album'

  within('#new_album') do
    fill_in 'Name', with: name
    fill_in 'Description', with: 'Description here'
    select 'Français', from: 'album[language_id]'
    select 'Creative Commons by', from: 'album[license_id]'
  end

  click_on 'Save'
end

Given('I change album {string} to {string}') do |title, new_name|
  visit 'member/albums'

  find('.albums-list .album', text: title).click_on 'Edit'

  within('.edit_album') do
    fill_in 'Name', with: new_name
    fill_in 'Description', with: 'Some new content'
  end

  click_on 'Save'
end
