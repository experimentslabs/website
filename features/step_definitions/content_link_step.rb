Given(/^a website with one (sfw|nsfw), (published|unpublished) link titled "(.*)"$/) do |sfw, state, title|
  factory = state == 'published' ? :link_published : :upload
  FactoryBot.create factory, title: title, language: Language.first, sfw: sfw == 'sfw'
end

Given(/^a website with one (sfw|nsfw), (published|unpublished) link, "(.*)" which I created$/) do |sfw, state, title|
  user    = User.find_by(email: 'bob@example.com')
  factory = state == 'published' ? :link_published : :link
  FactoryBot.create factory, title: title, user: user, language: Language.first, sfw: sfw == 'sfw'
end

Given('I create a link to {string}, titled {string}') do |url, title|
  visit '/member/links'
  click_on 'New link'

  within('#new_link') do
    fill_in 'Title', with: title
    fill_in 'URL', with: url
    select 'Français', from: 'link[language_id]'
  end

  click_on 'Save'
end

Given('I change link {string} to {string}') do |content, new_content|
  visit 'member/links'

  find('.links-list .link', text: content).click_on 'Edit'

  within('.edit_link') do
    fill_in 'Title', with: new_content
  end

  click_on 'Save'
end
