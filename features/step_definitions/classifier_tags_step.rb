Given('a website with a {string} tag') do |name|
  FactoryBot.create :tag, name: name
end

Given('I change a tag to {string}') do |name|
  visit '/admin/tags'

  within('.tag:first-child') do
    click_on 'Edit'
  end

  fill_in 'Name', with: name

  click_on 'Save'
end

Then('The tag should now have the {string} name') do |name|
  visit '/admin/tags'

  within('.tag:first-child') do
    expect(current_scope).to have_content(name)
  end
end

When('I delete the {string} tag') do |name|
  visit '/admin/tags'
  within('.tags-list') do
    find('.tag', text: name).click_on 'Destroy'
  end
end

Then('I should see that the {string} tag is not present in the list') do |name|
  visit('/admin/tags')
  within('.tags-list') do
    expect(current_scope).not_to have_content(name)
  end
end

And('I assign the {string} tag to all content') do |name|
  tag = Tag.find_by name: name

  %w[Album Article Link Note Project Upload].each do |model|
    model.to_s.constantize.all.each { |e| e.update tags: [tag] }
  end
end

And('I remove the tags of all content') do
  %w[Album Article Link Note Project Upload].each do |model|
    model.to_s.constantize.all.each { |e| e.update tags: [] }
  end
end

Then('the tag should have 1 associated content') do
  %w[Albums Articles Links Notes Projects Uploads].each do |model|
    expect(page).to have_content "#{model}: 1"
  end
end

Then('the tag should have no associated content') do
  %w[Albums Articles Links Notes Projects Uploads].each do |model|
    expect(page).to have_content "#{model}: 0"
  end
end
