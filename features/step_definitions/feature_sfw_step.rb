Then(/^I should see (a|one|\d+) warning[s]? about NSFW content$/) do |amount|
  amount = if %w[a one].include? amount
             1
           else
             amount
           end
  expect(page).to have_css('.nsfw', count: amount)
end

When('I toggle the SFW display option') do
  if page.has_link?('Show NSFW')
    click_on 'Show NSFW'
  else
    click_on 'Hide NSFW'
  end
end

Given('I toggle the SFW display option to display NSFW content') do
  click_on 'Show NSFW' if page.has_link?('Show NSFW')
end

Given('I toggle the SFW display option to hide NSFW content') do
  click_on 'Hide NSFW' if page.has_link?('Hide NSFW')
end

Then(/^I should see the unsafe (album|article|link|note|project|upload)/) do |type|
  expect(page).to have_css(".#{type}")
  expect(page).to have_css('.fa-is-nsfw')
end
