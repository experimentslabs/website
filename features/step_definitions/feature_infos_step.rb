Given('I change my infos with name {string} and bio {string}') do |real_name, bio|
  click_on 'Infos'

  fill_in 'Real name', with: real_name
  fill_in 'Biography', with: bio

  click_on 'Save'
end

When('I change my avatar to {string}') do |file_name|
  click_on 'Infos'

  file = File.realpath File.join(File.expand_path('../../spec/fixtures/files', __dir__), file_name)
  attach_file 'Avatar', file

  click_on 'Save'
end
