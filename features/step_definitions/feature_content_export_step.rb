Then('a markdown file should be presented to me') do
  expect(page.response_headers['Content-Type']).to eq 'text/markdown; charset=utf-8'
end
