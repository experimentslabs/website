Then(/^I should see (\d+) (update|lock|unlock|unpublish) action in the list$/) do |amount, action|
  expect(all(".act-status--#{action}").count).to eq(amount)
end

Then(/^I should not see an act for (album|article|link|note|project|upload) "(.*)" being updated$/) do |type, title|
  field = :name if %w[album project].include? type
  field ||= :title if %w[article upload].include? type
  field ||= :content if %w[note].include? type
  entity = type.classify.to_s.constantize.find_by(field.to_s => title)

  expect(find_all('.acts-list .act')[0]).not_to have_content("\"#{entity.title_to_display}\" has been updated")
end
Then(/^I should see an act for (album|article|link|note|project|upload) "(.*)" being updated to "(.*)"$/) do |type, title, reason|
  field = :name if %w[album project].include? type
  field ||= :title if %w[article upload link].include? type
  field ||= :content if %w[note].include? type
  entity = type.classify.to_s.constantize.find_by(field.to_s => title)

  expect(find_all('.acts-list .act')[0]).to have_content("\"#{entity.title_to_display}\" has been updated")
  expect(find_all('.acts-list .act')[0]).to have_content(reason)
end
