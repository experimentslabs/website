Feature: Activities
  As a maintainer
  In order to see the recent activity on the website
  I want to track the activity on the website content

  Scenario: Activity list
    Given I am not logged in
    And a website with 1 random safe, unlocked, published elements of every type
    And a website with 1 random unsafe, unlocked, published elements of every type
    When I visit the activity page
    Then I should see 1 element of every type
    Then I should see 6 unsafe elements

  Scenario Outline: Activity list with updates
    Given I am a member with email "annelise@tools-maker.lan"
    And a website with <amount> safe, published elements of every type created by "annelise@tools-maker.lan"
    And the first <singular_type> has been updated
    When I visit the activity page
    Then I should see 1 update action in the list

    Examples:
      | amount | singular_type |
      | 1      | album         |
      | 1      | article       |
      | 1      | note          |
      | 1      | project       |
      | 1      | upload        |

  Scenario Outline: Activity list with locks and unlocks
    Given I am a member with email "annelise@tools-maker.lan"
    And a website with <amount> safe, published elements of every type created by "annelise@tools-maker.lan"
    And the first <singular_type> has been locked
    And the first <singular_type> has been unlocked
    When I visit the activity page
    Then I should see <amount_locked> lock action in the list
    Then I should see <amount_unlocked> unlock action in the list

    Examples:
      | amount | amount_locked | amount_unlocked | singular_type |
      | 1      | 1             | 1               | album         |
      | 1      | 1             | 1               | article       |
      | 1      | 1             | 1               | note          |
      | 1      | 1             | 1               | project       |
      | 1      | 1             | 1               | upload        |

  Scenario Outline: Activity list with un/publish
    Given I am a member with email "annelise@tools-maker.lan"
    And a website with <amount> safe, published elements of every type created by "annelise@tools-maker.lan"
    And the first <singular_type> has been removed from publication
    And the first <singular_type> has been published
    When I visit the activity page
    Then I should see 1 unpublish action in the list

    Examples:
      | amount | singular_type |
      | 1      | album         |
      | 1      | article       |
      | 1      | note          |
      | 1      | project       |
      | 1      | upload        |
