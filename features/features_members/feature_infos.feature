Feature: Update members infos
  As a member
  In order to give some infos about me
  I want to be able to provide a some data

  Background:
    Given I am a member with email "john.do@recovery.com"

  Scenario: Update personal infos
    Given I change my infos with name "John Do" and bio "I like doing music"
    When I visit the "John Do" public users page
    Then I should see "John Do"
    And I should see "I like doing music"

  Scenario: Update avatar with an image
    When I change my avatar to "Woody.jpg"
    Then I should see "Your infos were successfully saved"

  Scenario: Update avatar with a text file
    When I change my avatar to "lorem.txt"
    Then I should see "Avatar needs to be a "png" or "jpg" image"

