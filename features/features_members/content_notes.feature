Feature: note management
  As a member
  In order to manage the content I publish
  I want to be able to create or update the note's content

  Background:
    Given I am a member with email "bob@example.com"
    And a website with a "Creative Commons by" license
    And a website with a "fr" language for content in "Français"
    And a website with one sfw, published note, "My first note" which I created

  Scenario: Create note
    Given I create an note containing "Extra links I found today"
    Then I should see the note containing "Extra links I found today" in the member zone
    And I should see a list of 2 notes

  Scenario: Update note
    Given I change note "My first note" to "Dead links"
    Then I should see the note containing "Dead links" in the member zone
    And I should see a list of 1 note
