Feature: Generic content management
  As a member
  In order to manage the content I publish
  I want to be able to list, delete and change the SFW status of it

  Background:
    Given I am a member with email "bob@example.com"
    And a website with a "fr" language for content in "Français"
    And a website with a "VueJS" tag
    And a website with a "GPL 3.0" license
    And a website with one sfw, published album, "My first album" which I created
    And a website with one sfw, published article, "My first article" which I created
    And a website with one sfw, published link, "My first link" which I created
    And a website with one sfw, published note, "My first note" which I created
    And a website with one sfw, published project, "My first project" which I created
    And a website with one sfw, published upload, "My first upload" which I created
    And a website with one sfw, unpublished album, "My second album" which I created
    And a website with one sfw, unpublished article, "My second article" which I created
    And a website with one sfw, unpublished link, "My second link" which I created
    And a website with one sfw, unpublished note, "My second note" which I created
    And a website with one sfw, unpublished project, "My second project" which I created
    And a website with one sfw, unpublished upload, "My second upload" which I created
    And a website with 2 albums created by other members
    And a website with 2 articles created by other members
    And a website with 2 links created by other members
    And a website with 2 notes created by other members
    And a website with 2 projects created by other members
    And a website with 2 uploads created by other members

  Scenario Outline: Member display content
    Given I visit the member <plural_type> page
    Then I should see a list of 2 <plural_type>
    Then I should see member generic information for every "<singular_type>" element

    Examples:
      | singular_type | plural_type |
      | album         | albums      |
      | article       | articles    |
      | link          | links       |
      | upload        | uploads     |
      | note          | notes       |
      | project       | projects    |

  Scenario Outline: Delete content
    When I delete the <singular_type> containing "<title>" via the member zone
    Then I should not see the <singular_type> containing "<title>" in the member zone

    Examples:
      | title            | singular_type |
      | My first album   | album         |
      | My first article | article       |
      | My first link    | link          |
      | My first upload  | upload        |
      | My first note    | note          |
      | My first project | project       |

  Scenario Outline: Publish content
    Given I visit the member <plural_type> page
    When I publish <singular_type> "<title>"
    Then I should be able to visit the <singular_type> page for "<title>"

    Examples:
      | title             | plural_type | singular_type |
      | My second album   | albums      | album         |
      | My second article | articles    | article       |
      | My second upload  | uploads     | upload        |
      | My second note    | notes       | note          |
      | My second project | projects    | project       |

  Scenario Outline: Unpublish content
    Given I visit the member <plural_type> page
    When I unpublish <singular_type> "<title>"
    Then I should not be able to visit the <singular_type> page for "<title>"

    Examples:
      | title            | plural_type | singular_type |
      | My first album   | albums      | album         |
      | My first article | articles    | article       |
      | My first note    | notes       | note          |
      | My first project | projects    | project       |
      | My first upload  | uploads     | upload        |

  Scenario Outline: Set element SFW
    Given I set the "<title>" <singular_type> as nsfw content
    Then I should see only a warning on the "<title>" <singular_type> public page

    Examples:
      | title            | singular_type |
      | My first album   | album         |
      | My first article | article       |
      | My first note    | note          |
      | My first project | project       |
      | My first upload  | upload        |

  Scenario Outline: Do a minor update don't create acts
    Given I do a minor update on <singular_type> "<title>", changing title to "<new_title>"
    When I visit the activity page
    Then I should not see an act for <singular_type> "<new_title>" being updated

    Examples:
      | title            | new_title                  | singular_type |
      | My first album   | My wonderful first album   | album         |
      | My first article | My wonderful first article | article       |
      | My first note    | My wonderful first note    | note          |
      | My first project | My wonderful first project | project       |
      | My first upload  | My wonderful first upload  | upload        |

  Scenario Outline: Do an update for a reason saves the reason
    Given I update the <singular_type> "<title>" title to "<new_title>" in order to "correct the title"
    When I visit the activity page
    Then I should see an act for <singular_type> "<new_title>" being updated to "correct the title"

    Examples:
      | title            | new_title                  | singular_type |
      | My first album   | My wonderful first album   | album         |
      | My first article | My wonderful first article | article       |
      | My first note    | My wonderful first note    | note          |
      | My first project | My wonderful first project | project       |
      | My first upload  | My wonderful first upload  | upload        |
