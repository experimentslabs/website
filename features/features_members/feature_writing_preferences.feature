Feature: writing preferences
  As a member
  In order to have a good experience
  I want to be able to save my writing preferences regarding the website

  Background:
    Given I am a member with email "john.do@recovery.com"
    Given a website with an "pi" language for content in "Pirate"
    And a website with an "pa" language for content in "Palladin"
    Given a website with an "Love license" license
    And a website with an "WTFPL license" license

  Scenario Outline: Change the default writing language

    And I set the writing language preference to "Pirate"
    When I visit the member form to create a new <content_type>
    Then the selected language should be "Pirate"

    Examples:
      | content_type |
      | album        |
      | article      |
      | note         |
      | project      |
      | upload       |

  Scenario Outline: Change the default writing license
    And I set the writing license preference to "WTFPL license"
    When I visit the member form to create a new <content_type>
    Then the selected license should be "WTFPL license"

    Examples:
      | content_type |
      | album        |
      | article      |
      | note         |
      | project      |
      | upload       |
