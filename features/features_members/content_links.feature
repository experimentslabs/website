Feature: link management
  As a member
  In order to manage the links I publish
  I want to be able to create or update their content

  Background:
    Given I am a member with email "bob@example.com"
    And a website with a "Creative Commons by" license
    And a website with a "fr" language for content in "Français"
    And a website with one sfw, published link, "My first link" which I created
    And a website with one sfw, unpublished link, "My second link" which I created

  Scenario: Create link
    Given I create a link to "https://experimentslabs.com", titled "ExperimentsLabs"
    Then I should see the link containing "ExperimentsLabs" in the member zone
    And I should see a list of 3 links

  Scenario: Update link
    Given I change link "My first link" to "Random link"
    Then I should see the link containing "Random link" in the member zone
    And I should see a list of 2 links

  # Delete is tested in generic_content

  Scenario: Publish content
    Given I visit the member links page
    And I publish link "My second link"
    When I visit the activity page
    Then I should see "My second link"

  Scenario: Unpublish content
    Given I visit the member links page
    And I unpublish link "My first link"
    When I visit the links page
    Then I should not see "My first link"


  Scenario: Set element SFW
    Given I set the "My first link" link as nsfw content
    Then I should see only a warning on the links public page
