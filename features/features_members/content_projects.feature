Feature: Project management
  As a member
  In order to manage the content I publish
  I want to be able to create or update the project's content

  Background:
    Given I am a member with email "bob@example.com"
    And a website with a "Creative Commons by" license
    And a website with a "fr" language for content in "Français"
    And a website with one sfw, published project, "My first project" which I created

  Scenario: Create project
    Given I create an project with title "Space X"
    Then I should see the project containing "Space X" in the member zone
    And I should see a list of 2 projects

  Scenario: Update project
    Given I change project "My first project" to "World domination"
    Then I should see the project containing "World domination" in the member zone
    And I should see a list of 1 project
