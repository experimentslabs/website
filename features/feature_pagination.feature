Feature: Pagination for users
  As a website maintainer,
  In order to preserve the bandwidth
  I want to paginate index pages which can have a lot of content

  Background:
    Given I am not logged in
    And a website with 20 safe, published elements of every type

  # We want all the licenses and tags
  # Languages are not tested
  Scenario Outline: Check maximum elements on a public page
    When I visit the public <plural_type> page
    Then I should see <amount> <plural_type>

    Examples:
      | plural_type | amount |
      | albums      | 15     |
      | articles    | 15     |
      | licenses    | 20     |
      | notes       | 15     |
      | projects    | 15     |
      | tags        | 20     |
      | uploads     | 15     |
