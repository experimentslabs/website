Feature: Comments
  As an user,
  I order to give feedback to creators
  I want to comment the content

  Background:
    Given a website with an active user "funny-sponge@example.com"
    Given a website with 1 safe, published element of every type created by "funny-sponge@example.com"
    And a website with an active user "chunky-bacon@example.com"

  Scenario Outline: Anonymous comments on content
    Given I am not logged in
    When I visit the "first" public <content_type> page
    And I leave a comment as "John", with email address "john@doe.com"
    Then I should see "Comment was successfully created."
    And I should not see any comment

    Examples:
      | content_type |
      | album        |
      | article      |
      | note         |
      | project      |
      | upload       |

  Scenario Outline: Other users comments on content
    Given I am a member with username "tester" and email "tester@example.com"
    When I visit the "first" public <content_type> page
    And I leave a comment
    Then I should see "Comment was successfully created."
    And I should not see any comment

    Examples:
      | content_type |
      | album        |
      | article      |
      | note         |
      | project      |
      | upload       |

  Scenario Outline: Member comments on content, is visible by Author
    Given I am a member with username "charlotte" and email "charlotte@example.com"
    When I visit the "first" public <content_type> page
    And I leave a comment
    Then I login again as "funny-sponge@example.com" with password "password"
    And I visit the "first" public <content_type> page
    Then I should see the comment posted by "charlotte"

    Examples:
      | content_type |
      | album        |
      | article      |
      | note         |
      | project      |
      | upload       |

  Scenario: Simple bot comment
    Given I am not logged in
    When I visit the "first" album page
    And I leave a comment, filling the bot field
    Then I should see a success message for bots
