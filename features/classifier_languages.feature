Feature: Classify content by language
  As a website maintainer
  In order to give users a good exploration experience
  I want them to be able to see language on content
  And filter content by language

  Scenario: filter the language list
    Given a website with a "de" language for content in "Deutsch"
    And a website with an active user "philou" with email "philemon@wonderla.nd"
    And a website with all content published and created by "philemon@wonderla.nd", written in one language for one license and tag
    When I visit the public languages page
    Then I should see 1 language
