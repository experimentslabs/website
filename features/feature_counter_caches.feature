Feature: Counter caches
  As a website maintainer
  In order to cache some sql queries
  I want to cache the amounts of entries related to some entities

  Background:
    Given I am an administrator
    And a website with an active user "philou" with email "philemon@wonderla.nd"
    And a website with all content published and created by "philemon@wonderla.nd", written in one language for one license and tag

  Scenario Outline: Check counter caches
    When I visit the "<link>" public <entity_type> page
    Then counters for <counter_fields> should be showing <amount>

    Examples:
      | link   | entity_type | counter_fields                                    | amount |
      | first  | album       | uploads                                           | 1      |
      | first  | language    | albums, articles, notes, projects, uploads, links | 1      |
      | first  | license     | albums, articles, notes, projects, uploads        | 1      |
      | first  | project     | albums, articles, notes, uploads                  | 1      |
      | first  | tag         | albums, articles, notes, projects, uploads, links | 1      |
      | philou | user        | albums, articles, notes, projects, uploads, links | 1      |

  Scenario Outline: Check counter caches when content is unpublished
    Given I unpublish all the articles, notes, uploads, links of "philemon@wonderla.nd"
    When I visit the "<link>" public <entity_type> page
    Then counters for <counter_fields> should be showing <amount>

    Examples:
      | link   | entity_type | counter_fields                  | amount |
      | first  | album       | uploads                         | 0      |
      | first  | language    | articles, notes, uploads, links | 0      |
      | first  | license     | articles, notes, uploads        | 0      |
      | first  | project     | articles, notes, uploads        | 0      |
      | first  | tag         | articles, notes, uploads, links | 0      |
      | philou | user        | articles, notes, uploads, links | 0      |

  Scenario Outline: Check counter caches when content is locked and unpublished
    Given I lock all the articles, notes, uploads, links of "philemon@wonderla.nd"
    Given I unpublish all the articles, notes, uploads, links of "philemon@wonderla.nd"
    When I visit the "<link>" public <entity_type> page
    Then counters for <counter_fields> should be showing <amount>

    Examples:
      | link   | entity_type | counter_fields                  | amount |
      | first  | album       | uploads                         | 0      |
      | first  | language    | articles, notes, uploads, links | 0      |
      | first  | license     | articles, notes, uploads        | 0      |
      | first  | project     | articles, notes, uploads        | 0      |
      | first  | tag         | articles, notes, uploads, links | 0      |
      | philou | user        | articles, notes, uploads, links | 0      |

  Scenario: Check counter caches when tags are changed on content
    Given a website with a 'Testing' tag
    And a website with a 'Fun' tag
    And I assign the 'Testing' tag to all content
    When I visit the "Testing" public tag page
    Then the tag should have 1 associated content
    When I remove the tags of all content
    And I visit the "Testing" public tag page
    Then the tag should have no associated content

  Scenario Outline: Check counter caches when content is locked
    Given I lock all the articles, notes, uploads, links of "philemon@wonderla.nd"
    When I visit the "<link>" public <entity_type> page
    Then counters for <counter_fields> should be showing <amount>

    Examples:
      | link   | entity_type | counter_fields                  | amount |
      | first  | album       | uploads                         | 0      |
      | first  | language    | articles, notes, uploads, links | 0      |
      | first  | license     | articles, notes, uploads        | 0      |
      | first  | project     | articles, notes, uploads        | 0      |
      | first  | tag         | articles, notes, uploads, links | 0      |
      | philou | user        | articles, notes, uploads, links | 0      |

  Scenario Outline: Check counter caches when content is removed
    Given I deleted all the articles, notes, uploads, links of "philemon@wonderla.nd"
    When I visit the "<link>" public <entity_type> page
    Then counters for <counter_fields> should be showing <amount>

    Examples:
      | link   | entity_type | counter_fields                  | amount |
      | first  | album       | uploads                         | 0      |
      | first  | language    | articles, notes, uploads, links | 0      |
      | first  | license     | articles, notes, uploads        | 0      |
      | first  | project     | articles, notes, uploads        | 0      |
      | first  | tag         | articles, notes, uploads, links | 0      |
      | philou | user        | articles, notes, uploads, links | 0      |

  Scenario Outline: Check counter caches when content is removed
    Given I deleted all the albums, projects of "philemon@wonderla.nd"
    When I visit the "<link>" public <entity_type> page
    Then counters for <counter_fields> should be showing <amount>

    Examples:
      | link   | entity_type | counter_fields   | amount |
      | first  | language    | albums, projects | 0      |
      | first  | license     | albums, projects | 0      |
      | first  | tag         | albums, projects | 0      |
      | philou | user        | albums, projects | 0      |

  Scenario Outline: Check counter caches when content is unpublished
    Given I unpublish all the albums, projects of "philemon@wonderla.nd"
    When I visit the "<link>" public <entity_type> page
    Then counters for <counter_fields> should be showing <amount>

    Examples:
      | link   | entity_type | counter_fields   | amount |
      | first  | language    | albums, projects | 0      |
      | first  | license     | albums, projects | 0      |
      | first  | tag         | albums, projects | 0      |
      | philou | user        | albums, projects | 0      |


  Scenario Outline: Check counter caches when content is locked
    Given I lock all the albums, projects of "philemon@wonderla.nd"
    When I visit the "<link>" public <entity_type> page
    Then counters for <counter_fields> should be showing <amount>

    Examples:
      | link   | entity_type | counter_fields   | amount |
      | first  | language    | albums, projects | 0      |
      | first  | license     | albums, projects | 0      |
      | first  | tag         | albums, projects | 0      |
      | philou | user        | albums, projects | 0      |
