Feature: Safe for work content
  As a maintainer
  In order to keep visitors safe
  I want to be able to flag content as SFW/NSFW

  Background:
    Given I am not logged in
    And a website with 1 unsafe, published element of every type

  Scenario: Visitors can see a SFW/NSFW toogle button
    Then I can see a "Show NSFW" link

  Scenario: Visitors can toggle SFW/NSFW content
    Given I visit the public albums page
    Then I should see a warning about NSFW content
    When I toggle the SFW display option
    Then I should see the unsafe album

  Scenario Outline: Unsafe content is hidden
    Given I visit the public <content_type> page
    Then I should see 1 warning about NSFW content
    Given I manually visit the first <content_type>
    Then I should see a warning about NSFW content

    Examples:
      | content_type |
      | album        |
      | article      |
      | note         |
      | project      |
      | upload       |
