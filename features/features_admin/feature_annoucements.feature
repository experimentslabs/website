Feature: Announcements
  As a website maintainer
  In order to inform visitors about events on the website
  I want to be able to publish some text

  Background:
    Given I am an administrator

  Scenario: Announcements for everyone
    When I create a global announcement
    And I close my session
    Then I should see the announcement on the homepage

  Scenario: Announcements for members
    When I create a member announcement
    Then I should see the announcement on the homepage
    When I close my session
    Then I should not see the announcement on the homepage
    When I am a member with username "pinhead" and email "pinhead@office.hell"
    Then I should see the announcement on the homepage

  Scenario: Announcements for admins
    When I create an admin announcement
    Then I should see the announcement on the homepage
    When I close my session
    Then I should not see the announcement on the homepage
    When I am a member with username "pinhead" and email "pinhead@office.hell"
    Then I should not see the announcement on the homepage

  Scenario: Past announcements
    When I create a announcement for last month
    Then I should not see the announcement on the homepage

  Scenario: Future announcements
    When I create a announcement for next month
    Then I should not see the announcement on the homepage
